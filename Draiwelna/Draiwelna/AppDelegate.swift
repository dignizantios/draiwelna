//
//  AppDelegate.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 20/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import UserNotifications
import Firebase
import FirebaseDatabase
import GeoFire
import JLocationKit

var ToastDuration:TimeInterval = 3.0
var CurrentScreen = Int()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate
{

    var window: UIWindow?
    var nav:UINavigationController?
    var backgroundUpdateTask: UIBackgroundTaskIdentifier!
    var locationManager:CLLocationManager!
    var timer = Timer()
    var didFindLocation = Bool()
    var storeWayPoints = Int()
    let location: LocationManager = LocationManager()

    //MARK:- Appdelegate Method
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
        GMSServices.provideAPIKey(GoogleMapsKey)
        GMSPlacesClient.provideAPIKey(GoogleMapsDrawPathKey)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        FirebaseApp.configure()        
       
        location.requestAccess = .requestAlwaysAuthorization
        
       // UserDefaults.standard.set("0", forKey: "isRideStart")
        BugSenseController.sharedController(withBugSenseAPIKey: "6b37df4e", userDictionary: nil, sendImmediately: true)
        let langStr = Locale.current.languageCode
        print("Lang -- ",langStr ?? "")
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
        } else {
            // Fallback on earlier versions
        }
        
        
        application.registerForRemoteNotifications()
        registerForPushNotifications(application: application)
        
       /* if(langStr == "ar")
        {
            setLangauge(language: Arabic)
            UserDefaults.standard.set(1, forKey: "lang")
        }
        else
        {
            setLangauge(language: English)
            UserDefaults.standard.set(0, forKey: "lang")
        }*/
        
       /* let geofireRef = Database.database().reference()
        let geoFire = GeoFire(firebaseRef: geofireRef)
        geoFire?.removeKey("driver-3")*/
    
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {        
        if UserDefaults.standard.value(forKey: "UserId") as? String != nil
        {
            if UserDefaults.standard.value(forKey: "isDriverLogin") as? String == "0"
            {
                //user
                if UserDefaults.standard.value(forKey: "isRideStart") as? String == "1"
                {
                   
                }
            }
            else
            {
                //driver
                if UserDefaults.standard.value(forKey: "isRideStart") as? String == "1"
                {
                    update()                    
                }
            }
        }
       // self.doBackgroundTask()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        UIApplication.shared.applicationIconBadgeNumber = 0
        if UserDefaults.standard.value(forKey: "UserId") as? String != nil
        {
            if UserDefaults.standard.value(forKey: "isDriverLogin") as? String == "0"
            {
                //user
               
            }
            else
            {
                //driver
               
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getCurrentDiverRide"), object: nil)
            
            }
            
            
        }
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Private Method
    
    func update()
    {
        
        location.getLocation(detectStyle: .SignificantLocationChanges, completion: { (loc) in
            print("latitude ",loc.currentLocation.coordinate.latitude.description)
            print("longitude ",loc.currentLocation.coordinate.longitude.description)
            let geofireRef = Database.database().reference()
            let geoFire = GeoFire(firebaseRef: geofireRef)
            geoFire?.setLocation(CLLocation(latitude: loc.currentLocation.coordinate.latitude, longitude: loc.currentLocation.coordinate.longitude), forKey: "driver-\(UserDefaults.standard.value(forKey: "UserId") as! String)") { (error) in
                if (error != nil) {
                    print("An error occured: \(error)")
                } else
                {
                    if let loadedCart = UserDefaults.standard.array(forKey: "CurrentRideData") as? [[String: Any]]
                    {
                        print(loadedCart)
                        print("loadedCart:- \(loadedCart)")
                        var dict: [[String: Any]] = []
                        for item in loadedCart
                        {
                            print(item["way_lat"]  as! String)
                            print(item["way_lng"] as! String)
                            dict.append(["way_lat": item["way_lat"] as! String, "way_lng": item["way_lng"] as! String])
                        }
                        dict.append(["way_lat": String(loc.currentLocation.coordinate.latitude), "way_lng": String(loc.currentLocation.coordinate.longitude)])
                        UserDefaults.standard.set(dict, forKey: "CurrentRideData")
                        UserDefaults.standard.synchronize()
                        
                    }
                    else
                    {
                        var dict: [[String: Any]] = []
                        dict.append(["way_lat": String(loc.currentLocation.coordinate.latitude), "way_lng": String(loc.currentLocation.coordinate.longitude)])
                        print("dict:- \(dict)")
                        UserDefaults.standard.set(dict, forKey: "CurrentRideData")
                        UserDefaults.standard.synchronize()
                    }
                }
            }

        }, error: { (error) in
            print("error \(error)")
            //optional
        }, authorizationChange: { (status) in
            //optional
            switch status {
            case .notDetermined:
                // If status has not yet been determied, ask for authorization
                self.location.requestAccess = .requestWhenInUseAuthorization
                break
            case .authorizedWhenInUse:
                break
            case .authorizedAlways:
                break
            case .restricted:
                // If restricted by e.g. parental controls. User can't enable Location Services
               // self.openSetting()
                break
            case .denied:
                // If user denied your app access to Location Services, but can grant access from Settings.app
              //  self.openSetting()
                break
                //   default:
                // break
            }
            
        }
        )
    }
    
    /*func openSetting()
    {
        let alertController = UIAlertController (title: AppName, message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }*/


    
    //MARK:- Push notification methods
   
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        NSLog("APNs registration Key: \(deviceTokenString)")
        
        UserDefaults.standard.setValue(deviceTokenString, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
        
        // Persist it in your backend in case it's new
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
        
        //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getToken"), object: nil)
        
        UserDefaults.standard.setValue("", forKey: "deviceToken")
        UserDefaults.standard.synchronize()
    }
    
    // Push notification received
    
    
    /* private func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping () -> Void)
     {
     
     print(userInfo)
     
     if UIApplication.shared.applicationState == UIApplicationState.background || UIApplication.shared.applicationState == UIApplicationState.inactive
     {
     
     }
     else
     {
     
     }
     
     
     
     }*/
    /*  func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any])
     {
     // Print notification payload data
     print("Push notification received: \(data)")
     
     }*/
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let dict = response.notification.request.content.userInfo as NSDictionary
        print(dict)
        
        if UserDefaults.standard.value(forKey: "UserId") as? String != nil
        {
            if UserDefaults.standard.value(forKey: "isDriverLogin") as? String == "0"
            {
                let method:HistoryViewController = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
                let navigationController = NavigationController(rootViewController: method)
                
                let mainViewController = MainViewController()
                mainViewController.rootViewController = navigationController
                mainViewController.setup(type: 6)
                
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = mainViewController
                
                UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)

            }
            else
            {
                let obj:DriverHomeViewController = DriverHomeViewController(nibName: "DriverHomeViewController", bundle: nil)
                self.nav?.pushViewController(obj, animated: true)
            }
        }
        
               
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        completionHandler([.badge, .alert, .sound])
        
    }
    func registerForPushNotifications(application: UIApplication)
    {
        
        // iOS 10 support
        if #available(iOS 10, *)
        {
            
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound])
            { (granted, error) in
                
                if granted
                {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else
                {
                    UserDefaults.standard.setValue("", forKey: "deviceToken")
                    UserDefaults.standard.synchronize()
                    
                }
                
            }
            
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            UIApplication.shared.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
    }
    

    //MARK:- Background UPdatte MEthod
    
    func beginBackgroundUpdateTask() {
        self.backgroundUpdateTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundUpdateTask()
        })
    }
    
    func endBackgroundUpdateTask() {
        UIApplication.shared.endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }
    
    func doBackgroundTask()
    {
        DispatchQueue.main.async(execute: { () -> Void in
            self.beginBackgroundUpdateTask()
            
            // Do something with the result.
            var timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(AppDelegate.displayAlert), userInfo: nil, repeats: true)
            RunLoop.current.add(timer, forMode: RunLoopMode.defaultRunLoopMode)
            RunLoop.current.run()
            
            // End the background task.
            self.endBackgroundUpdateTask()
           
        })
       /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.beginBackgroundUpdateTask()
            
            // Do something with the result.
            var timer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "displayAlert", userInfo: nil, repeats: false)
            NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSDefaultRunLoopMode)
            NSRunLoop.currentRunLoop().run()
            
            // End the background task.
            self.endBackgroundUpdateTask()
        })*/
    
    }
    
    func displayAlert()
    {
        print("Work")
        let note = UILocalNotification()
        note.alertBody = "As a test I'm hoping this will run in the background every X number of seconds..."
        note.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(note)
    }


}

extension UIApplication
{
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}


