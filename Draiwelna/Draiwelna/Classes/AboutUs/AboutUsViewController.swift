//
//  AboutUsViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 12/08/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView

class AboutUsViewController: UIViewController
{
    @IBOutlet var btnSideMenuOutletAR: UIButton!
    @IBOutlet var btnSideMenuOutletEN: UIButton!
    @IBOutlet var webviewOfAboutus: UIWebView!
    @IBOutlet var lblScreenTitle: UILabel!
    
    //MARK:- View Life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
       
        DidLoadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.AboutUsSceenLoad), name: NSNotification.Name(rawValue: "isComeFromAboutUs"), object: nil)

        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Method
    
    func AboutUsSceenLoad()
    {
        DidLoadData()
    }
    
    func DidLoadData()
    {
        self.lblScreenTitle.text = mapping.string(forKey: "About us")
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnSideMenuOutletAR.isHidden = true
            self.btnSideMenuOutletEN.isHidden = false
        }
        else
        {
            self.btnSideMenuOutletAR.isHidden = false
            self.btnSideMenuOutletEN.isHidden = true
        }
        getAboutUSData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Service
    
    func getAboutUSData()
    {
        let kRegiURL = "\(DraiwelnaMainURL)get_all_static_page"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "flag_view":"1"]
            
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user:basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print("json:-\(json)")
                    if json["flag"].stringValue == "1"
                    {
                        let dictData = json["data"]
                        let str = dictData["link_description"].stringValue
                        let finalHTML: String = "\("<div style=\"color:#ffffff\">")\(str)\("</div>")"
                        self.webviewOfAboutus.loadHTMLString(finalHTML, baseURL: nil)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}
