//
//  AccountViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView

class AccountViewController: UIViewController
{
    
    //MARK:- Variable Declaration
    
    var dictUserData:[String:JSON] = [:]
    
    //MARK:- Outlet Zone
    
    @IBOutlet var lblScreenTitle: UILabel!    
    @IBOutlet var lblUserAccountName: UILabel!
    @IBOutlet var imgOfUserProfile: UIImageView!
    @IBOutlet var lblUserMobileNumber: UILabel!
    @IBOutlet var lblUserEmaiAddress: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var btnChangePasswordOutlet: ShadowButton!
    @IBOutlet var btnEditDetailsOutlet: ShadowButton!
    @IBOutlet var lblUserLastname: UILabel!
    
    // English
    
    @IBOutlet var btnSideMenuOutletEN: UIButton!
    @IBOutlet var imgOfAccountNameEN: UIImageView!
    @IBOutlet var imgOfUserNameEN: UIImageView!
    @IBOutlet var imgOfUserMobileEN: UIImageView!
    @IBOutlet var imgOfUserEmailEN: UIImageView!
    @IBOutlet var imgOfUserLastNameEN: UIImageView!

    //Arabic
    
    @IBOutlet var imgOfUserLastNameAR: UIImageView!
    @IBOutlet var imgOfUserEmailAR: UIImageView!
    @IBOutlet var imgOfUserMobileAR: UIImageView!
    @IBOutlet var imgOfUserNameAR: UIImageView!
    @IBOutlet var imgOfAccountNameAR: UIImageView!
    @IBOutlet var btnSideMenuOutletAR: UIButton!
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
        
        DidLoadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpdateProfile), name: NSNotification.Name(rawValue: "UpdateProfile"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.AccountSceenLoad), name: NSNotification.Name(rawValue: "isComeFromAccount"), object: nil)
       

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        showGesture()
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Private Zone
    
    func UpdateProfile()
    {
        getUserProfileData()
    }
    
    func AccountSceenLoad()
    {
       DidLoadData()
    }
    
    func DidLoadData()
    {
        self.lblScreenTitle.text = mapping.string(forKey: "Account")
        self.btnChangePasswordOutlet.setTitle(mapping.string(forKey: "Change password"), for: .normal)
        self.btnEditDetailsOutlet.setTitle(mapping.string(forKey: "Edit details"), for: .normal)
        
        self.imgOfUserProfile.layer.cornerRadius = self.imgOfUserProfile.frame.size.height/2
        self.imgOfUserProfile.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
        self.imgOfUserProfile.layer.borderWidth = 2
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            //English
            
            self.btnSideMenuOutletEN.isHidden = false
            self.imgOfAccountNameEN.isHidden = false
            self.imgOfUserNameEN.isHidden = false
            self.imgOfUserMobileEN.isHidden = false
            self.imgOfUserEmailEN.isHidden = false
            self.imgOfUserLastNameEN.isHidden = false
            
            self.btnSideMenuOutletAR.isHidden = true
            self.imgOfAccountNameAR.isHidden = true
            self.imgOfUserNameAR.isHidden = true
            self.imgOfUserMobileAR.isHidden = true
            self.imgOfUserEmailAR.isHidden = true
            self.imgOfUserLastNameAR.isHidden = true
            
            self.lblUserName.textAlignment = .left
            self.lblUserAccountName.textAlignment = .left
            self.lblUserEmaiAddress.textAlignment = .left
            self.lblUserMobileNumber.textAlignment = .left
            self.lblUserLastname.textAlignment = .left
            
        }
        else
        {
            //Arabic
            self.btnSideMenuOutletEN.isHidden = true
            self.imgOfAccountNameEN.isHidden = true
            self.imgOfUserNameEN.isHidden = true
            self.imgOfUserMobileEN.isHidden = true
            self.imgOfUserEmailEN.isHidden = true
            self.imgOfUserLastNameEN.isHidden = true
            
            self.btnSideMenuOutletAR.isHidden = false
            self.imgOfAccountNameAR.isHidden = false
            self.imgOfUserNameAR.isHidden = false
            self.imgOfUserMobileAR.isHidden = false
            self.imgOfUserEmailAR.isHidden = false
            self.imgOfUserLastNameAR.isHidden = false
            
            self.lblUserName.textAlignment = .right
            self.lblUserAccountName.textAlignment = .right
            self.lblUserEmaiAddress.textAlignment = .right
            self.lblUserMobileNumber.textAlignment = .right
            self.lblUserLastname.textAlignment = .right
        }
         getUserProfileData()

    }
    
    //MARK:- Actin Zone
    
    @IBAction func btnChangePasswordAction(_ sender: Any)
    {
        let  obj:ChangePasswordViewController = ChangePasswordViewController(nibName: "ChangePasswordViewController", bundle: nil)
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
    }
    @IBAction func btnEditDetailsAction(_ sender: Any)
    {
        let obj:EditAccountViewController = EditAccountViewController(nibName: "EditAccountViewController", bundle: nil)
        obj.dictUserData = dictUserData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSideMenuAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            sideMenuController?.leftViewController?.showLeftViewAnimated(true)
        }
        else
        {
            sideMenuController?.rightViewController?.showRightViewAnimated(true)
        }
    }
    
  
    
    //MARK:- Servie
    
    func getUserProfileData()
    {
        
        let kRegiURL = "\(DraiwelnaMainURL)save_user_profile"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "flag_view":"0"]
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user:basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print("json:-\(json)")
                    if json["flag"].stringValue == "1"
                    {
                        self.dictUserData = json["data"].dictionaryValue
                       
                        let strImage = self.dictUserData["profile_image"]?.stringValue
                        let strPlaceImage="user_placeholder_register_screen.png"
                        let urlImage:NSURL = NSURL(string: strImage!)!
                        self.imgOfUserProfile.sd_setImage(with: urlImage as URL, placeholderImage:UIImage(named:strPlaceImage))
                        
                        self.lblUserAccountName.text = self.dictUserData["first_name"]?.stringValue
                        self.lblUserName.text = self.dictUserData["username"]?.stringValue
                        self.lblUserMobileNumber.text = self.dictUserData["mobile_number"]?.stringValue
                        self.lblUserEmaiAddress.text = self.dictUserData["email"]?.stringValue
                        self.lblUserLastname.text = self.dictUserData["last_name"]?.stringValue
                        
                        let defaults = UserDefaults.standard
                        defaults.set(self.dictUserData["profile_image"]?.stringValue, forKey: "UserProfilePicture")
                        defaults.set(self.dictUserData["username"]?.stringValue, forKey: "UserName")
                        defaults.synchronize()
                        
                        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tblRearENReload"), object: nil)
                        }
                        else
                        {
                           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tblRearARReload"), object: nil)
                        }
                        
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                   KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }


}
