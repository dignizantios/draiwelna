//
//  BookingDetailViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 29/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import KSToastView
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class BookingDetailViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate
{
    //MARK:- Varible Declaration

    var isCheckLanguage = Bool()
    var activeTextField = Int()
    var dictEditData:JSON!
    
    //MARK:- Outlet Zone
    
    // Common
    
    @IBOutlet var TopOfDatePickerView: NSLayoutConstraint!
    @IBOutlet var datePickerOfBookingRide: UIDatePicker!
    
    // English
    
    @IBOutlet var viewOfEnglish: ShadowView!
    @IBOutlet var txtBookingDateEN: UITextField!
    @IBOutlet var txtBookingTimeEN: UITextField!
    @IBOutlet var txtViewOfCommentsEN: UITextView!
    @IBOutlet var lblTxtViewPlaceHolderEN: UILabel!
    @IBOutlet var btnSpecialRequestOutletEN: UIButton!
    @IBOutlet var btnRegularBookOutletEN: UIButton!
    @IBOutlet var btnCancelOutlet: UIButton!
    @IBOutlet var btnDoneOutlet: UIButton!
    
    // Arabic Outlet
    
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var txtBookingDateAR: UITextField!
    @IBOutlet var txtBookingTimeAR: UITextField!
    @IBOutlet var txtViewOfCommentsAR: UITextView!
    @IBOutlet var lblTxtViewPlaceHolderAR: UILabel!
    @IBOutlet var btnSpecialRequestOutletAR: UIButton!
    @IBOutlet var btnRegularBookOutletAR: UIButton!


    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TopOfDatePickerView.constant = UIScreen.main.bounds.height
        
        print("dicData \(dictEditData)")
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
          
          //  isCheckLanguage = false
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dictEditData["start_date"].stringValue)
            self.txtBookingDateEN.text = DateToString(Formatter: "dd-MM-yyyy", date: strBookDate)
            self.txtBookingTimeEN.text = DateToString(Formatter: "hh:mm a", date: strBookDate)
            
            self.txtViewOfCommentsEN.text = dictEditData["comment"].stringValue
            if dictEditData["comment"].stringValue == ""
            {
                self.lblTxtViewPlaceHolderEN.isHidden = false
            }
            else
            {
                 self.lblTxtViewPlaceHolderEN.isHidden = true
            }
            
            self.viewOfEnglish.isHidden = false
            self.viewOfArabic.isHidden = true
            
            self.viewOfEnglish.layer.cornerRadius = 5
            
            self.btnRegularBookOutletEN.layer.cornerRadius = 5
            self.btnRegularBookOutletEN.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            self.btnRegularBookOutletEN.layer.borderWidth = 1
            
            self.btnSpecialRequestOutletEN.layer.cornerRadius = 5
            
            [txtBookingDateEN,txtBookingTimeEN].forEach({ customizeTextFieldEN(textfield: $0) })
            
            self.txtViewOfCommentsEN.layer.cornerRadius = 5
            self.txtViewOfCommentsEN.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            self.txtViewOfCommentsEN.layer.borderWidth = 1
            
            self.txtViewOfCommentsEN.textContainerInset = UIEdgeInsets(top: 9 , left: 13, bottom: 0, right: 10)
            
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
            
        }
        else
        {
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dictEditData["start_date"].stringValue)
            self.txtBookingDateAR.text = DateToString(Formatter: "dd-MM-yyyy", date: strBookDate)
            self.txtBookingTimeAR.text = DateToString(Formatter: "hh:mm a", date: strBookDate)
            
            self.viewOfEnglish.isHidden = true
            self.viewOfArabic.isHidden = false
            
            self.txtViewOfCommentsAR.text = dictEditData["comment"].stringValue
            if dictEditData["comment"].stringValue == ""
            {
                self.lblTxtViewPlaceHolderAR.isHidden = false
            }
            else
            {
                self.lblTxtViewPlaceHolderAR.isHidden = true
            }
            
           // isCheckLanguage = true
            
            self.viewOfArabic.layer.cornerRadius = 5
            
            self.btnRegularBookOutletAR.layer.cornerRadius = 5
            self.btnRegularBookOutletAR.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            self.btnRegularBookOutletAR.layer.borderWidth = 1
            
            self.btnSpecialRequestOutletAR.layer.cornerRadius = 5
            
            [txtBookingDateAR,txtBookingTimeAR].forEach({ customizeTextFieldAR(textfield: $0) })
            
            self.txtViewOfCommentsAR.layer.cornerRadius = 5
            self.txtViewOfCommentsAR.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            self.txtViewOfCommentsAR.layer.borderWidth = 1
            
            self.txtViewOfCommentsAR.textContainerInset = UIEdgeInsets(top: 9 , left: 10, bottom: 0, right: 13)
            
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
        }


        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        activeTextField = Int()
        if textField == self.txtBookingDateEN || textField == self.txtBookingDateAR
        {
            activeTextField = 1
            self.view.endEditing(true)
            self.datePickerOfBookingRide.minimumDate = Date()
            self.datePickerOfBookingRide.datePickerMode = .date
            self.movePickerUp()
            return false
        }
        else if textField == self.txtBookingTimeEN || textField == self.txtBookingTimeAR
        {
            activeTextField = 2
            self.view.endEditing(true)
            self.datePickerOfBookingRide.minimumDate = Date()
            self.datePickerOfBookingRide.datePickerMode = .time
            self.movePickerUp()
            return false
        }
        self.movePickerDown()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.movePickerDown()
        return textField.resignFirstResponder()
    }
    
    func ClerAllTextField(textfied:UITextField)
    {
        textfied.text = ""
    }
    
    
    //MARK:- TextView Delegate
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == self.txtViewOfCommentsEN
        {
            if self.txtViewOfCommentsEN.text == ""
            {
                self.lblTxtViewPlaceHolderEN.isHidden = false
            }
            else
            {
                self.lblTxtViewPlaceHolderEN.isHidden = true
            }
        }
        else
        {
            if self.txtViewOfCommentsAR.text == ""
            {
                self.lblTxtViewPlaceHolderAR.isHidden = false
            }
            else
            {
                self.lblTxtViewPlaceHolderAR.isHidden = true
            }
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK: - View UP and Down Method
    
    func movePickerUp()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.TopOfDatePickerView.constant = UIScreen.main.bounds.height - 180
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
        
        
    }
    
    func movePickerDown()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.TopOfDatePickerView.constant = UIScreen.main.bounds.height
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
    }
    
    //MARK:- Private MEthod
    
    func navigateToEditRide()
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if self.txtBookingDateEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: 1)
            }
            else if self.txtBookingTimeEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: 1)
            }
            else
            {
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsEN.text = trimmedUserName
                print("BookDate: \(self.txtBookingDateEN.text!)")
                obj.strBookingDate = self.txtBookingDateEN.text!
                obj.strBookingTime = self.txtBookingTimeEN.text!
                obj.strComment = self.txtViewOfCommentsEN.text!
                obj.strRideType = self.dictEditData["ride_type"].stringValue
                obj.dictLocationInfo = self.dictEditData["location_info"].dictionaryObject as! NSMutableDictionary
                obj.dictRideData = self.dictEditData["fair_data"]
                obj.strSpecialComment = ""
                obj.rideEditID = self.dictEditData["id"].stringValue
                obj.isComeFromEditRide = true
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        else
        {
            if self.txtBookingDateAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: 1)
            }
            else if self.txtBookingTimeAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: 1)
            }
            else
            {
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsAR.text = trimmedUserName
                obj.strBookingDate = self.txtBookingDateAR.text!
                obj.strBookingTime = self.txtBookingTimeAR.text!
                obj.strComment = self.txtViewOfCommentsAR.text!
                obj.strRideType = self.dictEditData["ride_type"].stringValue
                obj.strSpecialComment = ""
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnCloseBookingAction(_ sender: Any)
    {
       
    }
    @IBAction func btnSpecialRequestAction(_ sender: Any)
    {
        //  let obj:AccountViewController = AccountViewController(nibName: "AccountViewController", bundle: nil)
        //self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnRegularBookAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if self.txtBookingDateEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: 1)
            }
            else if self.txtBookingTimeEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: 1)
            }
            else
            {
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsEN.text = trimmedUserName
                print("BookDate: \(self.txtBookingDateEN.text!)")
                obj.strBookingDate = self.txtBookingDateEN.text!
                obj.strBookingTime = self.txtBookingTimeEN.text!
                obj.strComment = self.txtViewOfCommentsEN.text!
                obj.strRideType = self.dictEditData["ride_type"].stringValue
                obj.dictLocationInfo = self.dictEditData["location_info"].dictionaryObject as! NSMutableDictionary
                obj.dictRideData = self.dictEditData["fair_data"]
                obj.strSpecialComment = ""
                obj.rideEditID = self.dictEditData["id"].stringValue
                obj.isComeFromEditRide = true
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        else
        {
            if self.txtBookingDateAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: 1)
            }
            else if self.txtBookingTimeAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: 1)
            }
            else
            {
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsAR.text = trimmedUserName
                obj.strBookingDate = self.txtBookingDateAR.text!
                obj.strBookingTime = self.txtBookingTimeAR.text!
                obj.strComment = self.txtViewOfCommentsAR.text!
                obj.strRideType = self.dictEditData["ride_type"].stringValue
                obj.strSpecialComment = ""
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    
    @IBAction func btnPickerCancelAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.movePickerDown()
        }
        else
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
            }
        }
    }
    @IBAction func btnPickerDoneAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
            }
        }
        else
        {
            self.movePickerDown()
        }
    }



}
