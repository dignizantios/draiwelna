//
//  ChangePasswordViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import KSToastView
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ChangePasswordViewController: UIViewController,UITextFieldDelegate
{
    //MARK:- Outlet Zone
    
    @IBOutlet var viewOfChangePassword: UIView!
    @IBOutlet var txtOldPassword: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var btnChangePasswordOutlet: UIButton!
    @IBOutlet var btnCloseOutletEN: UIButton!
    @IBOutlet var lblChangePasswordTitle: UILabel!
    @IBOutlet var btnCloseOutletAR: UIButton!
    

    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        self.lblChangePasswordTitle.text = mapping.string(forKey: "Change password")
        self.btnChangePasswordOutlet.setTitle(mapping.string(forKey: "Change password"), for: .normal)
        
        self.txtNewPassword.placeholder = mapping.string(forKey: "New Password")
        self.txtOldPassword.placeholder = mapping.string(forKey: "Old Password")
        self.txtConfirmPassword.placeholder = mapping.string(forKey: "Confirm Password")
        
        [txtNewPassword,txtOldPassword,txtConfirmPassword].forEach({ customizeTextFieldEN(textfield: $0) })
        
        self.viewOfChangePassword.layer.cornerRadius = 5
        self.btnChangePasswordOutlet.layer.cornerRadius = 5
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnCloseOutletAR.isHidden = true
            self.btnCloseOutletEN.isHidden = false
            
            self.txtNewPassword.textAlignment = .left
            self.txtOldPassword.textAlignment = .left
            self.txtConfirmPassword.textAlignment = .left
        }
        else
        {
            self.btnCloseOutletAR.isHidden = false
            self.btnCloseOutletEN.isHidden = true
            self.txtNewPassword.textAlignment = .right
            self.txtOldPassword.textAlignment = .right
            self.txtConfirmPassword.textAlignment = .right
        }

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Memory Management

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Update Location
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnCloseAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnChangePasswordAction(_ sender: Any)
    {
        if self.txtOldPassword.text == ""
        {
            KSToastView.ks_showToast(mapping.string(forKey: "Please enter your old password"), duration: ToastDuration)
        }
        else if self.txtNewPassword.text == ""
        {
            KSToastView.ks_showToast(mapping.string(forKey: "Please enter your new password"), duration: ToastDuration)
        }
        else if !(self.txtNewPassword.text == "")
        {
            if (self.txtNewPassword.text?.characters.count)! < 6
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Password must be at least 6 characters long."), duration: ToastDuration)
            }
            else if self.txtConfirmPassword.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please confirm your password"), duration: ToastDuration)
            }
            else
            {
                if(!(self.txtNewPassword.text == self.txtConfirmPassword.text))
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Password and confirmation password must match."), duration: ToastDuration)
                }
                else
                {
                    userChangePassword()
                }
            }
            
        }
    }
    
    
    //MARK:- Service
    
    func userChangePassword()
    {
        let kRegiURL = "\(DraiwelnaMainURL)change_password"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "access_token": UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "user_id": UserDefaults.standard.value(forKey: "UserId") as! String,
                         "current_password": self.txtOldPassword.text!,
                         "new_password": self.txtNewPassword.text!]
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        
                        KSToastView.ks_showToast(json["msg"].stringValue, completion: {self.dismiss(animated: true, completion: nil)})
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }


}
