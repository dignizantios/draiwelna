//
//  ContactUsViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView
import GoogleMaps

class ContactUsViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,CLLocationManagerDelegate
{
    //MARK:- Varible Declaration
    
    let locationManager = CLLocationManager()
    var didFindLocation = Bool()
    var strCountryCode = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var btnSubmitOutlet: UIButton!
    @IBOutlet var lblScreenTitle: UILabel!
    
    //ENglish
    
    @IBOutlet var btnSideMenuOutletEN: UIButton!
    @IBOutlet var viewOfEnglish: UIView!
    @IBOutlet var txtUsernameEN: UITextField!
    @IBOutlet var txtUserMobileEN: UITextField!
    @IBOutlet var txtSubjectEN: UITextField!
    @IBOutlet var txtViewOfCommentsEN: UITextView!
    @IBOutlet var lblPlaceHolderEN: UILabel!
    
    //ARabic
    
    @IBOutlet var btnSideMenuOutletAR: UIButton!
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var txtUserNameAR: UITextField!
    @IBOutlet var txtUserMobileAR: UITextField!
    @IBOutlet var txtSubjectAR: UITextField!
    @IBOutlet var txtViewOfCommentsAR: UITextView!
    @IBOutlet var lblPlaceHolderAR: UILabel!
    
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      //  self.btnSubmitOutlet.setTitle(mapping.string(forKey: "SUBMIT"), for: .normal)
        
        locationManager.delegate = self
        didFindLocation = true
        locationManager.startUpdatingLocation()
        
        DidLoadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.ContactUsSceenLoad), name: NSNotification.Name(rawValue: "isComeFromContactUs"), object: nil)
      

        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Private Zone
    
    func ContactUsSceenLoad()
    {
        DidLoadData()
    }
    
    func DidLoadData()
    {
        self.lblScreenTitle.text = mapping.string(forKey: "Contact us")
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.viewOfArabic.isHidden = true
            self.viewOfEnglish.isHidden = false
            [txtUsernameEN,txtUserMobileEN,txtSubjectEN].forEach({ customizeTextFieldEN(textfield: $0) })
            
            self.txtViewOfCommentsEN.layer.cornerRadius = 5
            self.txtViewOfCommentsEN.layer.borderColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1).cgColor
            self.txtViewOfCommentsEN.layer.borderWidth = 1
            
            self.txtViewOfCommentsEN.textContainerInset = UIEdgeInsets(top: 9 , left: 13, bottom: 0, right: 10)
            
            self.btnSideMenuOutletAR.isHidden = true
            self.btnSideMenuOutletEN.isHidden = false
            
        }
        else
        {
            self.btnSideMenuOutletAR.isHidden = false
            self.btnSideMenuOutletEN.isHidden = true
            
            self.viewOfArabic.isHidden = false
            self.viewOfEnglish.isHidden = true
            
            [txtUserNameAR,txtUserMobileAR,txtSubjectAR].forEach({ customizeTextFieldAR(textfield: $0) })
            
            self.txtViewOfCommentsAR.layer.cornerRadius = 5
            self.txtViewOfCommentsAR.layer.borderColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1).cgColor
            self.txtViewOfCommentsAR.layer.borderWidth = 1
            
            self.txtViewOfCommentsAR.textContainerInset = UIEdgeInsets(top: 9 , left: 10, bottom: 0, right: 13)
        }
          getUserProfileData()

    }
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }

    
    //MARK:- TextView Delegate
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == self.txtViewOfCommentsEN
        {
            if self.txtViewOfCommentsEN.text == ""
            {
                self.lblPlaceHolderEN.isHidden = false
            }
            else
            {
                self.lblPlaceHolderEN.isHidden = true
            }
        }
        else
        {
            if self.txtViewOfCommentsAR.text == ""
            {
                self.lblPlaceHolderAR.isHidden = false
            }
            else
            {
                self.lblPlaceHolderAR.isHidden = true
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtUserMobileEN ||  textField == txtUserMobileAR
        {
            let limitLength = 8
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength // Bool
        }
        return true
    }

    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK:- Update Location
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if didFindLocation == true
        {
            didFindLocation = false
            // let currentLocation:CLLocationCoordinate2D = manager.location!.coordinate
            
            //   print("locations = \(currentLocation.latitude) \(currentLocation.longitude)")
            guard let currentLocation = locations.first else { return }
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
                guard let currentLocPlacemark = placemarks?.first else { return }
                print(currentLocPlacemark.country ?? "No country found")
                print(currentLocPlacemark.isoCountryCode ?? "No country code found")
                self.strCountryCode = self.getCountryPhonceCode(currentLocPlacemark.isoCountryCode!)
            }
            
        }
        else
        {
            didFindLocation = false
            locationManager.stopUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        didFindLocation = false
        locationManager.stopUpdatingLocation()
    }


   //MARK:- Action Zone
    
    
    @IBAction func btnSubmitAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let trimmedUserName = self.txtUsernameEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUsernameEN.text = trimmedUserName
            
            let trimmedUserLastName = self.txtSubjectEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtSubjectEN.text = trimmedUserLastName
            
            let trimmedUserFirstName = self.txtViewOfCommentsEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtViewOfCommentsEN.text = trimmedUserFirstName            
            
            if self.txtUsernameEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserMobileEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your mobile number"), duration: ToastDuration)
            }
            else if (self.txtUserMobileEN.text?.characters.count)! < 8
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a valid 8-digit mobile number"), duration: ToastDuration)
            }
            else if self.txtSubjectEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a subject."), duration: ToastDuration)
            }
            else if self.txtViewOfCommentsEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter comments"), duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                postContactUsFormEnglish()
            }
        }
        else
        {
            let trimmedUserName = self.txtUserNameAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserNameAR.text = trimmedUserName
            
            let trimmedUserLastName = self.txtSubjectAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtSubjectAR.text = trimmedUserLastName
            
            let trimmedUserFirstName = self.txtViewOfCommentsAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtViewOfCommentsAR.text = trimmedUserFirstName
            
            
            if self.txtUserNameAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserMobileAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your mobile number"), duration: ToastDuration)
            }
            else if (self.txtUserMobileAR.text?.characters.count)! < 8
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a valid 8-digit mobile number"), duration: ToastDuration)
            }
            else if self.txtSubjectAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a subject."), duration: ToastDuration)
            }
            else if self.txtViewOfCommentsAR.text == ""
            {
                 KSToastView.ks_showToast(mapping.string(forKey: "Please enter comments"), duration: ToastDuration)
            }
            else
            {                
                self.view.endEditing(true)
                postContactUsFormArabic()
            }
        }
    }
    
    //MARK:- Service

    func getUserProfileData()
    {
        let kRegiURL = "\(DraiwelnaMainURL)save_user_profile"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "flag_view":"0"]
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user:basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print("json:-\(json)")
                    if json["flag"].stringValue == "1"
                    {
                        let dictUserData = json["data"].dictionaryValue
                       
                        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                        {
                            self.txtUsernameEN.text = dictUserData["username"]?.stringValue
                            self.txtUserMobileEN.text = dictUserData["mobile_number"]?.stringValue
                        }
                        else
                        {
                            self.txtUserNameAR.text = dictUserData["username"]?.stringValue
                            self.txtUserMobileAR.text = dictUserData["mobile_number"]?.stringValue
                        }
                      
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

    func postContactUsFormEnglish()
    {
        let kRegiURL = "\(DraiwelnaMainURL)get_all_static_page"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "name":self.txtUsernameEN.text!,
                         "phone":self.txtUserMobileEN.text!,
                         "subject":self.txtSubjectEN.text!,
                         "comment":self.txtViewOfCommentsEN.text!,
                         "country_code":strCountryCode,
                         "flag_view":"0"]
            
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user:basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print("json:-\(json)")
                    if json["flag"].stringValue == "1"
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func postContactUsFormArabic()
    {
        let kRegiURL = "\(DraiwelnaMainURL)get_all_static_page"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "name":self.txtUserNameAR.text!,
                         "phone":self.txtUserMobileAR.text!,
                         "subject":self.txtSubjectAR.text!,
                         "comment":self.txtViewOfCommentsAR.text!,
                         "country_code":strCountryCode,
                         "flag_view":"0"]
            
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user:basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print("json:-\(json)")
                    if json["flag"].stringValue == "1"
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }



}
