//
//  CreditCardViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 27/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class CreditCardViewController: UIViewController,UITextFieldDelegate
{

    //MARK:- Outlet ZOne
    
    @IBOutlet var btnBackOutletEN: UIButton!
    @IBOutlet var btnBackOutletAR: UIButton!
    @IBOutlet var lblScreenTitle: UILabel!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var txtCreditCardName: UITextField!
    @IBOutlet var txtCreditCardNumber: UITextField!
    @IBOutlet var txtExpiryMonth: UITextField!
    @IBOutlet var txtCVV: UITextField!
    @IBOutlet var txtSecurityNumber: UITextField!
    @IBOutlet var btnMakePaymnetOutlet: UIButton!
    @IBOutlet var imgOfDropDownAR: UIImageView!
    @IBOutlet var imgOfDropDownEN: UIImageView!
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblScreenTitle.text = mapping.string(forKey: "Credit Card")
        self.lblTitle.text = mapping.string(forKey: "Enter your card details below")
        self.btnMakePaymnetOutlet.setTitle(mapping.string(forKey: "Confirm payment"), for: .normal)
        self.txtCreditCardName.placeholder = mapping.string(forKey: "Name on card")
        self.txtCreditCardNumber.placeholder = mapping.string(forKey: "Card number")
        self.txtExpiryMonth.placeholder = mapping.string(forKey: "Expiry date")
        self.txtSecurityNumber.placeholder = mapping.string(forKey: "Security number")
        
        self.btnMakePaymnetOutlet.layer.cornerRadius =  self.btnMakePaymnetOutlet.frame.size.height/2
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnBackOutletAR.isHidden = true
            self.btnBackOutletEN.isHidden = false
            
            self.imgOfDropDownAR.isHidden = true
            self.imgOfDropDownEN.isHidden = false
            
            self.txtCreditCardName.textAlignment = .left
            self.txtCreditCardNumber.textAlignment = .left
            self.txtExpiryMonth.textAlignment = .left
            self.txtSecurityNumber.textAlignment = .left
            self.txtCVV.textAlignment = .left
            self.lblTitle.textAlignment = .left
            
            [txtCreditCardName,txtCreditCardNumber,txtExpiryMonth,txtSecurityNumber,txtCVV].forEach({ customizeTextFieldEN(textfield: $0) })
            
        }
        else
        {
            self.btnBackOutletAR.isHidden = false
            self.btnBackOutletEN.isHidden = true
            
            self.imgOfDropDownAR.isHidden = false
            self.imgOfDropDownEN.isHidden = true
            
            self.lblTitle.textAlignment = .right
            
            self.txtCreditCardName.textAlignment = .right
            self.txtCreditCardNumber.textAlignment = .right
            self.txtExpiryMonth.textAlignment = .right
            self.txtSecurityNumber.textAlignment = .right
            self.txtCVV.textAlignment = .right
            
             [txtCreditCardName,txtCreditCardNumber,txtExpiryMonth,txtSecurityNumber,txtCVV].forEach({ customizeTextFieldAR(textfield: $0) })
        }
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- Memory Management

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return textField.resignFirstResponder()
    }
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMakePaymentAction(_ sender: Any)
    {
    }

}
