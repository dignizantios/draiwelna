//
//  DriverInfoViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import MessageUI
import KSToastView

class DriverInfoViewController: UIViewController,MFMailComposeViewControllerDelegate
{
    
    //MARK:- Variable Declaration
    
    var dictDriverData:JSON!
    var dictCabData:JSON!
    var strProfile = String()

    //MARK:- Outlet Zone
    
    //Common
    @IBOutlet var lblScreentitle: UILabel!
    @IBOutlet var imgOfDriveProfile: UIImageView!
    @IBOutlet var lblDriverName: UILabel!
    @IBOutlet var lblDriverAddress: UILabel!
    @IBOutlet var lblDrivePhoneNumber: UILabel!
    @IBOutlet var lblDriveLiencense: UILabel!
    @IBOutlet var lblCarName: UILabel!
    @IBOutlet var btnCallDriveOutlet: UIButton!
    
    @IBOutlet var btnMsgDeiverOutlet: UIButton!
    @IBOutlet var btnCallDriverOutlet: UIButton!
    
    //ENglish
    @IBOutlet var btnBackENOutlet: UIButton!
    @IBOutlet var imgOfAddressEN: UIImageView!
    @IBOutlet var imgOfMobileEN: UIImageView!
    @IBOutlet var imgOfDriverLiecenseEN: UIImageView!
    @IBOutlet var imgOfCarNameEN: UIImageView!
    @IBOutlet var imgOfCallEN: UIImageView!
    
    //ARabic
    
    @IBOutlet var btnBackOutletAR: UIButton!
    @IBOutlet var imgOfAddressAR: UIImageView!
    @IBOutlet var imgOfMobileAR: UIImageView!
    @IBOutlet var imgOfDriverLiecenseAR: UIImageView!
    @IBOutlet var imgOfCarNameAR: UIImageView!
    @IBOutlet var imgOfCallAR: UIImageView!
    
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()      
        
        self.btnCallDriveOutlet.setTitle(mapping.string(forKey: "CALL"), for: .normal)
        self.lblScreentitle.text = mapping.string(forKey: "Driver Information")
        
        self.imgOfDriveProfile.layer.masksToBounds = false
        self.imgOfDriveProfile.layer.cornerRadius = self.imgOfDriveProfile.frame.height/2
        self.imgOfDriveProfile.clipsToBounds = true
        self.lblDriverName.text = (dictDriverData["driver_name"].stringValue).uppercased()
        
        self.imgOfDriveProfile.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
        self.imgOfDriveProfile.layer.borderWidth = 2
       
        let strPlaceImage="user_placeholder_register_screen.png"
        let urlImage:NSURL = NSURL(string: dictDriverData["driver_image"].stringValue)!
        self.imgOfDriveProfile.sd_setImage(with: urlImage as URL, placeholderImage:UIImage(named:strPlaceImage))
        
        self.lblDriverAddress.text = dictDriverData["city"].stringValue
        self.lblDrivePhoneNumber.text = dictDriverData["mobile_number"].stringValue
        self.lblDriveLiencense.text = dictDriverData["licence_number"].stringValue
        self.lblCarName.text = dictCabData["cab_model"].stringValue
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            //ENglish
            
            self.btnCallDriverOutlet.setImage(UIImage.init(named: "ic_call_driver_track_details.png"), for: .normal)
            self.btnMsgDeiverOutlet.setImage(UIImage.init(named: "ic_message_driver_track_details.png"), for: .normal)
            
            self.lblDriverAddress.textAlignment = .left
            self.lblDrivePhoneNumber.textAlignment = .left
            self.lblDriveLiencense.textAlignment = .left
            self.lblCarName.textAlignment = .left
            
            self.btnBackENOutlet.isHidden = false
            self.imgOfAddressEN.isHidden = false
            self.imgOfMobileEN.isHidden = false
            self.imgOfDriverLiecenseEN.isHidden = false
            self.imgOfCarNameEN.isHidden = false
            self.imgOfCallEN.isHidden = true
            
            self.btnBackOutletAR.isHidden = true
            self.imgOfAddressAR.isHidden = true
            self.imgOfMobileAR.isHidden = true
            self.imgOfDriverLiecenseAR.isHidden = true
            self.imgOfCarNameAR.isHidden = true
            self.imgOfCallAR.isHidden = true
        }
        else
        {
            //Arabic
            self.btnBackENOutlet.isHidden = true
            self.imgOfAddressEN.isHidden = true
            self.imgOfMobileEN.isHidden = true
            self.imgOfDriverLiecenseEN.isHidden = true
            self.imgOfCarNameEN.isHidden = true
            self.imgOfCallEN.isHidden = true
            
            self.btnBackOutletAR.isHidden = false
            self.imgOfAddressAR.isHidden = false
            self.imgOfMobileAR.isHidden = false
            self.imgOfDriverLiecenseAR.isHidden = false
            self.imgOfCarNameAR.isHidden = false
            self.imgOfCallAR.isHidden = true
            
            self.lblDriverAddress.textAlignment = .right
            self.lblDrivePhoneNumber.textAlignment = .right
            self.lblDriveLiencense.textAlignment = .right
            self.lblCarName.textAlignment = .right
            
            self.btnCallDriverOutlet.setImage(UIImage.init(named: "ic_message_driver_track_details.png"), for: .normal)
            self.btnMsgDeiverOutlet.setImage(UIImage.init(named: "ic_call_driver_track_details.png"), for: .normal)
        }

        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- ActionZone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnCallDriverAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud?.color = UIColor.clear
            
            let phone = dictDriverData["mobile_number"].stringValue
            
            var phoneStr: String = "telprompt://\(phone)"
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
            else
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Calling is not available."), duration: ToastDuration)
                //BasicStuff.showAlertMessage(vc: self, titleStr: "CoolDrive", messageStr: "Call facility is not available!!!")
            }
            self.perform(#selector(self.PushToSalonListController), with: nil, afterDelay: 0.5)
            
        }
        else
        {
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail()
            {
                self.present(mailComposeViewController, animated: true, completion: nil)
                // self.present(mailComposeViewController, animated: true, completion: nil)
            }
            else
            {
                self.showSendMailErrorAlert()
            }
        }
    }
  
    @IBAction func btnMsgDriverAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let mailComposeViewController = configuredMailComposeViewController()
            if MFMailComposeViewController.canSendMail()
            {
                self.present(mailComposeViewController, animated: true, completion: nil)
                // self.present(mailComposeViewController, animated: true, completion: nil)
            }
            else
            {
                self.showSendMailErrorAlert()
            }
        }
        else
        {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud?.color = UIColor.clear
            
            let phone = dictDriverData["mobile_number"].stringValue
            
            var phoneStr: String = "telprompt://\(phone)"
            phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
            phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
            
            let urlPhone = URL(string: phoneStr)
            if UIApplication.shared.canOpenURL(urlPhone!)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
            else
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Calling is not available."), duration: ToastDuration)
                //BasicStuff.showAlertMessage(vc: self, titleStr: "CoolDrive", messageStr: "Call facility is not available!!!")
            }
            self.perform(#selector(self.PushToSalonListController), with: nil, afterDelay: 0.5)
            
        }
    }
    
    //MARK: - Private Method
    
    func PushToSalonListController()
    {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController
    {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([dictDriverData["email"].stringValue])
        // mailComposerVC.setSubject("Sending you an in-app e-mail...")
        //  mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert()
    {
        KSToastView.ks_showToast(mapping.string(forKey: "Email was not send. Please send your email settings."), duration: ToastDuration)
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(String(describing: error?.localizedDescription))")
        }

        controller.dismiss(animated: true, completion: nil)
    }
   
}
