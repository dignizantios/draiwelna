//
//  DriverHomeViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 28/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView
import Firebase
import FirebaseDatabase
import GeoFire
import JLocationKit
import CoreLocation

class DriverHomeViewController: UIViewController
{
    
    //MARK:- Varible Declaration
    
    var strDriveStatus = String()
    var strRideID = String()
    var strAddress = String()
    var strLatitude = String()
    var strLongitude = String()
    let location: LocationManager = LocationManager()
    var isAnyRide = Bool()
    var origin = CLLocationCoordinate2D()
    var destination = CLLocationCoordinate2D()
    var dictData:JSON!
    var arrMarkers = [GMSMarker]()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var ViewOfGoogleMaps: GMSMapView!
    @IBOutlet var btnShowPastRideOutlet: UIButton!
    @IBOutlet var btnDriveLogoutOutlet: UIButton!
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var lblCustomerName: UILabel!
    @IBOutlet var btnEndRideOutlet: UIButton!
    @IBOutlet var btnStartRideOutlet: UIButton!
    @IBOutlet var viewOfLocationInfo: ShadowView!
    
    //English
    
    @IBOutlet var viewOfEnglish: UIView!
    @IBOutlet var lblPickupLocationEN: UILabel!
    @IBOutlet var lblDropLocationEN: UILabel!
    
    // Arabic
    
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var lblDropLocationAR: UILabel!
    @IBOutlet var lblPickupLocationAR: UILabel!
    
    

    //MARK:- ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.navigationController?.isNavigationBarHidden = true
        HideGestture()
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
      
        location.requestAccess = .requestAlwaysAuthorization
        location.requestAccess = .requestWhenInUseAuthorization
        
        self.viewOfLocationInfo.isHidden = false
        self.btnEndRideOutlet.isHidden = false
        self.btnStartRideOutlet.isHidden = false
        
        self.lblScreenTitle.text = mapping.string(forKey: "Home")
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnStartRideOutlet.setTitle(mapping.string(forKey: "Start"), for: .normal)
            self.btnEndRideOutlet.setTitle(mapping.string(forKey: "End"), for: .normal)
            
            self.btnDriveLogoutOutlet.setImage(UIImage.init(named: "ic_logout_sidemenu.png"), for: .normal)
            self.btnShowPastRideOutlet.setImage(UIImage.init(named: "ic_history.png"), for: .normal)
            
            self.viewOfEnglish.isHidden = false
            self.viewOfArabic.isHidden = true
           
        }
        else
        {
            self.btnStartRideOutlet.setTitle(mapping.string(forKey: "End"), for: .normal)
            self.btnEndRideOutlet.setTitle(mapping.string(forKey: "Start"), for: .normal)
            
            self.viewOfEnglish.isHidden = true
            self.viewOfArabic.isHidden = false
            
            self.btnDriveLogoutOutlet.setImage(UIImage.init(named: "ic_history.png"), for: .normal)
            self.btnShowPastRideOutlet.setImage(UIImage.init(named: "ic_logout_sidemenu.png"), for: .normal)
            
            self.btnStartRideOutlet.backgroundColor = UIColor.init(red: 188/255, green: 31/255, blue: 47/255, alpha: 1.0)
            self.btnEndRideOutlet.backgroundColor = UIColor.init(red: 31/255, green: 188/255, blue: 42/255, alpha: 1.0)           
           
        }

        strDriveStatus = "0"
        strLatitude = ""
        strLongitude = ""
        strRideID = ""
        strAddress = ""
        getDriveRide()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getCurrentRideRefreh), name: NSNotification.Name(rawValue: "getCurrentDiverRide"), object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnShowPastRideAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let obj:PastRideViewController = PastRideViewController(nibName: "PastRideViewController", bundle: nil)
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else
        {
            let alert = UIAlertController(title: AppName, message: mapping.string(forKey: "Are you sure want to logout?"), preferredStyle: UIAlertControllerStyle.alert);
            let cancelAction = UIAlertAction(title: mapping.string(forKey: "No"), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
                
            }
            let okAction = UIAlertAction(title: mapping.string(forKey: "Yes"), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
               self.userLogout()       
                
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }

    }

    @IBAction func btnDriverLogoutAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let alert = UIAlertController(title: AppName, message: mapping.string(forKey: "Are you sure want to logout?"), preferredStyle: UIAlertControllerStyle.alert);
            let cancelAction = UIAlertAction(title: mapping.string(forKey: "No"), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
                
            }
            let okAction = UIAlertAction(title: mapping.string(forKey: "Yes"), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                print("OK")
                self.userLogout()
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let obj:PastRideViewController = PastRideViewController(nibName: "PastRideViewController", bundle: nil)
            self.navigationController?.pushViewController(obj, animated: true)
        }

    }
    @IBAction func btnEndRideAction(_ sender: Any)
    {
        UserDefaults.standard.set("1", forKey: "isRideStart")
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.strDriveStatus = "3"
            self.update()
        }
        else
        {
            self.strDriveStatus = "1"
            self.update()
            self.btnEndRideOutlet.isUserInteractionEnabled = false
        }

    }
    @IBAction func btnStartRideAction(_ sender: Any)
    {
        UserDefaults.standard.set("1", forKey: "isRideStart")
     
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.strDriveStatus = "1"
            self.update()
            self.btnStartRideOutlet.isUserInteractionEnabled = false
        }
        else
        {
            self.strDriveStatus = "3"
            self.update()
        }

    }
    
    //MARK:- Private Method
    
    func getCurrentRideRefreh()
    {
        strDriveStatus = "0"
        strLatitude = ""
        strLongitude = ""
        strRideID = ""
        strAddress = ""
        getDriveRide()
    }
    
    func openSetting()
    {
        let alertController = UIAlertController (title: AppName, message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    private func attributedString(str1:String,str2:String) -> NSAttributedString?
    {
        let attributes1 = [
            
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont(name: "OpenSans", size: 16)!
            ] as [String : Any]
        
        let attributes2 = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 22)!
            ] as [String : Any]
        
        let attributedString1 = NSAttributedString(string: str1, attributes: attributes1)
        let attributedString2 = NSAttributedString(string: str2, attributes: attributes2)
        
        let FormatedString = NSMutableAttributedString()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            FormatedString.append(attributedString1)
            FormatedString.append(attributedString2)
        }
        else
        {
            FormatedString.append(attributedString2)
            FormatedString.append(attributedString1)
        }
        return FormatedString
    }
    
    //MARK:- CLLocationManager Delegate
    
    func currentLocationIfNoRide()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        location.requestAccess = .requestAlwaysAuthorization
        location.getLocation(detectStyle: .Once, completion: { (loc) in
            print("latitude ",loc.currentLocation.coordinate.latitude.description)
            print("longitude ",loc.currentLocation.coordinate.longitude.description)
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            if self.isAnyRide == true
            {
                self.ViewOfGoogleMaps.clear()
                self.drawPath()
                let dictLocationInfo = self.dictData["location_info"].dictionaryValue
                self.setUpPinOnMap(lat: (dictLocationInfo["source_lat"]?.doubleValue)!, long: (dictLocationInfo["source_lng"]?.doubleValue)!, type: 1)
                self.setUpPinOnMap(lat: (dictLocationInfo["desti_lat"]?.doubleValue)!, long: (dictLocationInfo["desti_lng"]?.doubleValue)!, type: 2)
                self.setUpPinOnMap(lat: loc.currentLocation.coordinate.latitude, long: loc.currentLocation.coordinate.longitude, type: 3)
                let camera = GMSCameraPosition.camera(withLatitude: loc.currentLocation.coordinate.latitude, longitude: loc.currentLocation.coordinate.longitude, zoom: 12)
                self.ViewOfGoogleMaps.animate(to: camera)
            }
            else
            {
                self.ViewOfGoogleMaps.clear()
                self.setUpPinOnMap(lat: loc.currentLocation.coordinate.latitude, long: loc.currentLocation.coordinate.longitude, type: 3)
                let camera = GMSCameraPosition.camera(withLatitude: loc.currentLocation.coordinate.latitude, longitude: loc.currentLocation.coordinate.longitude, zoom: 20)
                self.ViewOfGoogleMaps.animate(to: camera)
            }
           
        }, error: { (error) in
            //optional
            print("error \(error)")
        },authorizationChange: { (status) in
            //optional
            switch status {
            case .notDetermined:
                // If status has not yet been determied, ask for authorization
                self.location.requestAccess = .requestWhenInUseAuthorization
                break
            case .authorizedWhenInUse:
                break
            case .authorizedAlways:
                break
            case .restricted:
                // If restricted by e.g. parental controls. User can't enable Location Services
                self.openSetting()
                break
            case .denied:
                // If user denied your app access to Location Services, but can grant access from Settings.app
                self.openSetting()
                break
                //   default:
                // break
            }

        }
        )
    }
    
    func update()
    {
        location.requestAccess = .requestAlwaysAuthorization
        location.getLocation(detectStyle: .SignificantLocationChanges, completion: { (loc) in
            print("latitude ",loc.currentLocation.coordinate.latitude.description)
            print("longitude ",loc.currentLocation.coordinate.longitude.description)
            if self.strDriveStatus == "1" || self.strDriveStatus == "3"
            {
                self.strLatitude = String(loc.currentLocation.coordinate.latitude)
                self.strLongitude = String(loc.currentLocation.coordinate.longitude)
                self.getDriveRide()
                if self.strDriveStatus == "1"
                {
                    self.origin.latitude = loc.currentLocation.coordinate.latitude
                    self.origin.longitude = loc.currentLocation.coordinate.longitude
                }
                
            }
            self.UpdateCurrentLocation(latitude: loc.currentLocation.coordinate.latitude, longitude: loc.currentLocation.coordinate.longitude)
           // self.ViewOfGoogleMaps.clear()
           // self.drawPath()
         
            for i in 0..<self.arrMarkers.count
            {
                self.arrMarkers[i].map = nil
            }
            self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude, type: 1)
            print("latitude:- \(self.destination.latitude)  longitude :- \(self.destination.longitude)")
            self.setUpPinOnMap(lat: self.destination.latitude, long: self.destination.longitude, type: 2)
            self.setUpPinOnMap(lat: loc.currentLocation.coordinate.latitude, long: loc.currentLocation.coordinate.longitude, type: 3)
            let target = CLLocationCoordinate2D(latitude: loc.currentLocation.coordinate.latitude, longitude: loc.currentLocation.coordinate.longitude)
           // self.ViewOfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: target, zoom: 20)
            let update = GMSCameraUpdate.setTarget(target)
            self.ViewOfGoogleMaps.moveCamera(update)
            KSToastView.ks_showToast("Location Updated:- Latitude \(String(loc.currentLocation.coordinate.latitude)) Longitude:- \(String(loc.currentLocation.coordinate.longitude))", duration: ToastDuration)
            if self.strDriveStatus == "3"
            {
                 self.location.stopAll()
            }
        }, error: { (error) in
            print("error \(error)")
            //optional
        }, authorizationChange: { (status) in
            //optional
            switch status {
            case .notDetermined:
                // If status has not yet been determied, ask for authorization
                self.location.requestAccess = .requestWhenInUseAuthorization
                self.location.requestAccess = .requestAlwaysAuthorization
                break
            case .authorizedWhenInUse:
                break
            case .authorizedAlways:
                break
            case .restricted:
                // If restricted by e.g. parental controls. User can't enable Location Services
                 self.openSetting()
                break
            case .denied:
                // If user denied your app access to Location Services, but can grant access from Settings.app
                  self.openSetting()
                break
                //   default:
                // break
            }
        })
    }
    
    //MARK:- Reverse gecode and get local address from Latitude and longitude
    
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                let lines = address.lines! as [String]
                currentAddress = lines.joined(separator: ",")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
                self.strAddress = currentAddress
                print("Address - ",currentAddress)
            }
        }
        return currentAddress
    }
    
    
    //MARK:- Set Pin On Map
    
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
       // let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 8)
      //  ViewOfGoogleMaps.animate(to: camera)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
       // let Address = reverseGeoCoding(Latitude: lat, Longitude: long)
       // marker.title = Address
        if type == 1
        {
            marker.icon = UIImage.init(named: "ic_user_pin_red.png")
        }
        else if type == 2
        {
            marker.icon = UIImage.init(named: "ic_user_pin_gray.png")
        }
        else
        {
            marker.icon = UIImage.init(named: "pin_pickup_location.png")
            arrMarkers.append(marker)
        }
        
        marker.map = ViewOfGoogleMaps
    }

    
    //MARK:- Service
    
    func getDriveRide()
    {
        let kRegiURL = "\(DraiwelnaDriverURL)manage_current_ride"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param = ["driver_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" :String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "flag_view":strDriveStatus,
                         "latitude":strLatitude,
                         "longitude":strLongitude,
                         "ride_id":strRideID]
                        // "address":strAddress]
            
            
            if self.strDriveStatus != "2"
            {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            
            if self.strDriveStatus == "3"
            {
                if let loadedCart = UserDefaults.standard.array(forKey: "CurrentRideData") as? [[String: Any]]
                {
                    if let theJSONData = try? JSONSerialization.data(withJSONObject: loadedCart,options: [])
                    {
                        if let theJSONText = String(data: theJSONData,encoding: .ascii)
                        {
                            print("JSON string = \(theJSONText)")
                            param["waypoint"] = theJSONText
                        }
                    }
                }
                print("param \(param)")
            }
            
            print("param \(param)")
          
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                
                
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print("json \(json)")
                    
                    if json["flag"].stringValue == "4"
                    {
                        print(json)
                        if self.strDriveStatus == "0"
                        {
                            self.viewOfLocationInfo.isHidden = false
                            self.btnEndRideOutlet.isHidden = false
                            self.btnStartRideOutlet.isHidden = false
                            self.dictData = json["data"]
                            
                            let userInfo = self.dictData["user_info"].dictionaryValue
                            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                            {
                                self.lblCustomerName.attributedText = self.attributedString(str1: "CUSTOMER: ", str2: (userInfo["user_name"]?.stringValue)!)
                            }
                            else
                            {
                                 self.lblCustomerName.attributedText = self.attributedString(str1: ":العملاء", str2: "\((userInfo["user_name"]?.stringValue)!) ")
                            }
                            
                            self.strRideID = (self.dictData["id"].stringValue)
                            let dictLocationInfo = self.dictData["location_info"].dictionaryValue
                          /*  self.strLatitude = (dictLocationInfo["source_lat"]?.stringValue)!
                            self.strLongitude = (dictLocationInfo["source_lng"]?.stringValue)!
                            self.strAddress = (dictLocationInfo["source_address"]?.stringValue)!*/
                            
                            self.origin.latitude = (dictLocationInfo["source_lat"]?.doubleValue)!
                            self.origin.longitude = (dictLocationInfo["source_lng"]?.doubleValue)!
                            
                            /*let camera = GMSCameraPosition.camera(withLatitude: (dictLocationInfo["source_lat"]?.doubleValue)!, longitude: (dictLocationInfo["source_lng"]?.doubleValue)!, zoom: 8)
                            self.ViewOfGoogleMaps.animate(to: camera)*/
                            
                            self.destination.latitude = (dictLocationInfo["desti_lat"]?.doubleValue)!
                            self.destination.longitude = (dictLocationInfo["desti_lng"]?.doubleValue)!
                            print("latitude:- \(self.destination.latitude)  longitude :- \(self.destination.longitude)")
                                
                            self.setUpPinOnMap(lat: (dictLocationInfo["source_lat"]?.doubleValue)!, long: (dictLocationInfo["source_lng"]?.doubleValue)!, type: 1)
                            self.setUpPinOnMap(lat: (dictLocationInfo["desti_lat"]?.doubleValue)!, long: (dictLocationInfo["desti_lng"]?.doubleValue)!, type: 2)
                            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                            {
                                self.lblPickupLocationEN.text = (dictLocationInfo["source_address"]?.stringValue)!
                                self.lblDropLocationEN.text = (dictLocationInfo["desti_address"]?.stringValue)!
                            }
                            else
                            {
                                self.lblPickupLocationAR.text = (dictLocationInfo["source_address"]?.stringValue)!
                                self.lblDropLocationAR.text = (dictLocationInfo["desti_address"]?.stringValue)!
                            }
                            self.isAnyRide = true
                            self.drawPath()
                            
                            if self.dictData["ride_type"].stringValue == "2"
                            {
                                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                                {
                                    self.btnStartRideOutlet.isUserInteractionEnabled = false
                                }
                                else
                                {
                                     self.btnEndRideOutlet.isUserInteractionEnabled = false
                                }
                                UserDefaults.standard.set("1", forKey: "isRideStart")
                                self.update()
                                
                            }
                            else
                            {
                                self.btnStartRideOutlet.isUserInteractionEnabled = true
                                self.btnEndRideOutlet.isUserInteractionEnabled = true
                                UserDefaults.standard.set("0", forKey: "isRideStart")
                                self.currentLocationIfNoRide()
                            }
                          
                        }
                        else
                        {
                           // KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                        }
                    }
                    else if json["flag"].stringValue == "1"
                    {
                        UserDefaults.standard.set("1", forKey: "isRideStart")
                        self.strDriveStatus = "2"
                        self.isAnyRide = true
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    else if json["flag"].stringValue == "2"
                    {
                        UserDefaults.standard.set("1", forKey: "isRideStart")
                        self.isAnyRide = true
                        self.strDriveStatus = "2"
                        let locationInfo = json["data"].dictionaryValue
                        let arrWayPoints = locationInfo["waypoint"]?.arrayValue
                        let totalWayPoints:Int = arrWayPoints!.count - 1
                        for i in 0..<totalWayPoints
                        {
                            let dictFirst = arrWayPoints?[i].dictionaryValue
                            let dictSecond = arrWayPoints?[i+1].dictionaryValue
                        }

                    }
                    else if json["flag"].stringValue == "3"
                    {
                        self.location.stopAll()
                        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                        {
                            self.btnStartRideOutlet.isUserInteractionEnabled = true
                        }
                        else
                        {
                            self.btnEndRideOutlet.isUserInteractionEnabled = true
                        }
                        UserDefaults.standard.set("0", forKey: "isRideStart")
                        UserDefaults.standard.removeObject(forKey: "CurrentRideData")
                        UserDefaults.standard.synchronize()
                        self.isAnyRide = false
                        self.viewOfLocationInfo.isHidden = true
                        self.lblCustomerName.text = json["msg"].stringValue
                        self.btnEndRideOutlet.isHidden = true
                        self.btnStartRideOutlet.isHidden = true
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                        
                        let locationInfo = json["data"].dictionaryValue
                        let arrWayPoints = locationInfo["waypoint"]!.arrayValue
                        let path = GMSMutablePath()
                        self.ViewOfGoogleMaps.clear()
                        for i in 0..<arrWayPoints.count
                        {
                            let dictFirst = arrWayPoints[i].dictionaryValue
                            path.add(CLLocationCoordinate2D(latitude: (dictFirst["way_lat"]!.doubleValue), longitude: (dictFirst["way_lng"]?.doubleValue)!))
                            if i == 0
                            {
                                self.setUpPinOnMap(lat: dictFirst["way_lat"]!.doubleValue, long: dictFirst["way_lng"]!.doubleValue,type: 1)
                            }
                            else if i == (arrWayPoints.count - 1)
                            {
                                self.setUpPinOnMap(lat: dictFirst["way_lat"]!.doubleValue, long: dictFirst["way_lng"]!.doubleValue,type: 2)
                            }
                        }
                        let rectangle = GMSPolyline(path: path)
                        rectangle.strokeColor = MySingleton.sharedManager.themeColor
                        rectangle.strokeWidth = 3
                        rectangle.map = self.ViewOfGoogleMaps
                        let bounds = GMSCoordinateBounds(path: path)
                        self.ViewOfGoogleMaps!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                        
                        let geofireRef = Database.database().reference()
                        let geoFire = GeoFire(firebaseRef: geofireRef)                        
                        geoFire?.removeKey("driver-\(UserDefaults.standard.value(forKey: "UserId") as! String)")

                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                        if self.strDriveStatus == "3"
                        {
                            let geofireRef = Database.database().reference()
                            let geoFire = GeoFire(firebaseRef: geofireRef)
                            geoFire?.removeKey("driver-\(UserDefaults.standard.value(forKey: "UserId") as! String)")
                            self.location.stopAll()
                        }
                        if self.strDriveStatus == "1"
                        {
                            self.location.stopAll()
                            self.isAnyRide = false
                            self.viewOfLocationInfo.isHidden = true
                            self.lblCustomerName.text = json["msg"].stringValue
                            self.btnEndRideOutlet.isHidden = true
                            self.btnStartRideOutlet.isHidden = true
                            self.currentLocationIfNoRide()
                          //  self.strDriveStatus = "0"
                            let geofireRef = Database.database().reference()
                            let geoFire = GeoFire(firebaseRef: geofireRef)
                            geoFire?.removeKey("driver-\(UserDefaults.standard.value(forKey: "UserId") as! String)")

                        }

                        if self.strDriveStatus != "1"
                        {
                            UserDefaults.standard.set("0", forKey: "isRideStart")
                            self.isAnyRide = false
                            self.viewOfLocationInfo.isHidden = true
                            self.lblCustomerName.text = json["msg"].stringValue
                            self.btnEndRideOutlet.isHidden = true
                            self.btnStartRideOutlet.isHidden = true
                            self.currentLocationIfNoRide()
                            self.strDriveStatus = "0"
                            let geofireRef = Database.database().reference()
                            let geoFire = GeoFire(firebaseRef: geofireRef)
                            geoFire?.removeKey("driver-\(UserDefaults.standard.value(forKey: "UserId") as! String)")
                        }
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    
    func drawPath()
    {
        
        let origin:String = "\(self.origin.latitude),\(self.origin.longitude)"
        let destination:String = "\(self.destination.latitude),\(self.destination.longitude)"      
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(GoogleMapsDrawPathKey)"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url).responseJSON { response in
                
                let json = JSON(data: response.data!)
               // print("json: \(json)")
                let routes = json["routes"].arrayValue
                if routes.count == 0
                {
                    return
                }
              // print("routes: \(routes)")
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                  //  print("routeOverviewPolyline: \(routeOverviewPolyline)")
                    let points = routeOverviewPolyline?["points"]?.stringValue
                  //  print("points: \(points)")
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.map = self.ViewOfGoogleMaps
                    polyline.strokeColor = MySingleton.sharedManager.themeColor
                    polyline.strokeWidth = 3
                   // let bounds = GMSCoordinateBounds(path: path!)
                   // self.ViewOfGoogleMaps!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100))
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
        
    }
    
    //MARK:- Logout
    
    func userLogout()
    {
        let kRegiURL = "\(DraiwelnaMainURL)logout"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "device_token" : UserDefaults.standard.value(forKey: "deviceToken") as! String,
                         "user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "user_type":UserDefaults.standard.value(forKey: "isDriverLogin") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String]
            
            print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                       // KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                        UserDefaults.standard.removeObject(forKey: "UserId")
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as!ViewController
                        let navigationController = NavigationController(rootViewController: nextViewController)
                        
                        let mainViewController = MainViewController()
                        mainViewController.rootViewController = navigationController
                        mainViewController.setup(type: 6)
                        
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = mainViewController
                        
                        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }



}
