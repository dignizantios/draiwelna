//
//  EditAccountViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView
import CoreLocation
import GoogleMaps

class EditAccountViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,CLLocationManagerDelegate
{
    //MARK:- Variable Declaration
    
    var picker:UIImagePickerController?=UIImagePickerController()
    var strCheckImage = String()
    var dictUserData:[String : JSON] = [:]
    let locationManager = CLLocationManager()
    var didFindLocation = Bool()
    var strCountryCode = String()
    
    //MARK:- Outlet Zone
    
    //Common
    
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var imgOfUserProfilePicture: UIImageView!
    @IBOutlet var btnDoneOutlet: ShadowButton!
    @IBOutlet var btnChangePasswordOutlet: ShadowButton!
    
    
    //English
    @IBOutlet var viewOfEnglish: UIView!
    @IBOutlet var btnBackOutletEN: UIButton!
    @IBOutlet var txtUserAccountEN: UITextField!
    @IBOutlet var txtUserNameEN: UITextField!
    @IBOutlet var txtMobileNumberEN: UITextField!
    @IBOutlet var txtUserEmailEN: UITextField!
    @IBOutlet var txtUserLastNameEN: UITextField!
    
    //Arabic
    @IBOutlet var btnBackOutletAR: UIButton!
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var txtUserAcoountAR: UITextField!
    @IBOutlet var txtUserNameAR: UITextField!
    @IBOutlet var txtMobileNumberAR: UITextField!
    @IBOutlet var txtUserEmailAR: UITextField!
    @IBOutlet var txtUserLastNameAR: UITextField!
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
       

        self.lblScreenTitle.text = mapping.string(forKey: "Edit Account")
        self.btnChangePasswordOutlet.setTitle(mapping.string(forKey: "Change password"), for: .normal)
        self.btnDoneOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
       
        
        self.imgOfUserProfilePicture.layer.cornerRadius = self.imgOfUserProfilePicture.frame.size.height/2
        self.imgOfUserProfilePicture.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
        self.imgOfUserProfilePicture.layer.borderWidth = 2
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.viewOfEnglish.isHidden = false
            self.viewOfArabic.isHidden = true
            
            self.btnBackOutletEN.isHidden = false
            self.btnBackOutletAR.isHidden = true
            
            [txtUserAccountEN,txtUserNameEN,txtMobileNumberEN,txtUserEmailEN,txtUserLastNameEN].forEach({ customizeTextFieldEN(textfield: $0) })
            
            let strImage = self.dictUserData["profile_image"]?.stringValue
            if strImage == ""
            {
                 strCheckImage = "0"
            }
            else
            {
                strCheckImage = "1"
            }
            let strPlaceImage="user_placeholder_register_screen.png"
            let urlImage:NSURL = NSURL(string: strImage!)!
            self.imgOfUserProfilePicture.sd_setImage(with: urlImage as URL, placeholderImage:UIImage(named:strPlaceImage))
            
            self.txtUserAccountEN.text = self.dictUserData["first_name"]?.stringValue
            self.txtUserNameEN.text = self.dictUserData["username"]?.stringValue
            self.txtMobileNumberEN.text = self.dictUserData["mobile_number"]?.stringValue
            self.txtUserEmailEN.text = self.dictUserData["email"]?.stringValue
            self.txtUserLastNameEN.text = self.dictUserData["last_name"]?.stringValue
        }
        else
        {
            
            self.viewOfEnglish.isHidden = true
            self.viewOfArabic.isHidden = false
            
            self.btnBackOutletEN.isHidden = true
            self.btnBackOutletAR.isHidden = false
            
            [txtUserAcoountAR,txtUserNameAR,txtMobileNumberAR,txtUserEmailAR,txtUserLastNameAR].forEach({ customizeTextFieldAR(textfield: $0) })
            
            let strImage = self.dictUserData["profile_image"]?.stringValue
            if strImage == ""
            {
                strCheckImage = "0"
            }
            else
            {
                strCheckImage = "1"
            }
            let strPlaceImage="user_placeholder_register_screen.png"
            let urlImage:NSURL = NSURL(string: strImage!)!
            self.imgOfUserProfilePicture.sd_setImage(with: urlImage as URL, placeholderImage:UIImage(named:strPlaceImage))
            
            self.txtUserAcoountAR.text = self.dictUserData["first_name"]?.stringValue
            self.txtUserNameAR.text = self.dictUserData["username"]?.stringValue
            self.txtMobileNumberAR.text = self.dictUserData["mobile_number"]?.stringValue
            self.txtUserEmailAR.text = self.dictUserData["email"]?.stringValue
            self.txtUserLastNameAR.text = self.dictUserData["last_name"]?.stringValue
        }
        
        HideGestture()
        
        locationManager.delegate = self
       
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Memory Management

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        textfield.layer.borderWidth = 1
      //  textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        textfield.layer.borderWidth = 1
        //textfield.setValue(UIColor.init(red: 1, green: 1, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if textField == self.txtUserAccountEN
            {
                self.txtUserNameEN.becomeFirstResponder()
            }
            else if textField == self.txtUserNameEN
            {
                self.txtUserNameEN.becomeFirstResponder()
            }
            else if textField == self.txtUserNameEN
            {
                self.txtMobileNumberEN.becomeFirstResponder()
            }
            else if textField == self.txtUserEmailEN
            {
                self.txtUserEmailEN.resignFirstResponder()
            }
        }
        else
        {
            if textField == self.txtUserAcoountAR
            {
                self.txtUserNameAR.becomeFirstResponder()
            }
            else if textField == self.txtUserNameAR
            {
                self.txtUserNameAR.becomeFirstResponder()
            }
            else if textField == self.txtUserNameAR
            {
                self.txtMobileNumberAR.becomeFirstResponder()
            }
            else if textField == self.txtUserEmailAR
            {
                self.txtUserEmailAR.resignFirstResponder()
            }
        }
       
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNumberEN ||  textField == txtMobileNumberAR
        {
            let limitLength = 8
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength // Bool
        }
        return true
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnChooseProfilePictureAction(_ sender: Any)
    {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: mapping.string(forKey: "Choose option"), preferredStyle: .actionSheet)
        
        // 2
        let galleryAction = UIAlertAction(title:mapping.string(forKey: "Select from library") , style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.openGallary()
        })
        let camaraAction = UIAlertAction(title: mapping.string(forKey: "Take a Photo"), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.openCamera()
        })
        
        //
        let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel"), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        // 4
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(camaraAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    @IBAction func btnChangePasswordAction(_ sender: Any)
    {
        let  obj:ChangePasswordViewController = ChangePasswordViewController(nibName: "ChangePasswordViewController", bundle: nil)
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
    }

    @IBAction func btnDoneAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let trimmedUserName = self.txtUserNameEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserNameEN.text = trimmedUserName
            
            let trimmedUserLastName = self.txtUserLastNameEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserLastNameEN.text = trimmedUserLastName
            
            let trimmedUserFirstName = self.txtUserAccountEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserAccountEN.text = trimmedUserFirstName
            
            let trimmedUserEmail = self.txtUserEmailEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserEmailEN.text = trimmedUserEmail
            
            if self.txtUserNameEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your first name"), duration: ToastDuration)
            }
            else if self.txtUserLastNameEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your last name"), duration: ToastDuration)
            }
           /* else if strCheckImage == "0"
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select profile picture"), duration: ToastDuration)
            }*/
            else if self.txtUserAccountEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserEmailEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter email address"), duration: ToastDuration)
            }
            else
            {
                let emailid: String = self.txtUserEmailEN.text!
                let myStringMatchesRegEx: Bool = self.isValidEmail(emailAddressString: emailid)
                if myStringMatchesRegEx == false
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter valid email address"), duration: ToastDuration)
                }
                else if self.txtMobileNumberEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter your mobile number"), duration: ToastDuration)
                }
                else if (self.txtMobileNumberEN.text?.characters.count)! < 8
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter a valid 8-digit mobile number"), duration: ToastDuration)
                }
                else
                {
                    self.view.endEditing(true)
                    saveUserProfileDataEnglish()
                }
            }
            
        }
        else
        {
            let trimmedUserName = self.txtUserNameAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserNameAR.text = trimmedUserName
            
            let trimmedUserLastName = self.txtUserLastNameAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserLastNameAR.text = trimmedUserLastName
            
            let trimmedUserFirstName = self.txtUserAcoountAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserAcoountAR.text = trimmedUserFirstName
            
            let trimmedUserEmail = self.txtUserEmailAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserEmailAR.text = trimmedUserEmail
            
            if self.txtUserNameAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your first name"), duration: ToastDuration)
            }
            else if self.txtUserLastNameAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your last name"), duration: ToastDuration)
            }
            /*else if strCheckImage == "0"
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select profile picture"), duration: ToastDuration)
            }*/
            else if self.txtUserAcoountAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserEmailAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter email address"), duration: ToastDuration)
            }
            else
            {
                let emailid: String = self.txtUserEmailAR.text!
                let myStringMatchesRegEx: Bool = self.isValidEmail(emailAddressString: emailid)
                if myStringMatchesRegEx == false
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter valid email address"), duration: ToastDuration)
                }
                else if self.txtMobileNumberAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter your mobile number"), duration: ToastDuration)
                }
                else if (self.txtMobileNumberAR.text?.characters.count)! < 8
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter a valid 8-digit mobile number"), duration: ToastDuration)
                }
                else
                {
                    self.view.endEditing(true)
                    saveUserProfileDataArabic()
                }
            }
            
        }
    }
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
  
    
    //MARK:- CLLocationManager Delegate
    
    func openSetting()
    {
        let alertController = UIAlertController (title: AppName, message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func setupUserCurrentLocation()
    {
        didFindLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }

    //MARK:- Update Location
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if didFindLocation == true
        {
            didFindLocation = false
           // let currentLocation:CLLocationCoordinate2D = manager.location!.coordinate
            
         //   print("locations = \(currentLocation.latitude) \(currentLocation.longitude)")
            guard let currentLocation = locations.first else { return }
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
                guard let currentLocPlacemark = placemarks?.first else { return }
                print(currentLocPlacemark.country ?? "No country found")
                print(currentLocPlacemark.isoCountryCode ?? "No country code found")
                self.strCountryCode = self.getCountryPhonceCode(currentLocPlacemark.isoCountryCode!)
            }
       
        }
        else
        {
            didFindLocation = false
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        didFindLocation = false
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            setupUserCurrentLocation()
            // manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            openSetting()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            openSetting()
            break
            //   default:
            // break
        }
    }

    //MARK:- Reverse gecode and get local address from Latitude and longitude
    
   /* func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = CLGeocoder()
        var coordinate = CLLocationCoordinate2D()
        coordinate.latitude = Latitude
        coordinate.longitude = Longitude
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeLocation(coordinate) { (placemarks, error) in
            guard let currentLocPlacemark = placemarks?.first else { return }
            print(currentLocPlacemark.country ?? "No country found")
            print(currentLocPlacemark.isoCountryCode ?? "No country code found")
        }
      /*  geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            print("response \(String(describing: response))")
            if let address = response?.firstResult()
            {
                print("response \(String(describing: response?.results()))")
               
                print("address - ",address)
                
                let lines = address.lines! as [String]
                
                currentAddress = lines.joined(separator: ",")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
                
                print("Address - ",currentAddress)
                
            }
        }*/
        return currentAddress
    }*/
    
    //MARK: - ImagePicker Delegate Methods
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        strCheckImage = "0"
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        //  imgOfUserProfileEN.contentMode = .scaleAspectFit
        imgOfUserProfilePicture.image = chosenImage        
        strCheckImage = "1"
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Select Picture Methods
    
    func openGallary()
    {
        picker?.delegate = self
        picker!.allowsEditing = true
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker?.delegate = self
            picker!.allowsEditing = true
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: AppName, message: mapping.string(forKey: "This device has no Camera"), preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Service
    
    func saveUserProfileDataEnglish()
    {
        let kRegiURL = "\(DraiwelnaMainURL)save_user_profile"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param:[String:String] = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "flag_view":"1",
                         "first_name" : self.txtUserAccountEN.text!,
                         "username" : self.txtUserNameEN.text!,
                         "last_name":self.txtUserLastNameEN.text!,
                         "email": self.txtUserEmailEN.text!,
                         "mobile_number" : self.txtMobileNumberEN.text!,
                         "country_code" : strCountryCode]
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            let PasswordString =  String(format: "\(basic_username):\(basic_password)") // "usr_easyjob:3a8wGTszSJWU2J99"
            let PasswordData = PasswordString.data(using: .utf8)
            let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
            
            let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
            print("headers:==\(headers)")
            
            let unit64:UInt64 = 10_000_000
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if let img = self.imgOfUserProfilePicture.image {
                    let imgData = UIImageJPEGRepresentation(img, 0.7)
                    multipartFormData.append(imgData!, withName: "profile_image", fileName:"image" , mimeType: "image/png")
                }
                
                for (key, value) in param
                {
                    print("\(key) \(value)")
                    multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }, usingThreshold: unit64, to: kRegiURL, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
                print("encoding result:\(encodingResult)")
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //send progress using delegate
                    })
                    upload.responseSwiftyJSON(completionHandler: { (responce) in
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        print("response:==>\(responce)")
                        if let json = responce.result.value
                        {
                            print("json\(json)")
                            if json["flag"].stringValue == "1"
                            {
                                KSToastView.ks_showToast(json["msg"].stringValue, completion: { self.navigationController?.popViewController(animated: true)})
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfile"), object: nil)                                
                            }
                            else
                            {
                                KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                            }
                            
                        }
                        else
                        {
                        }
                        //completion(responce.result)
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            })
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func saveUserProfileDataArabic()
    {
        let kRegiURL = "\(DraiwelnaMainURL)save_user_profile"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param:[String:String] = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                                         "timezone": TimeZone.current.identifier,
                                         "flag_view":"1",
                                         "first_name" : self.txtUserAcoountAR.text!,
                                         "last_name":self.txtUserLastNameAR.text!,
                                         "username" : self.txtUserNameAR.text!,
                                         "email": self.txtUserEmailAR.text!,
                                         "mobile_number" : self.txtMobileNumberAR.text!,
                                         "country_code" : strCountryCode]
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            let PasswordString =  String(format: "\(basic_username):\(basic_password)") // "usr_easyjob:3a8wGTszSJWU2J99"
            let PasswordData = PasswordString.data(using: .utf8)
            let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
            
            let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
            print("headers:==\(headers)")
            
            let unit64:UInt64 = 10_000_000
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if let img = self.imgOfUserProfilePicture.image {
                    let imgData = UIImageJPEGRepresentation(img, 0.7)
                    multipartFormData.append(imgData!, withName: "profile_image", fileName:"image" , mimeType: "image/png")
                }
                
                for (key, value) in param
                {
                    print("\(key) \(value)")
                    multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }, usingThreshold: unit64, to: kRegiURL, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
                print("encoding result:\(encodingResult)")
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //send progress using delegate
                    })
                    upload.responseSwiftyJSON(completionHandler: { (responce) in
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        print("response:==>\(responce)")
                        if let json = responce.result.value
                        {
                            print("json\(json)")
                            if json["flag"].stringValue == "1"
                            {
                                KSToastView.ks_showToast(json["msg"].stringValue, completion: { self.navigationController?.popViewController(animated: true)})
                                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfile"), object: nil)
                            }
                            else
                            {
                               KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                            }
                            
                        }
                        else
                        {
                             KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                        }
                        //completion(responce.result)
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            })
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}
