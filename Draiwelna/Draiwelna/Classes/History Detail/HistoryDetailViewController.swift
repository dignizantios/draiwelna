//
//  HistoryDetailViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import GoogleMaps
import AlamofireSwiftyJSON
import SwiftyJSON
import Alamofire
import KSToastView
import Firebase
import FirebaseDatabase
import GeoFire

class HistoryDetailViewController: UIViewController,GMSMapViewDelegate
{
    //MARK:- Variable Declaration
    
    var dictData:JSON!
    var timer = Timer()
    var isAlready = Bool()
    var arrMarkers = [GMSMarker]()
    
    //MARK:- Outlet ZOne
    
    @IBOutlet var btnBackOuletEN: UIButton!
    @IBOutlet var btnBackOuletAR: UIButton!
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var viewfGoogleMaps: GMSMapView!
    
    //English
    @IBOutlet var viewOfEnglish: UIView!
    @IBOutlet var lblTotalPriceEN: UILabel!
    @IBOutlet var lblPickupLocationEN: UILabel!
    @IBOutlet var lblDropLocationEN: UILabel!
    
    //Arabic
    
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var lblTotalPriceAR: UILabel!
    @IBOutlet var lblPickupLocationAR: UILabel!
    @IBOutlet var lblDropLocationAR: UILabel!

    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
        HideGestture()
        print("dictData \(dictData)")
        
        self.lblScreenTitle.text = mapping.string(forKey: "History Details")
        let locationInfo = dictData["location_info"]
        let fairData = dictData["fair_data"]
        
       
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.lblPickupLocationEN.text = locationInfo["source_address"].stringValue
            self.lblDropLocationEN.text = locationInfo["desti_address"].stringValue
            
           // self.lblTotalPriceEN.text = "\(fairData["currency"].stringValue) \(String(format: "%.2f", fairData["fair_amount"].floatValue))"
            self.lblTotalPriceEN.attributedText = attributedString(str1: "\(fairData["currency"].stringValue) ", str2: (String(format: "%.2f", fairData["fair_amount"].floatValue)))
            
            self.viewOfArabic.isHidden = true
            self.viewOfEnglish.isHidden = false
            
            self.btnBackOuletAR.isHidden = true
            self.btnBackOuletEN.isHidden = false
        }
        else
        {
            self.lblPickupLocationAR.text = locationInfo["source_address"].stringValue
            self.lblDropLocationAR.text = locationInfo["desti_address"].stringValue
            self.lblTotalPriceAR.attributedText = attributedString(str1: "\(fairData["currency"].stringValue)", str2: "\((String(format: "%.2f", fairData["fair_amount"].floatValue))) ")
            
            self.viewOfArabic.isHidden = false
            self.viewOfEnglish.isHidden = true

            self.btnBackOuletAR.isHidden = false
            self.btnBackOuletEN.isHidden = true
        }
        
        if dictData["ride_type"].stringValue == "3"
        {
            let arrWayPoints = locationInfo["waypoint"].arrayValue
            let path = GMSMutablePath()
            self.viewfGoogleMaps.clear()
            for i in 0..<arrWayPoints.count
            {
                let dictFirst = arrWayPoints[i].dictionaryValue
                path.add(CLLocationCoordinate2D(latitude: (dictFirst["way_lat"]!.doubleValue), longitude: (dictFirst["way_lng"]?.doubleValue)!))
                if i == 0
                {
                    self.setUpPinOnMap(lat: dictFirst["way_lat"]!.doubleValue, long: dictFirst["way_lng"]!.doubleValue,type: 1)
                }
                else if i == (arrWayPoints.count - 1)
                {
                    self.setUpPinOnMap(lat: dictFirst["way_lat"]!.doubleValue, long: dictFirst["way_lng"]!.doubleValue,type: 2)
                }
            }
            let rectangle = GMSPolyline(path: path)
            rectangle.strokeColor = MySingleton.sharedManager.themeColor
            rectangle.strokeWidth = 3
            rectangle.map = self.viewfGoogleMaps
            let bounds = GMSCoordinateBounds(path: path)
            self.viewfGoogleMaps!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100))

        }
        else if dictData["ride_type"].stringValue == "2"
        {
            isAlready = true
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
           //getCurrentRide(strRideId: dictData["id"].stringValue)
        }
        else
        {
            self.viewfGoogleMaps.clear()
            drawPath()
            self.setUpPinOnMap(lat: locationInfo["source_lat"].doubleValue, long: locationInfo["source_lng"].doubleValue,type: 1)
            self.setUpPinOnMap(lat: locationInfo["desti_lat"].doubleValue, long: locationInfo["desti_lng"].doubleValue,type: 2)
            let camera = GMSCameraPosition.camera(withLatitude: locationInfo["source_lat"].doubleValue, longitude: locationInfo["source_lng"].doubleValue, zoom: 15)
            viewfGoogleMaps.animate(to: camera)
        }
       
       // getCurrentRide(strRideId: dictData["id"].stringValue)
       
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Managememt
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

    //MARK:- Private Method
    
    func update()
    {
        
        let geofireRef = Database.database().reference()
        let geoFire = GeoFire(firebaseRef: geofireRef)
        geoFire?.getLocationForKey("driver-\(dictData["driver_id"].stringValue)", withCallback: { (location, error) in
            if (error != nil) {
                print("An error occurred getting the location for driver-\(self.dictData["driver_id"].stringValue) : \(String(describing: error?.localizedDescription))")
            } else if (location != nil) {
                print("Location for driver-\(self.dictData["driver_id"].stringValue) is [\(String(describing: location?.coordinate.latitude)), \(String(describing: location?.coordinate.longitude))]")
                for i in 0..<self.arrMarkers.count
                {
                    self.arrMarkers[i].map = nil
                }
                let locationInfo = self.dictData["location_info"]
                self.setUpPinOnMap(lat: locationInfo["source_lat"].doubleValue, long: locationInfo["source_lng"].doubleValue,type: 1)
                self.setUpPinOnMap(lat: locationInfo["desti_lat"].doubleValue, long: locationInfo["desti_lng"].doubleValue,type: 2)
                self.setUpPinOnMap(lat: (location?.coordinate.latitude)!, long: (location?.coordinate.longitude)!,type: 3)
                let target = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
                //self.viewfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: target, zoom: 20)
                let update = GMSCameraUpdate.setTarget(target)
                self.viewfGoogleMaps.moveCamera(update)
                if self.isAlready == true
                {
                    self.isAlready = false
                    let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 20)
                    self.viewfGoogleMaps.animate(to: camera)
                }
                //KSToastView.ks_showToast("Location Updated:- Latitude \(String((location?.coordinate.latitude)!)) Longitude:- \(String((location?.coordinate.longitude)!))", duration: ToastDuration)
                
            } else {
                print("GeoFire does not contain a location for \"firebase-hq\"")
            }
        })
        
       //getCurrentRide(strRideId: dictData["id"].stringValue)
    }
    
    private func attributedString(str1:String,str2:String) -> NSAttributedString?
    {
        let attributes1 = [
            
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont(name: "OpenSans", size: 16)!
            ] as [String : Any]
        
        let attributes2 = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 22)!
            ] as [String : Any]
        
        let attributedString1 = NSAttributedString(string: str1, attributes: attributes1)
        let attributedString2 = NSAttributedString(string: str2, attributes: attributes2)
        
        let FormatedString = NSMutableAttributedString()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            FormatedString.append(attributedString1)
            FormatedString.append(attributedString2)
        }
        else
        {
            FormatedString.append(attributedString2)
            FormatedString.append(attributedString1)
        }
        
        return FormatedString
    }

    
    
    //MARK:- Action Zone

    @IBAction func btnReportAction(_ sender: Any)
    {
        if dictData["report_flag"].stringValue == "1"
        {
            KSToastView.ks_showToast(mapping.string(forKey: "Report already added."), duration: ToastDuration)
            return
        }
        let obj:ReportViewController = ReportViewController(nibName: "ReportViewController", bundle: nil)
        obj.strRideId = dictData["id"].stringValue
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
    }
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDriveInformationAction(_ sender: Any)
    {
        let obj:DriverInfoViewController = DriverInfoViewController(nibName: "DriverInfoViewController", bundle: nil)
        if dictData["driver_info"].count == 0
        {
            KSToastView.ks_showToast(mapping.string(forKey: "There is no driver assigned yet."), duration: ToastDuration)
            return
        }
        obj.dictDriverData = dictData["driver_info"]
        obj.dictCabData = dictData["cab_info"]
       // obj.strProfile = dictData["user_profile"].stringValue
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Draw Path
    
    func drawPath()
    {
        
        let locationInfo = dictData["location_info"]
        
        var origin = String()
        var destination = String()
        
        origin = "\(locationInfo["source_lat"].stringValue),\(locationInfo["source_lng"].stringValue)"
        destination = "\(locationInfo["desti_lat"].stringValue),\(locationInfo["desti_lng"].stringValue)"       
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(GoogleMapsDrawPathKey)"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url).responseJSON { response in
                
                let json = JSON(data: response.data!)
                let routes = json["routes"].arrayValue
                if routes.count == 0
                {
                    return
                }
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.map = self.viewfGoogleMaps
                    polyline.strokeColor = MySingleton.sharedManager.themeColor
                    polyline.strokeWidth = 3
                    if self.dictData["ride_type"].stringValue != "2"
                    {
                        let bounds = GMSCoordinateBounds(path: path!)
                        self.viewfGoogleMaps!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100))
                    }
                
                }
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
        
    }
    
    
    //MARK:- Set Pin On Map
    
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
       // self.viewfGoogleMaps.clear()
        let marker = GMSMarker()       
        
      //  let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 8)
        //viewfGoogleMaps.animate(to: camera)
        
        // Creates a marker in the center of the map.
       
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let Address = reverseGeoCoding(Latitude: lat, Longitude: long)
        marker.title = Address
        if type == 1
        {
            marker.icon = UIImage.init(named: "pin_pickup_location.png")
        }
        else if type == 2
        {
            marker.icon = UIImage.init(named: "pin_drop_location.png")
        }
        else if type == 3
        {
            marker.icon = UIImage.init(named: "ic_user_pin_red.png")
            arrMarkers.append(marker)
        }
        
        marker.map = viewfGoogleMaps
    }
    
    //MARK:- Reverse gecode and get local address from Latitude and longitude
    
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                
                
                let lines = address.lines! as [String]
                
                currentAddress = lines.joined(separator: ",")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
               
                //self.getDriveRide()
                print("Address - ",currentAddress)
                
            }
        }
        return currentAddress
    }

    
    //MARK:- Service
  
    
    /*func getCurrentRide(strRideId:String)
    {
        let kRegiURL = "\(DraiwelnaMainURL)current_ride"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" :String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "ride_id":strRideId]
            
           // MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print(json)
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                       
                        //self.drawPath()
                        let dict = json["data"].dictionaryValue
                        let locationInfo = dict["location_info"]?.dictionaryValue
                        let arrWayPoints = locationInfo?["waypoint"]?.arrayValue
                        let totalWayPoints:Int = arrWayPoints!.count - 1
                        for i in 0..<totalWayPoints
                        {
                            let dictFirst = arrWayPoints?[i].dictionaryValue
                            let dictSecond = arrWayPoints?[i+1].dictionaryValue
                            self.drawCurrentEndRidePath(origin_lat: (dictFirst?["way_lat"]?.stringValue)!, origin_lng: (dictFirst?["way_lng"]?.stringValue)!, desti_lat: (dictSecond?["way_lat"]?.stringValue)!, desti_lng: (dictSecond?["way_lat"]?.stringValue)!)                            
                        }
                        let dictFirst = arrWayPoints?[totalWayPoints].dictionaryValue
                        self.setUpPinOnMap(lat:(dictFirst?["way_lat"]?.doubleValue)!, long: (dictFirst?["way_lat"]?.doubleValue)!,type: 1)
                        let locationOldInfo = self.dictData["location_info"]
                        self.setUpPinOnMap(lat: locationOldInfo["source_lat"].doubleValue, long: locationOldInfo["source_lng"].doubleValue,type: 1)
                        self.setUpPinOnMap(lat: locationOldInfo["desti_lat"].doubleValue, long: locationOldInfo["desti_lng"].doubleValue,type: 2)
                        self.timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                        self.timer.invalidate()
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }*/


}
