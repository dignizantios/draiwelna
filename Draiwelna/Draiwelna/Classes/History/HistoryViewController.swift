//
//  HistoryViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView

class HistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate
{
    var strOffset = String()
    var arrOfHistory :[JSON] = []
    var strMessage  = String()
    var activeTextField = Int()
    var dictEditData:JSON!
    var strListType = String()
    var refreshControl: UIRefreshControl!
    
    //MARK:- Outlet Zone
    
    @IBOutlet var lblSceenTitle: UILabel!
    @IBOutlet var btnSideMenuOutletAR: UIButton!
    @IBOutlet var btnSideMenuOutletEN: UIButton!
    @IBOutlet var tblOfHistory: UITableView!
    @IBOutlet var btnUpcomingOutlet: UIButton!
    @IBOutlet var viewUpcomingSelected: UIView!
    @IBOutlet var btnPreviousOutlet: UIButton!
    @IBOutlet var viewPreviousSelected: UIView!
    @IBOutlet var heightOfPrevious: NSLayoutConstraint!
    @IBOutlet var heightOfUpcoming: NSLayoutConstraint!
    
    // Common
    
    @IBOutlet var viewOfBookingDetails: UIView!
    @IBOutlet var TopOfDatePickerView: NSLayoutConstraint!
    @IBOutlet var datePickerOfBookingRide: UIDatePicker!
    
    // English
    
    @IBOutlet var viewOfEnglish: UIView!
   // @IBOutlet var viewOfEnglish: ShadowView!
    @IBOutlet var txtBookingDateEN: UITextField!
    @IBOutlet var txtBookingTimeEN: UITextField!
    @IBOutlet var txtViewOfCommentsEN: UITextView!
    @IBOutlet var lblTxtViewPlaceHolderEN: UILabel!
    @IBOutlet var btnSpecialRequestOutletEN: UIButton!
    @IBOutlet var btnRegularBookOutletEN: UIButton!
    @IBOutlet var btnCancelOutlet: UIButton!
    @IBOutlet var btnDoneOutlet: UIButton!
    @IBOutlet var heightOftxtCommentEN: NSLayoutConstraint!
    
    
    // Arabic Outlet
    
    @IBOutlet var viewOfArabic: UIView!
   // @IBOutlet var viewOfArabic: ShadowView!
    @IBOutlet var txtBookingDateAR: UITextField!
    @IBOutlet var txtBookingTimeAR: UITextField!
    @IBOutlet var txtViewOfCommentsAR: UITextView!
    @IBOutlet var lblTxtViewPlaceHolderAR: UILabel!
    @IBOutlet var btnSpecialRequestOutletAR: UIButton!
    @IBOutlet var btnRegularBookOutletAR: UIButton!
    @IBOutlet var heightOftxtCommentAR: NSLayoutConstraint!
    

    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TopOfDatePickerView.constant = UIScreen.main.bounds.height
        self.navigationController?.isNavigationBarHidden = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white
        let attr = [NSForegroundColorAttributeName:UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: mapping.string(forKey: "Pull to refresh"), attributes:attr)
        self.tblOfHistory.addSubview(refreshControl)
        
        DidLoadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HistorySceenLoad), name: NSNotification.Name(rawValue: "isComeFromHistory"), object: nil)
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        showGesture()
    }

    //MARK:- Memory Managament
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- TableView Delegate & dataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrOfHistory.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.white
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }        
        tableView.backgroundView = nil
        return arrOfHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let cell:HistoryCell = self.tblOfHistory.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
            let dict = arrOfHistory[indexPath.row]
            
            if dict["ride_type"].stringValue == "0" || dict["ride_type"].stringValue == "1"
            {
                cell.btnEditRideOutlet.isHidden = false
                cell.btnDeleteRideOutlet.isHidden = false
            }
            else
            {
                cell.btnEditRideOutlet.isHidden = true
                cell.btnDeleteRideOutlet.isHidden = true
            }
            cell.btnEditRideOutlet.tag = indexPath.row
            cell.btnEditRideOutlet.addTarget(self, action: #selector(HistoryViewController.btnEditRideAction), for: .touchUpInside)
            cell.btnDeleteRideOutlet.tag = indexPath.row
            cell.btnDeleteRideOutlet.addTarget(self, action: #selector(HistoryViewController.btnDeleteRideAction), for: .touchUpInside)
            
            let dictInformation = dict["location_info"]
            cell.lblPickUpLocationEN.text = dictInformation["source_address"].stringValue
            cell.lblDestinationLocationEN.text = dictInformation["desti_address"].stringValue
            let dictFairData = dict["fair_data"]
          //  cell.lblTotalPriceEN.text = "\(dictFairData["currency"].stringValue) \(String(format: "%.2f", dictFairData["fair_amount"].floatValue))"
            if dict["ride_type"].stringValue == "1" || dict["ride_type"].stringValue == "2"
            {
                if dict["ride_type"].stringValue == "1"
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: mapping.string(forKey: "Accepted"), type: dict["ride_type"].stringValue)
                }
                else
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: mapping.string(forKey: "In Progress"), type: dict["ride_type"].stringValue)
                }
                
            }
            else if dict["ride_type"].stringValue == "0" || dict["ride_type"].stringValue == "3" || dict["ride_type"].stringValue == "4" || dict["ride_type"].stringValue == "5"
            {
                if dict["ride_type"].stringValue == "0"
                {
                     cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: mapping.string(forKey: "Pending"), type: dict["ride_type"].stringValue)
                }
                else if dict["ride_type"].stringValue == "3"
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: mapping.string(forKey: "Completed"), type: dict["ride_type"].stringValue)
                }
                else
                {
                     cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: mapping.string(forKey: "Cancelled"), type: dict["ride_type"].stringValue)
                }
            }
           
            cell.lblTotalPriceEN.attributedText = attributedString(str1: "\(dictFairData["currency"].stringValue) ", str2: (String(format: "%.2f", dictFairData["fair_amount"].floatValue)), type: "")
            cell.lblDistanceEN.text = dictFairData["total_km"].stringValue
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dict["book_date"].stringValue)
            let strShowBookDate = DateToString(Formatter: "MMMM dd - hh:mm a", date: strBookDate)
            cell.lblRideDateTime.text = strShowBookDate
            
            return cell
        }
        else
        {
            let cell:HistoryARCell = self.tblOfHistory.dequeueReusableCell(withIdentifier: "HistoryARCell") as! HistoryARCell
            let dict = arrOfHistory[indexPath.row]
            
            if dict["ride_type"].stringValue == "0" || dict["ride_type"].stringValue == "1"
            {
                cell.btnEditRideOutlet.isHidden = false
                cell.btnDeleteRideOutlet.isHidden = false
            }
            else
            {
                cell.btnEditRideOutlet.isHidden = true
                cell.btnDeleteRideOutlet.isHidden = true
            }
            cell.btnEditRideOutlet.tag = indexPath.row
            cell.btnEditRideOutlet.addTarget(self, action: #selector(HistoryViewController.btnEditRideAction), for: .touchUpInside)
            cell.btnDeleteRideOutlet.tag = indexPath.row
            cell.btnDeleteRideOutlet.addTarget(self, action: #selector(HistoryViewController.btnDeleteRideAction), for: .touchUpInside)
            
            let dictInformation = dict["location_info"]
            cell.lblPickUpLocationAR.text = dictInformation["source_address"].stringValue
            cell.lblDestinationLocationAR.text = dictInformation["desti_address"].stringValue
            let dictFairData = dict["fair_data"]
          //  cell.lblTotalPriceAR.text = "\(dictFairData["currency"].stringValue) \(String(format: "%.2f", dictFairData["fair_amount"].floatValue))"
            if dict["ride_type"].stringValue == "1" || dict["ride_type"].stringValue == "2"
            {
                if dict["ride_type"].stringValue == "1"
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Accepted"), type: dict["ride_type"].stringValue)
                }
                else
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "In Progress"), type: dict["ride_type"].stringValue)
                }
                
            }
            else if dict["ride_type"].stringValue == "0" || dict["ride_type"].stringValue == "3" || dict["ride_type"].stringValue == "4" || dict["ride_type"].stringValue == "5"
            {
                if dict["ride_type"].stringValue == "0"
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Pending"), type: dict["ride_type"].stringValue)
                }
                else if dict["ride_type"].stringValue == "3"
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Completed"), type: dict["ride_type"].stringValue)
                }
                else
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Cancelled"), type: dict["ride_type"].stringValue)
                }
            }

            cell.lblTotalPriceAR.attributedText = attributedString(str1: "\(dictFairData["currency"].stringValue)", str2: "\((String(format: "%.2f", dictFairData["fair_amount"].floatValue))) ", type: "")
            cell.lblDistanceAR.text = dictFairData["total_km"].stringValue
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dict["book_date"].stringValue)
            let strShowBookDate = DateToString(Formatter: "MMMM dd - hh:mm a", date: strBookDate)
            cell.lblRideDateTime.text = strShowBookDate
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj:HistoryDetailViewController = HistoryDetailViewController(nibName: "HistoryDetailViewController", bundle: nil)
        obj.dictData = self.arrOfHistory[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Private Method
    
    func HistorySceenLoad()
    {
        DidLoadData()
    }
    
    func DidLoadData()
    {
        self.lblSceenTitle.text = mapping.string(forKey: "Rides")
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.tblOfHistory.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
            self.btnSideMenuOutletAR.isHidden = true
            self.btnSideMenuOutletEN.isHidden = false
            
            self.viewOfEnglish.layer.cornerRadius = 5
            
            self.btnRegularBookOutletEN.layer.cornerRadius = 5
            self.btnRegularBookOutletEN.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            self.btnRegularBookOutletEN.layer.borderWidth = 1
            
            self.btnSpecialRequestOutletEN.layer.cornerRadius = 5
            
            [txtBookingDateEN,txtBookingTimeEN].forEach({ customizeTextFieldEN(textfield: $0) })
            
            self.txtViewOfCommentsEN.layer.cornerRadius = 5
            self.txtViewOfCommentsEN.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            self.txtViewOfCommentsEN.layer.borderWidth = 1
            
            self.txtViewOfCommentsEN.textContainerInset = UIEdgeInsets(top: 9 , left: 13, bottom: 0, right: 10)
            
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
            
            self.btnUpcomingOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
            self.viewUpcomingSelected.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnPreviousOutlet.setTitleColor(UIColor.white, for: .normal)
            self.viewPreviousSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
            
            self.heightOfPrevious.constant = 1
            self.heightOfUpcoming.constant = 3
            self.viewUpcomingSelected.alpha = 1
            
            self.btnUpcomingOutlet.setTitle(mapping.string(forKey: "Upcoming"), for: .normal)
            self.btnPreviousOutlet.setTitle(mapping.string(forKey: "Previous"), for: .normal)
        }
        else
        {
            self.tblOfHistory.register(UINib(nibName: "HistoryARCell", bundle: nil), forCellReuseIdentifier: "HistoryARCell")
            self.btnSideMenuOutletAR.isHidden = false
            self.btnSideMenuOutletEN.isHidden = true
            self.viewOfArabic.layer.cornerRadius = 5
            
            self.btnRegularBookOutletAR.layer.cornerRadius = 5
            self.btnRegularBookOutletAR.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            self.btnRegularBookOutletAR.layer.borderWidth = 1
            
            self.btnSpecialRequestOutletAR.layer.cornerRadius = 5
            
            [txtBookingDateAR,txtBookingTimeAR].forEach({ customizeTextFieldAR(textfield: $0) })
            
            self.txtViewOfCommentsAR.layer.cornerRadius = 5
            self.txtViewOfCommentsAR.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            self.txtViewOfCommentsAR.layer.borderWidth = 1
            
            self.txtViewOfCommentsAR.textContainerInset = UIEdgeInsets(top: 9 , left: 10, bottom: 0, right: 13)
            
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
            
            self.btnPreviousOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
            self.viewPreviousSelected.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnUpcomingOutlet.setTitleColor(UIColor.white, for: .normal)
            self.viewUpcomingSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
            
            self.heightOfPrevious.constant = 3
            self.heightOfUpcoming.constant = 1
            self.viewPreviousSelected.alpha = 1
            
            self.btnPreviousOutlet.setTitle(mapping.string(forKey: "Upcoming"), for: .normal)
            self.btnUpcomingOutlet.setTitle(mapping.string(forKey: "Previous"), for: .normal)
        }
        
        strOffset = "0"
        strListType = "0"
        getHistoryList()
    }
    
    func refresh(sender:AnyObject)
    {
        strOffset = "0"
        getHistoryList()
        refreshControl.endRefreshing()
        // Code to refresh table view
    }
    
    private func attributedString(str1:String,str2:String,type:String) -> NSAttributedString?
    {
        let FormatedString = NSMutableAttributedString()
        if type == ""
        {
            let attributes1 = [
                
                NSForegroundColorAttributeName : MySingleton.sharedManager.themeColor,
                NSFontAttributeName : UIFont(name: "OpenSans", size: 16)!
                ] as [String : Any]
            
            let attributes2 = [
                NSForegroundColorAttributeName : MySingleton.sharedManager.themeColor,
                NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 22)!
                ] as [String : Any]
            
            let attributedString1 = NSAttributedString(string: str1, attributes: attributes1)
            let attributedString2 = NSAttributedString(string: str2, attributes: attributes2)
            
           
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                FormatedString.append(attributedString1)
                FormatedString.append(attributedString2)
            }
            else
            {
                FormatedString.append(attributedString2)
                FormatedString.append(attributedString1)
            }

        }
        else
        {
            var attributes1  = [String:Any]()
            var attributes2  = [String:Any]()
            if type == "1" || type == "2"
            {
                attributes1 = [
                    NSForegroundColorAttributeName : UIColor.white,
                    NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!
                    ] as [String : Any]
                attributes2 = [
                    NSForegroundColorAttributeName : UIColor.init(red: 31/255, green: 188/255, blue: 42/255, alpha: 1.0),
                    NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 14)!
                    ] as [String : Any]
            }
            else if type == "0" || type == "3" || type == "4" || type == "5"
            {
                attributes1 = [
                    NSForegroundColorAttributeName : UIColor.white,
                    NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!
                    ] as [String : Any]
                attributes2 = [
                    NSForegroundColorAttributeName : MySingleton.sharedManager.themeColor,
                    NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 14)!
                    ] as [String : Any]
            }
            
            let attributedString1 = NSAttributedString(string: str1, attributes: attributes1)
            let attributedString2 = NSAttributedString(string: str2, attributes: attributes2)
            
            
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                FormatedString.append(attributedString1)
                FormatedString.append(attributedString2)
            }
            else
            {
                FormatedString.append(attributedString2)
                FormatedString.append(attributedString1)
            }
        }
        return FormatedString
    }

    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        activeTextField = Int()
        if textField == self.txtBookingDateEN || textField == self.txtBookingDateAR
        {
            activeTextField = 1
            self.view.endEditing(true)
            self.datePickerOfBookingRide.minimumDate = Date()
            self.datePickerOfBookingRide.datePickerMode = .date
            self.movePickerUp()
            return false
        }
        else if textField == self.txtBookingTimeEN || textField == self.txtBookingTimeAR
        {
            activeTextField = 2
            self.view.endEditing(true)
            self.datePickerOfBookingRide.minimumDate = Date()
            self.datePickerOfBookingRide.datePickerMode = .time
            self.movePickerUp()
            return false
        }
        self.movePickerDown()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.movePickerDown()
        return textField.resignFirstResponder()
    }
    
    func ClerAllTextField(textfied:UITextField)
    {
        textfied.text = ""
    }
    
    
    //MARK:- TextView Delegate
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == self.txtViewOfCommentsEN
        {
            if self.txtViewOfCommentsEN.text == ""
            {
                self.lblTxtViewPlaceHolderEN.isHidden = false
            }
            else
            {
                self.lblTxtViewPlaceHolderEN.isHidden = true
            }
        }
        else
        {
            if self.txtViewOfCommentsAR.text == ""
            {
                self.lblTxtViewPlaceHolderAR.isHidden = false
            }
            else
            {
                self.lblTxtViewPlaceHolderAR.isHidden = true
            }
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK: - View UP and Down Method
    
    func movePickerUp()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.TopOfDatePickerView.constant = UIScreen.main.bounds.height - 180
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
    }
    
    func movePickerDown()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.TopOfDatePickerView.constant = UIScreen.main.bounds.height
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
    }
 
    //MARK:- Action Zone
    
    func btnEditRideAction(sender:UIButton)
    {
        self.viewOfBookingDetails.alpha = 0
        self.viewOfBookingDetails.isHidden = false
        viewOfBookingDetails.fadeIn()
        let buttonRow = sender.tag
        dictEditData = self.arrOfHistory[buttonRow]
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dictEditData["start_date"].stringValue)
            self.txtBookingDateEN.text = DateToString(Formatter: "dd-MM-yyyy", date: strBookDate)
            self.txtBookingTimeEN.text = DateToString(Formatter: "hh:mm a", date: strBookDate)
            self.txtViewOfCommentsEN.text = dictEditData["comment"].stringValue
            if dictEditData["comment"].stringValue == ""
            {
                self.lblTxtViewPlaceHolderEN.isHidden = true
                self.heightOftxtCommentEN.constant = 0
            }
            else
            {
                self.heightOftxtCommentEN.constant = 93
                self.lblTxtViewPlaceHolderEN.isHidden = true
            }
            self.viewOfEnglish.isHidden = false
            self.viewOfArabic.isHidden = true
        }
        else
        {
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dictEditData["start_date"].stringValue)
            self.txtBookingDateAR.text = DateToString(Formatter: "dd-MM-yyyy", date: strBookDate)
            self.txtBookingTimeAR.text = DateToString(Formatter: "hh:mm a", date: strBookDate)
            
            self.viewOfEnglish.isHidden = true
            self.viewOfArabic.isHidden = false
            
            self.txtViewOfCommentsAR.text = dictEditData["comment"].stringValue
            if dictEditData["comment"].stringValue == ""
            {
                self.heightOftxtCommentAR.constant = 0
                self.lblTxtViewPlaceHolderAR.isHidden = true
            }
            else
            {
                self.heightOftxtCommentAR.constant = 93
                self.lblTxtViewPlaceHolderAR.isHidden = true
            }
        }

    }
    
    func btnDeleteRideAction(sender:UIButton)
    {
        //showAlertMessage(vc: self, titleStr: AppName, messageStr: "Are you sure want to delete this ride?")
        let alert = UIAlertController(title: AppName, message: mapping.string(forKey: "Are you sure want to delete this ride?"), preferredStyle: UIAlertControllerStyle.alert);
        let cancelAction = UIAlertAction(title: mapping.string(forKey: "No"), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
            
        }
        let okAction = UIAlertAction(title: mapping.string(forKey: "Yes"), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
            let buttonRow = sender.tag
            let dict = self.arrOfHistory[buttonRow]
            self.DeleteRide(strDeleteId: dict["id"].stringValue)
            
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func btnCloseBookingAction(_ sender: Any)
    {
        self.view.endEditing(true)
        viewOfBookingDetails.fadeOut()
    }
    @IBAction func btnSpecialRequestAction(_ sender: Any)
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                self.heightOftxtCommentEN.constant = 93
                self.lblTxtViewPlaceHolderEN.isHidden = false
                self.txtViewOfCommentsEN.text = ""
            }
            else
            {
                self.heightOftxtCommentAR.constant = 93
                self.lblTxtViewPlaceHolderAR.isHidden = false
                self.txtViewOfCommentsAR.text = ""
            }
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
        
        
        //  let obj:AccountViewController = AccountViewController(nibName: "AccountViewController", bundle: nil)
        //self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnRegularBookAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if self.txtBookingDateEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: ToastDuration)
            }
            else if self.txtBookingTimeEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: ToastDuration)
            }
            else
            {
                self.movePickerDown()
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsEN.text = trimmedUserName
                print("BookDate: \(self.txtBookingDateEN.text!)")
                obj.strBookingDate = self.txtBookingDateEN.text!
                obj.strBookingTime = self.txtBookingTimeEN.text!
                obj.strComment = self.txtViewOfCommentsEN.text!
                obj.strRideType = self.dictEditData["ride_type"].stringValue
                obj.dictLocationInfo = self.dictEditData["location_info"].dictionaryObject as! NSMutableDictionary
                obj.dictRideData = self.dictEditData["fair_data"]
                obj.strSpecialComment = ""
                obj.rideEditID = self.dictEditData["id"].stringValue
                obj.isComeFromEditRide = true
                self.navigationController?.pushViewController(obj, animated: true)
            
            }
            
        }
        else
        {
            if self.txtBookingDateAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: ToastDuration)
            }
            else if self.txtBookingTimeAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: ToastDuration)
            }
            else
            {
                self.movePickerDown()
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsAR.text = trimmedUserName
                obj.strBookingDate = self.txtBookingDateAR.text!
                obj.strBookingTime = self.txtBookingTimeAR.text!
                obj.strComment = self.txtViewOfCommentsAR.text!
                obj.strRideType = self.dictEditData["ride_type"].stringValue
                obj.dictLocationInfo = self.dictEditData["location_info"].dictionaryObject as! NSMutableDictionary
                obj.dictRideData = self.dictEditData["fair_data"]
                obj.strSpecialComment = ""
                obj.rideEditID = self.dictEditData["id"].stringValue
                obj.isComeFromEditRide = true
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    
    @IBAction func btnPickerCancelAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.movePickerDown()
        }
        else
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
            }
        }
    }
    @IBAction func btnPickerDoneAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
            }
        }
        else
        {
            self.movePickerDown()
        }
    }
    
    @IBAction func btnUpcomingAction(_ sender: Any)
    {
        self.btnUpcomingOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
        self.viewUpcomingSelected.backgroundColor = MySingleton.sharedManager.themeColor
        self.viewUpcomingSelected.alpha = 1
        self.btnPreviousOutlet.setTitleColor(UIColor.white, for: .normal)
        self.viewPreviousSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        self.heightOfPrevious.constant = 1
        self.heightOfUpcoming.constant = 3
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
           
            strListType = "0"
            strOffset = "0"
            getHistoryList()
        }
        else
        {
            strListType = "1"
            strOffset = "0"
            getHistoryList()
        }
       
    }

    @IBAction func btnPreviousAction(_ sender: Any)
    {
        self.btnPreviousOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
        self.viewPreviousSelected.backgroundColor = MySingleton.sharedManager.themeColor
        self.btnUpcomingOutlet.setTitleColor(UIColor.white, for: .normal)
        self.viewUpcomingSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        self.viewPreviousSelected.alpha = 1
        self.heightOfPrevious.constant = 3
        self.heightOfUpcoming.constant = 1
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            strListType = "1"
            strOffset = "0"
            getHistoryList()
        }
        else
        {           
            strListType = "0"
            strOffset = "0"
            getHistoryList()
        }
       
    }
   
    
    
    //MARK:- Service
    
    func getHistoryList()
    {
        let kRegiURL = "\(DraiwelnaMainURL)ride_history"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" :String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "login_user":UserDefaults.standard.value(forKey: "isDriverLogin") as! String,
                         "offset":strOffset,
                         "flag_view":strListType]
            
            print("parm \(param)")
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print(json)
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                        
                        if self.strOffset == "0"
                        {
                           self.arrOfHistory = []
                        }
                        
                        self.strOffset = json["next_offset"].stringValue
                        self.arrOfHistory = json["data"].arrayValue
                        self.tblOfHistory.reloadData()
                        
                    }
                    else
                    {
                        self.arrOfHistory = []
                        self.strOffset = json["next_offset"].stringValue
                        self.strMessage = json["msg"].stringValue
                        self.tblOfHistory.reloadData()
                    }
                    
                }
                else
                {
                   KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

    func DeleteRide(strDeleteId:String)
    {
        let kRegiURL = "\(DraiwelnaMainURL)manage_ride_request"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["ride_id" : strDeleteId,
                         "is_cancel" :"1",
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "user_id" : UserDefaults.standard.value(forKey: "UserId") as! String]
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print(json)
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                        self.strOffset = "0"
                        self.getHistoryList()
                        
                    }
                    else
                    {
                       KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    //MARK: - Scrollview Delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if strOffset != "-1" && strOffset != "0"
        {
            if tblOfHistory.contentOffset.y >= (tblOfHistory.contentSize.height - tblOfHistory.bounds.size.height)
            {
                getHistoryList()
                MBProgressHUD.hideAllHUDs(for: self.view!, animated: true)
            }
        }
    }
    

}
