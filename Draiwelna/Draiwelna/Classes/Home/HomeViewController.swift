//
//  HomeViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 21/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import GoogleMaps
import KSToastView
import Firebase
import FirebaseDatabase
import GeoFire

class HomeViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,UITextViewDelegate
{
    //MARK:- Varible Declaration
    
    var locationManager = CLLocationManager()
    var didFindLocation = Bool()
    var activeTextField = Int()
    
    //MARK:- Outlet Zone

    //Common Outlet
    
    @IBOutlet var imgOfBookRideAr: UIImageView!
    @IBOutlet var imgOfBookRideEN: UIImageView!
    @IBOutlet var btnBookRideOutlet: ShadowButton!
    @IBOutlet var btnUserCurrentLocationOutlet: UIButton!
    @IBOutlet var btnSideMenuOutlet: UIButton!
    @IBOutlet var viewOfGoogleMaps: GMSMapView!
    
    @IBOutlet var viewOfBookingDetails: UIView!
    @IBOutlet var TopOfDatePickerView: NSLayoutConstraint!
    @IBOutlet var datePickerOfBookingRide: UIDatePicker!
    
    // English Outlet
    @IBOutlet var viewOfEnglish: ShadowView!
    @IBOutlet var txtBookingDateEN: UITextField!
    @IBOutlet var txtBookingTimeEN: UITextField!
    @IBOutlet var txtViewOfCommentsEN: UITextView!
    @IBOutlet var lblTxtViewPlaceHolderEN: UILabel!
    @IBOutlet var btnSpecialRequestOutletEN: UIButton!
    @IBOutlet var btnRegularBookOutletEN: UIButton!
    @IBOutlet var btnCancelOutlet: UIButton!
    @IBOutlet var btnDoneOutlet: UIButton!
    @IBOutlet var heightOftxtCommentEN: NSLayoutConstraint!
    
    // Arabic Outlet
    
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var txtBookingDateAR: UITextField!
    @IBOutlet var txtBookingTimeAR: UITextField!
    @IBOutlet var txtViewOfCommentsAR: UITextView!
    @IBOutlet var lblTxtViewPlaceHolderAR: UILabel!
    @IBOutlet var btnSpecialRequestOutletAR: UIButton!
    @IBOutlet var btnRegularBookOutletAR: UIButton!
    @IBOutlet var heightOftxtCommentAR: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest    
        
        self.TopOfDatePickerView.constant = UIScreen.main.bounds.height
        DidLoadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.HomeSceenLoad), name: NSNotification.Name(rawValue: "isComeFromHome"), object: nil)
        
        // Do any additional setup after loading the view.
    }    
    
    override func viewWillAppear(_ animated: Bool)
    {
        showGesture()
    }

    //MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    


    //MARK:- Private Method
    
    @available(iOS 10.0, *)
    func openSetting()
    {
        let alertController = UIAlertController (title: AppName, message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func setupUserCurrentLocation()
    {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        didFindLocation = true
    }
   
    func HomeSceenLoad()
    {
        DidLoadData()
    }
    
    func DidLoadData()
    {
        self.btnBookRideOutlet.setTitle(mapping.string(forKey: "Book a ride"), for: .normal)
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.imgOfBookRideAr.isHidden = true
            self.imgOfBookRideEN.isHidden = false
            
            self.btnUserCurrentLocationOutlet.setImage(UIImage.init(named: "ic_current_location_white_header.png"), for: .normal)
            self.btnSideMenuOutlet.setImage(UIImage.init(named: "ic_menubar_white_header.png"), for: .normal)
            
            
            self.viewOfEnglish.isHidden = false
            self.viewOfArabic.isHidden = true
            
            self.viewOfEnglish.layer.cornerRadius = 5
            
            self.btnRegularBookOutletEN.layer.cornerRadius = 5
            self.btnRegularBookOutletEN.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            self.btnRegularBookOutletEN.layer.borderWidth = 1
            
            self.btnSpecialRequestOutletEN.layer.cornerRadius = 5
            
            [txtBookingDateEN,txtBookingTimeEN].forEach({ customizeTextFieldEN(textfield: $0) })
            
            self.txtViewOfCommentsEN.layer.cornerRadius = 5
            self.txtViewOfCommentsEN.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            self.txtViewOfCommentsEN.layer.borderWidth = 1
            
            self.txtViewOfCommentsEN.textContainerInset = UIEdgeInsets(top: 9 , left: 13, bottom: 0, right: 10)
            self.heightOftxtCommentEN.constant = 0
            self.lblTxtViewPlaceHolderEN.isHidden = true
            
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
            
        }
        else
        {
            self.viewOfEnglish.isHidden = true
            self.viewOfArabic.isHidden = false
            
            self.imgOfBookRideAr.isHidden = false
            self.imgOfBookRideEN.isHidden = true
            
            self.btnUserCurrentLocationOutlet.setImage(UIImage.init(named: "ic_menubar_white_header.png"), for: .normal)
            self.btnSideMenuOutlet.setImage(UIImage.init(named: "ic_current_location_white_header.png"), for: .normal)
            
            
            self.viewOfArabic.layer.cornerRadius = 5
            self.heightOftxtCommentAR.constant = 0
            self.lblTxtViewPlaceHolderAR.isHidden = true
            
            self.btnRegularBookOutletAR.layer.cornerRadius = 5
            self.btnRegularBookOutletAR.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            self.btnRegularBookOutletAR.layer.borderWidth = 1
            
            self.btnSpecialRequestOutletAR.layer.cornerRadius = 5
            
            [txtBookingDateAR,txtBookingTimeAR].forEach({ customizeTextFieldAR(textfield: $0) })
            
            self.txtViewOfCommentsAR.layer.cornerRadius = 5
            self.txtViewOfCommentsAR.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            self.txtViewOfCommentsAR.layer.borderWidth = 1
            
            self.txtViewOfCommentsAR.textContainerInset = UIEdgeInsets(top: 9 , left: 10, bottom: 0, right: 13)
            
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- MapView Delegate
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        print("marker:- \(marker)")
        
        return true
    }
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        activeTextField = Int()
        if textField == self.txtBookingDateEN || textField == self.txtBookingDateAR
        {
            activeTextField = 1
            self.view.endEditing(true)
            self.datePickerOfBookingRide.minimumDate = Date()
            self.datePickerOfBookingRide.datePickerMode = .date
            self.movePickerUp()
            return false
        }
        else if textField == self.txtBookingTimeEN || textField == self.txtBookingTimeAR
        {
            activeTextField = 2
            self.view.endEditing(true)
            self.datePickerOfBookingRide.minimumDate = Date()
            self.datePickerOfBookingRide.datePickerMode = .time
            self.movePickerUp()
           return false
        }
        self.movePickerDown()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.movePickerDown()
        return textField.resignFirstResponder()
    }
    
    func ClerAllTextField(textfied:UITextField)
    {
        textfied.text = ""
    }


    //MARK:- TextView Delegate

    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == self.txtViewOfCommentsEN
        {
            if self.txtViewOfCommentsEN.text == ""
            {
                self.lblTxtViewPlaceHolderEN.isHidden = false
            }
            else
            {
                self.lblTxtViewPlaceHolderEN.isHidden = true
            }
        }
        else
        {
            if self.txtViewOfCommentsAR.text == ""
            {
                self.lblTxtViewPlaceHolderAR.isHidden = false
            }
            else
            {
                self.lblTxtViewPlaceHolderAR.isHidden = true
            }
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK: - View UP and Down Method
    
    func movePickerUp()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.TopOfDatePickerView.constant = UIScreen.main.bounds.height - 180
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
        
    }
    
    func movePickerDown()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.TopOfDatePickerView.constant = UIScreen.main.bounds.height
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
    }
    
    //MARK:- Reverse gecode and get local address from Latitude and longitude
    
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                
                
                let lines = address.lines! as [String]
                
                currentAddress = lines.joined(separator: ",")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
              
                //self.getDriveRide()
                print("Address - ",currentAddress)
                
            }
        }
        return currentAddress
    }
    
    //MARK:- CLLocationManager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if didFindLocation == true
        {
            didFindLocation = false
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 20)
            viewOfGoogleMaps.animate(to: camera)
            
            // Creates a marker in the center of the map.
            self.viewOfGoogleMaps.clear()
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
            marker.icon = UIImage.init(named: "ic_user_pin_red.png")
            let Address = reverseGeoCoding(Latitude: locValue.latitude, Longitude: locValue.longitude)
            marker.title = Address
            marker.map = viewOfGoogleMaps            
        }
        else
        {
             didFindLocation = false
        }
       

    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
             setupUserCurrentLocation()
          //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            setupUserCurrentLocation()
           // manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            if #available(iOS 10.0, *) {
                openSetting()
            } else {
                // Fallback on earlier versions
            }
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            if #available(iOS 10.0, *) {
                openSetting()
            } else {
                // Fallback on earlier versions
            }
            break
     //   default:
           // break
        }
    }   
    
  
    
    //MARK:- Action Zone
    
    @IBAction func btnCloseBookingAction(_ sender: Any)
    {
        self.view.endEditing(true)
        [txtBookingDateEN,txtBookingTimeEN,txtBookingDateAR,txtBookingDateAR].forEach({ ClerAllTextField(textfied: $0)})
        self.txtViewOfCommentsEN.text = ""
        self.txtViewOfCommentsAR.text = ""        
        self.lblTxtViewPlaceHolderEN.isHidden = false
        self.lblTxtViewPlaceHolderAR.isHidden = false
        self.btnSpecialRequestOutletEN.isUserInteractionEnabled = true
        self.btnSpecialRequestOutletAR.isUserInteractionEnabled = true
        viewOfBookingDetails.fadeOut()
    }
    
    @IBAction func btnBookRideAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.heightOftxtCommentEN.constant = 0
            self.lblTxtViewPlaceHolderEN.isHidden = true
            self.txtViewOfCommentsEN.text = ""
        }
        else
        {
            self.heightOftxtCommentAR.constant = 0
            self.lblTxtViewPlaceHolderAR.isHidden = true
            self.txtViewOfCommentsAR.text = ""
        }
        self.viewOfBookingDetails.alpha = 0
        self.viewOfBookingDetails.isHidden = false
        viewOfBookingDetails.fadeIn()
    }
    
    @IBAction func btnSideMenuAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            sideMenuController?.leftViewController?.showLeftViewAnimated(true)
        }
        else
        {
            setupUserCurrentLocation()
        }
    }
    @IBAction func btnUserCurrentLocationAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            setupUserCurrentLocation()
        }
        else
        {
            sideMenuController?.rightViewController?.showRightViewAnimated(true)
        }
    }

    @IBAction func btnSpecialRequestAction(_ sender: Any)
    {
        self.view.layoutIfNeeded()
        UITextView.animate(withDuration: 0.3)
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                self.heightOftxtCommentEN.constant = 93
                self.lblTxtViewPlaceHolderEN.isHidden = false
                self.txtViewOfCommentsEN.text = ""
                self.btnSpecialRequestOutletEN.isUserInteractionEnabled = false
            }
            else
            {
                self.heightOftxtCommentAR.constant = 93
                self.lblTxtViewPlaceHolderAR.isHidden = false
                self.txtViewOfCommentsAR.text = ""
                self.btnSpecialRequestOutletAR.isUserInteractionEnabled = false
            }
        }
        self.view.layoutIfNeeded()
        UITextView.commitAnimations()
    }
    
    @IBAction func btnRegularBookAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if self.txtBookingDateEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: ToastDuration)
            }
            else if self.txtBookingTimeEN.text == ""
            {
                 KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: ToastDuration)
            }
            else
            {
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsEN.text = trimmedUserName
                print("BookDate: \(self.txtBookingDateEN.text!)")
                obj.strBookingDate = self.txtBookingDateEN.text!
                obj.strBookingTime = self.txtBookingTimeEN.text!
                obj.strComment = self.txtViewOfCommentsEN.text!
                obj.strRideType = "0"
                obj.strSpecialComment = ""
                self.navigationController?.pushViewController(obj, animated: true)
            }

        }
        else
        {
            if self.txtBookingDateAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select date"), duration: ToastDuration)
            }
            else if self.txtBookingTimeAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select time"), duration: ToastDuration)
            }
            else
            {
                let obj:RideDetailViewController = RideDetailViewController(nibName: "RideDetailViewController", bundle: nil)
                let trimmedUserName = self.txtViewOfCommentsAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtViewOfCommentsAR.text = trimmedUserName
                obj.strBookingDate = self.txtBookingDateAR.text!
                obj.strBookingTime = self.txtBookingTimeAR.text!
                obj.strComment = self.txtViewOfCommentsAR.text!
                obj.strRideType = "0"
                obj.strSpecialComment = ""
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        
    }
    @IBAction func btnPickerCancelAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.movePickerDown()
        }
        else
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
               
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
            }
        }
    }
    @IBAction func btnPickerDoneAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    self.txtBookingDateAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeEN.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    self.txtBookingTimeAR.text = dateFormatter.string(from: self.datePickerOfBookingRide.date)
                    self.movePickerDown()
                }
            }
        }
        else
        {
            self.movePickerDown()
        }
    }
}
