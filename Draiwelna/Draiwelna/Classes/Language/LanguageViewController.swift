//
//  LanguageViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 01/09/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController
{
    //MARK:- Outlet Zone
    
  
    @IBOutlet var btnEnglishOutlet: UIButton!
    @IBOutlet var btnArabicOutlet: UIButton!

    //MARK:- ViewLife cycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.btnEnglishOutlet.backgroundColor = MySingleton.sharedManager.themeColor
        self.btnEnglishOutlet.layer.cornerRadius = self.btnEnglishOutlet.bounds.size.height/2
        self.btnEnglishOutlet.clipsToBounds = true
        
        self.btnArabicOutlet.layer.cornerRadius = self.btnArabicOutlet.frame.size.height/2
        self.btnArabicOutlet.clipsToBounds = true
        self.btnArabicOutlet.layer.borderColor = UIColor.white.cgColor
        self.btnArabicOutlet.layer.borderWidth = 1.0


        // Do any additional setup after loading the view.
    }

      //MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - StatusBar
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return UIStatusBarStyle.lightContent
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Action Zone
    
    @IBAction func btnEnglishAction(_ sender: Any)
    {
        setLangauge(language: English)
        UserDefaults.standard.set(0, forKey: "lang")
        if CurrentScreen == 1
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromHome"), object: nil)
        }
        else if CurrentScreen == 2
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromHistory"), object: nil)
        }
        else if CurrentScreen == 3
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromAccount"), object: nil)
        }
        else if CurrentScreen == 5
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromContactUs"), object: nil)
        }
        else if CurrentScreen == 6
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromAboutUs"), object: nil)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tblRearENReload"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnArabicAction(_ sender: Any)
    {
        setLangauge(language: Arabic)
        UserDefaults.standard.set(1, forKey: "lang")
        if CurrentScreen == 1
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromHome"), object: nil)
        }
        else if CurrentScreen == 2
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromHistory"), object: nil)
        }
        else if CurrentScreen == 3
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromAccount"), object: nil)
        }
        else if CurrentScreen == 5
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromContactUs"), object: nil)
        }
        else if CurrentScreen == 6
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "isComeFromAboutUs"), object: nil)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tblRearARReload"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnClosePopupAction(_ sender: Any) {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tblRearENReload"), object: nil)
        }
        else
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tblRearARReload"), object: nil)
        }

        self.dismiss(animated: true, completion: nil)
    }

}
