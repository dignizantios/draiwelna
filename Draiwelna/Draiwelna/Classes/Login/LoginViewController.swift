//
//  LoginViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 20/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import KSToastView
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class LoginViewController: UIViewController,UITextFieldDelegate
{
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var viewOfEnglish: UIView!
    @IBOutlet var viewOfinnerFGPass: UIView!
    @IBOutlet var viewOfForgotPassword: UIView!
    
    //English Outlet
    
    @IBOutlet var btnLoginOutlet: UIButton!
    @IBOutlet var txtUserPassword: UITextField!
    @IBOutlet var txtForgotPassword: UITextField!
    @IBOutlet var btnDoneOutlet: UIButton!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var btnCloseFPOutletEN: UIButton!
    @IBOutlet var btnUserOutletEN: UIButton!
    @IBOutlet var lblSelectUserEN: UILabel!
    @IBOutlet var btnDriveOutletEN: UIButton!
    @IBOutlet var lblSelectDriveEN: UILabel!
    @IBOutlet var btnSignUpOutletEN: UIButton!
    
    //Arabic Outlet
    
    @IBOutlet var btnSignUpOutletAR: UIButton!
    @IBOutlet var txtUserNameAR: UITextField!
    @IBOutlet var txtUserPasswordAR: UITextField!
    @IBOutlet var btnLoginOutletAR: UIButton!
    @IBOutlet var btnCloseFPOutletAR: UIButton!
    @IBOutlet var btnUserOutletAR: UIButton!
    @IBOutlet var lblSelectUserAR: UILabel!
    @IBOutlet var btnDriveOutletAR: UIButton!
    @IBOutlet var lblSelectDriveAR: UILabel!
    //Common Outlet
    
    @IBOutlet var lblFPTitle: UILabel!
    
    
    //MARK:- Variable Declaration
    
    var isOnDriverLogin = Bool()
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        HideGestture()
        
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.viewOfArabic.isHidden = true
            self.viewOfEnglish.isHidden = false
            [txtUserName,txtUserPassword,txtForgotPassword].forEach({ customizeTextFieldEN(textfield: $0) })
            self.btnLoginOutlet.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnLoginOutlet.layer.cornerRadius = self.btnLoginOutlet.bounds.size.height/2
            self.btnLoginOutlet.clipsToBounds = true
            self.txtForgotPassword.textAlignment = .left
            self.btnCloseFPOutletAR.isHidden = true
            self.btnCloseFPOutletEN.isHidden = false
        }
        else
        {
            self.viewOfArabic.isHidden = false
            self.viewOfEnglish.isHidden = true
            [txtUserNameAR,txtUserPasswordAR,txtForgotPassword].forEach({ customizeTextFieldAR(textfield: $0) })
            self.btnLoginOutletAR.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnLoginOutletAR.layer.cornerRadius = self.btnLoginOutletAR.bounds.size.height/2
            self.btnLoginOutletAR.clipsToBounds = true
            self.txtForgotPassword.textAlignment = .right
            self.btnCloseFPOutletAR.isHidden = false
            self.btnCloseFPOutletEN.isHidden = true
        }
       
        self.viewOfinnerFGPass.layer.cornerRadius = 3
        txtForgotPassword.layer.borderColor = UIColor.black.cgColor
        txtForgotPassword.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        self.btnDoneOutlet.layer.cornerRadius = 3
        
        self.txtForgotPassword.placeholder = mapping.string(forKey: "Email")
        self.lblFPTitle.text = mapping.string(forKey: "Forgot Password")
        self.btnDoneOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.txtForgotPassword
        {
            self.txtForgotPassword.resignFirstResponder()
        }
        if textField == self.txtUserName
        {
            self.txtUserPassword.becomeFirstResponder()
        }
        else if textField == self.txtUserNameAR
        {
            self.txtUserPasswordAR.becomeFirstResponder()
        }
        else if textField == self.txtUserPassword
        {
            self.txtUserPassword.resignFirstResponder()
        }
        else if textField == self.txtUserPasswordAR
        {
            self.txtUserPassword.resignFirstResponder()
        }
        return true
    }

    
    //MARK: - StatusBar
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return UIStatusBarStyle.lightContent
    }


    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Action ZOne
    
    @IBAction func btnLoginAction(_ sender: Any)
    {
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let trimmedUserName = self.txtUserName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserName.text = trimmedUserName
            if self.txtUserName.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserPassword.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your password"), duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                userLoginEnglish()
            }
        }
        else
        {
            let trimmedUserName = self.txtUserNameAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserNameAR.text = trimmedUserName
            if self.txtUserNameAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserPasswordAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your password"), duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                userLoginArabic()
            }
        }
       
    }
    @IBAction func btnSignUpAction(_ sender: Any)
    {
        let obj:SignUpViewController = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any)
    {
        self.txtForgotPassword.becomeFirstResponder()
        self.viewOfForgotPassword.alpha = 0
        self.viewOfForgotPassword.isHidden = false
        viewOfForgotPassword.fadeIn()
    }

    @IBAction func btnCloseFGPassViewAction(_ sender: Any)
    {
        self.view.endEditing(true)
        viewOfForgotPassword.fadeOut()
    }
    @IBAction func btnDoneAction(_ sender: Any)
    {
        let trimmedUserName = self.txtForgotPassword.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.txtForgotPassword.text = trimmedUserName
        if self.txtForgotPassword.text == ""
        {
            KSToastView.ks_showToast(mapping.string(forKey: "Please enter email address"), duration: ToastDuration)
        }
        else
        {
            let emailid: String = self.txtForgotPassword.text!
            let myStringMatchesRegEx: Bool = self.isValidEmail(emailAddressString: emailid)
            if myStringMatchesRegEx == false
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter valid email address"), duration: ToastDuration)
            }
            else
            {
                self.view.endEditing(true)
                userForgotPassword()
            }
        }
    }
    
    @IBAction func btnUserLoginAction(_ sender: Any)
    {
        isOnDriverLogin = false
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnSignUpOutletEN.isHidden = false
            self.lblSelectUserEN.isHidden = false
            self.lblSelectDriveEN.isHidden = true
            self.btnDriveOutletEN.alpha = 0.5
            self.btnUserOutletEN.alpha = 1
        }
        else
        {
            self.btnSignUpOutletAR.isHidden = false
            self.lblSelectUserAR.isHidden = false
            self.lblSelectDriveAR.isHidden = true
            self.btnDriveOutletAR.alpha = 0.5
            self.btnUserOutletAR.alpha = 1
        }
        
    }
    
    
    @IBAction func btnUserDriverAction(_ sender: Any)
    {
        isOnDriverLogin = true
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnSignUpOutletEN.isHidden = true
            self.lblSelectUserEN.isHidden = true
            self.lblSelectDriveEN.isHidden = false
            self.btnDriveOutletEN.alpha = 1
            self.btnUserOutletEN.alpha = 0.5
        }
        else
        {
            self.btnSignUpOutletAR.isHidden = true
            self.lblSelectUserAR.isHidden = true
            self.lblSelectDriveAR.isHidden = false
            self.btnDriveOutletAR.alpha = 1
            self.btnUserOutletAR.alpha = 0.5
        }
    }
    //MARK:- Service
    
    func userLoginEnglish()
    {
        var loginType = String()
        if isOnDriverLogin == true
        {
            loginType = "1"
        }
        else
        {
            loginType = "0"
        }
        let kRegiURL = "\(DraiwelnaMainURL)login"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["username" : self.txtUserName.text!,
                         "password" : self.txtUserPassword.text!,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "device_token" : UserDefaults.standard.value(forKey: "deviceToken") as! String,
                         "register_id" : "",
                         "login_user":loginType,
                         "device_type" : DeviceType]
            
            print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                        let dictData = json["data"]
                        let defaults = UserDefaults.standard
                        defaults.set(json["access_token"].stringValue, forKey: "AccessToken")
                        defaults.set(dictData["id"].stringValue, forKey: "UserId")
                        defaults.set(dictData["profile_image"].stringValue, forKey: "UserProfilePicture")
                        defaults.set(dictData["username"].stringValue, forKey: "UserName")
                        defaults.synchronize()
                        
                        if self.isOnDriverLogin == true
                        {
                            UserDefaults.standard.set("1", forKey: "isDriverLogin")
                            let obj:DriverHomeViewController = DriverHomeViewController(nibName: "DriverHomeViewController", bundle: nil)
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                        else
                        {
                            CurrentScreen = 1
                            UserDefaults.standard.set("0", forKey: "isRideStart")
                            UserDefaults.standard.set("0", forKey: "isDriverLogin")
                            let HomeVC = HomeViewController()
                            let navigationController = NavigationController(rootViewController: HomeVC)
                            let mainViewController = MainViewController()
                            mainViewController.rootViewController = navigationController
                            mainViewController.setup(type: UInt(6))
                            
                            navigationController.isNavigationBarHidden = true
                            let window = UIApplication.shared.delegate!.window!!
                            window.rootViewController = mainViewController
                        }
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                     KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func userLoginArabic()
    {
        var loginType = String()
        if isOnDriverLogin == true
        {
            loginType = "1"
        }
        else
        {
            loginType = "0"
        }

        let kRegiURL = "\(DraiwelnaMainURL)login"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["username" : self.txtUserNameAR.text!,
                         "password" : self.txtUserPasswordAR.text!,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "login_user":loginType,
                         "device_token" : UserDefaults.standard.value(forKey: "deviceToken") as! String,
                         "register_id" : "",
                         "device_type" : DeviceType]
            
            print("param: \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                        let dictData = json["data"]
                        let defaults = UserDefaults.standard
                        defaults.set(json["access_token"].stringValue, forKey: "AccessToken")
                        defaults.set(dictData["id"].stringValue, forKey: "UserId")
                        defaults.set(dictData["profile_image"].stringValue, forKey: "UserProfilePicture")
                        defaults.set(dictData["username"].stringValue, forKey: "UserName")
                        defaults.synchronize()
                        
                        if self.isOnDriverLogin == true
                        {
                            UserDefaults.standard.set("1", forKey: "isDriverLogin")
                            let obj:DriverHomeViewController = DriverHomeViewController(nibName: "DriverHomeViewController", bundle: nil)
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                        else
                        {
                            CurrentScreen = 1
                            UserDefaults.standard.set("0", forKey: "isRideStart")
                            UserDefaults.standard.set("0", forKey: "isDriverLogin")
                            let HomeVC = HomeViewController()
                            let navigationController = NavigationController(rootViewController: HomeVC)
                            let mainViewController = MainViewController()
                            mainViewController.rootViewController = navigationController
                            mainViewController.setup(type: UInt(6))
                            
                            navigationController.isNavigationBarHidden = true
                            let window = UIApplication.shared.delegate!.window!!
                            window.rootViewController = mainViewController
                        }
                        
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    
    func userForgotPassword()
    {
        var loginType = String()
        if isOnDriverLogin == true
        {
            loginType = "1"
        }
        else
        {
            loginType = "0"
        }
        let kRegiURL = "\(DraiwelnaMainURL)forgot_password"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["username" : self.txtForgotPassword.text!,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "login_user":loginType,
                         "timezone": TimeZone.current.identifier]
            
             print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        self.viewOfForgotPassword.fadeOut()
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}
