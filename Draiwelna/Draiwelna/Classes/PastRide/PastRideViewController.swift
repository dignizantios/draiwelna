//
//  PastRideViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 28/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView


class PastRideViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,LocationDelegate
{
    
    //MARK:- Variable Declaration
    
    var strOffset = String()
    var arrOfHistory :[JSON] = []
    var strMessage  = String()
    var locationManager: Location!
    var strListType = String()
     var refreshControl: UIRefreshControl!
    
    //MARK:- Outlet Zone
    
    @IBOutlet var btnBackOutletEN: UIButton!
    @IBOutlet var btnBackOutletAR: UIButton!
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var tblPastRide: UITableView!
    
    @IBOutlet var btnUpcomingOutlet: UIButton!
    @IBOutlet var viewUpcomingSelected: UIView!
    @IBOutlet var btnPreviousOutlet: UIButton!
    @IBOutlet var viewPreviousSelected: UIView!
    @IBOutlet var heightOfPrevious: NSLayoutConstraint!
    @IBOutlet var heightOfUpcoming: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
        
        self.lblScreenTitle.text = mapping.string(forKey: "Past Ride")
        
        locationManager = Location()
        locationManager.needToDisplayAlert = false
        locationManager.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white
        let attr = [NSForegroundColorAttributeName:UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: mapping.string(forKey: "Pull to refresh"), attributes:attr)
        self.tblPastRide.addSubview(refreshControl)

        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.tblPastRide.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
            self.btnBackOutletAR.isHidden = true
            self.btnBackOutletEN.isHidden = false
            
            self.btnUpcomingOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
            self.viewUpcomingSelected.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnPreviousOutlet.setTitleColor(UIColor.white, for: .normal)
            self.viewPreviousSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
            
            self.heightOfPrevious.constant = 1
            self.heightOfUpcoming.constant = 3
            self.viewUpcomingSelected.alpha = 1
            
            self.btnUpcomingOutlet.setTitle(mapping.string(forKey: "Upcoming"), for: .normal)
            self.btnPreviousOutlet.setTitle(mapping.string(forKey: "Previous"), for: .normal)
        }
        else
        {
            self.tblPastRide.register(UINib(nibName: "HistoryARCell", bundle: nil), forCellReuseIdentifier: "HistoryARCell")
            self.btnBackOutletAR.isHidden = false
            self.btnBackOutletEN.isHidden = true
            self.btnPreviousOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
            self.viewPreviousSelected.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnUpcomingOutlet.setTitleColor(UIColor.white, for: .normal)
            self.viewUpcomingSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
            
            self.heightOfPrevious.constant = 3
            self.heightOfUpcoming.constant = 1
            self.viewPreviousSelected.alpha = 1
            
            self.btnPreviousOutlet.setTitle(mapping.string(forKey: "Upcoming"), for: .normal)
            self.btnUpcomingOutlet.setTitle(mapping.string(forKey: "Previous"), for: .normal)
        }
        
        print("CheckDriveLogin :- \(UserDefaults.standard.value(forKey: "isDriverLogin") as! String)")
        print("UserId :- \(UserDefaults.standard.value(forKey: "UserId") as! String)")
        print("AccessToken :- \(UserDefaults.standard.value(forKey: "AccessToken") as! String)")

        strOffset = "0"
        strListType = "0"
        getHistoryList()

        // Do any additional setup after loading the view.
    }

   
    
    
    //MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Update Location
    
    func didUpdateLocation(lat: Double?, lon: Double?)
    {
        if UserDefaults.standard.value(forKey: "isDriverLogin") as? String == "0"
        {
            //user
            if UserDefaults.standard.value(forKey: "isRideStart") as? String == "1"
            {
                
            }
        }
        else
        {
            //driver
            if UserDefaults.standard.value(forKey: "isRideStart") as? String == "1"
            {
                UpdateCurrentLocation(latitude: lat!, longitude: lon!)
            }
        }
    }
   
    
    //MARK:- TableView Delegate & dataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrOfHistory.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.white
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        
        tableView.backgroundView = nil
        return arrOfHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let cell:HistoryCell = self.tblPastRide.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
            let dict = arrOfHistory[indexPath.row]
            
            cell.btnEditRideOutlet.isHidden = true
            cell.btnDeleteRideOutlet.isHidden = true
            
            let dictInformation = dict["location_info"]
            cell.lblPickUpLocationEN.text = dictInformation["source_address"].stringValue
            cell.lblDestinationLocationEN.text = dictInformation["desti_address"].stringValue
            let dictFairData = dict["fair_data"]
            //  cell.lblTotalPriceEN.text = "\(dictFairData["currency"].stringValue) \(String(format: "%.2f", dictFairData["fair_amount"].floatValue))"
            if dict["ride_type"].stringValue == "1" || dict["ride_type"].stringValue == "2"
            {
                if dict["ride_type"].stringValue == "1"
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: "Accepted", type: dict["ride_type"].stringValue)
                }
                else
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: "In Progress", type: dict["ride_type"].stringValue)
                }
            }
            else if dict["ride_type"].stringValue == "0" || dict["ride_type"].stringValue == "3" || dict["ride_type"].stringValue == "4" || dict["ride_type"].stringValue == "5"
            {
                if dict["ride_type"].stringValue == "0"
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: "Pending", type: dict["ride_type"].stringValue)
                }
                else if dict["ride_type"].stringValue == "3"
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: "Completed", type: dict["ride_type"].stringValue)
                }
                else
                {
                    cell.lblStatus.attributedText = attributedString(str1: "Status: ", str2: "Cancelled", type: dict["ride_type"].stringValue)
                }
            }

            cell.lblTotalPriceEN.attributedText = attributedString(str1: "\(dictFairData["currency"].stringValue) ", str2: "\((String(format: "%.2f", dictFairData["fair_amount"].floatValue))) ", type: "")
            cell.lblDistanceEN.text = dictFairData["total_km"].stringValue
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dict["book_date"].stringValue)
            let strShowBookDate = DateToString(Formatter: "MMMM dd - hh:mm a", date: strBookDate)
            cell.lblRideDateTime.text = strShowBookDate
            
            return cell
        }
        else
        {
            let cell:HistoryARCell = self.tblPastRide.dequeueReusableCell(withIdentifier: "HistoryARCell") as! HistoryARCell
            let dict = arrOfHistory[indexPath.row]
            
            cell.btnEditRideOutlet.isHidden = true
            cell.btnDeleteRideOutlet.isHidden = true
            
            let dictInformation = dict["location_info"]
            cell.lblPickUpLocationAR.text = dictInformation["source_address"].stringValue
            cell.lblDestinationLocationAR.text = dictInformation["desti_address"].stringValue
            let dictFairData = dict["fair_data"]
            //  cell.lblTotalPriceAR.text = "\(dictFairData["currency"].stringValue) \(String(format: "%.2f", dictFairData["fair_amount"].floatValue))"
            if dict["ride_type"].stringValue == "1" || dict["ride_type"].stringValue == "2"
            {
                if dict["ride_type"].stringValue == "1"
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Accepted"), type: dict["ride_type"].stringValue)
                }
                else
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "In Progress"), type: dict["ride_type"].stringValue)
                }
                
            }
            else if dict["ride_type"].stringValue == "0" || dict["ride_type"].stringValue == "3" || dict["ride_type"].stringValue == "4" || dict["ride_type"].stringValue == "5"
            {
                if dict["ride_type"].stringValue == "0"
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Pending"), type: dict["ride_type"].stringValue)
                }
                else if dict["ride_type"].stringValue == "3"
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Completed"), type: dict["ride_type"].stringValue)
                }
                else
                {
                    cell.lblStatus.attributedText = attributedString(str1: " :الحالة", str2: mapping.string(forKey: "Cancelled"), type: dict["ride_type"].stringValue)
                }
            }

            cell.lblTotalPriceAR.attributedText = attributedString(str1: "\(dictFairData["currency"].stringValue)", str2: "\((String(format: "%.2f", dictFairData["fair_amount"].floatValue))) ", type: "")
            cell.lblDistanceAR.text = dictFairData["total_km"].stringValue
            let strBookDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dict["book_date"].stringValue)
            let strShowBookDate = DateToString(Formatter: "MMMM dd - hh:mm a", date: strBookDate)
            cell.lblRideDateTime.text = strShowBookDate
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    //MARK:- Private Method
    
    func refresh(sender:AnyObject)
    {
        strOffset = "0"
        getHistoryList()
        refreshControl.endRefreshing()
        // Code to refresh table view
    }
    
    private func attributedString(str1:String,str2:String,type:String) -> NSAttributedString?
    {
        let FormatedString = NSMutableAttributedString()
        if type == ""
        {
            let attributes1 = [
                
                NSForegroundColorAttributeName : MySingleton.sharedManager.themeColor,
                NSFontAttributeName : UIFont(name: "OpenSans", size: 16)!
                ] as [String : Any]
            
            let attributes2 = [
                NSForegroundColorAttributeName : MySingleton.sharedManager.themeColor,
                NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 22)!
                ] as [String : Any]
            
            let attributedString1 = NSAttributedString(string: str1, attributes: attributes1)
            let attributedString2 = NSAttributedString(string: str2, attributes: attributes2)
            
            
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                FormatedString.append(attributedString1)
                FormatedString.append(attributedString2)
            }
            else
            {
                FormatedString.append(attributedString2)
                FormatedString.append(attributedString1)
            }
            
        }
        else
        {
            var attributes1  = [String:Any]()
            var attributes2  = [String:Any]()
            if type == "1" || type == "2"
            {
                attributes1 = [
                    NSForegroundColorAttributeName : UIColor.white,
                    NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!
                    ] as [String : Any]
                attributes2 = [
                    NSForegroundColorAttributeName : UIColor.init(red: 31/255, green: 188/255, blue: 42/255, alpha: 1.0),
                    NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 14)!
                    ] as [String : Any]
            }
            else if type == "0" || type == "3" || type == "4" || type == "5"
            {
                attributes1 = [
                    NSForegroundColorAttributeName : UIColor.white,
                    NSFontAttributeName : UIFont(name: "OpenSans", size: 14)!
                    ] as [String : Any]
                attributes2 = [
                    NSForegroundColorAttributeName : MySingleton.sharedManager.themeColor,
                    NSFontAttributeName : UIFont(name: "OpenSans-Bold", size: 14)!
                    ] as [String : Any]
            }
            
            let attributedString1 = NSAttributedString(string: str1, attributes: attributes1)
            let attributedString2 = NSAttributedString(string: str2, attributes: attributes2)
            
            
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                FormatedString.append(attributedString1)
                FormatedString.append(attributedString2)
            }
            else
            {
                FormatedString.append(attributedString2)
                FormatedString.append(attributedString1)
            }
            
        }       
        return FormatedString
    }


    
    //MARK:- Action Zone
    
    @IBAction func btnUpcomingAction(_ sender: Any)
    {
        self.btnUpcomingOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
        self.viewUpcomingSelected.backgroundColor = MySingleton.sharedManager.themeColor
        self.viewUpcomingSelected.alpha = 1
        self.btnPreviousOutlet.setTitleColor(UIColor.white, for: .normal)
        self.viewPreviousSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        self.heightOfPrevious.constant = 1
        self.heightOfUpcoming.constant = 3
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            strListType = "0"
            strOffset = "0"
            getHistoryList()
        }
        else
        {
            strListType = "1"
            strOffset = "0"
            getHistoryList()
        }
        
    }
    
    @IBAction func btnPreviousAction(_ sender: Any)
    {
        self.btnPreviousOutlet.setTitleColor(MySingleton.sharedManager.themeColor, for: .normal)
        self.viewPreviousSelected.backgroundColor = MySingleton.sharedManager.themeColor
        self.btnUpcomingOutlet.setTitleColor(UIColor.white, for: .normal)
        self.viewUpcomingSelected.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        self.viewPreviousSelected.alpha = 1
        self.heightOfPrevious.constant = 3
        self.heightOfUpcoming.constant = 1
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            strListType = "1"
            strOffset = "0"
            getHistoryList()
        }
        else
        {
            strListType = "0"
            strOffset = "0"
            getHistoryList()
        }
        
    }

    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Serice
    
    func getHistoryList()
    {
        let kRegiURL = "\(DraiwelnaMainURL)ride_history"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" :String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "login_user":UserDefaults.standard.value(forKey: "isDriverLogin") as! String,
                         "offset":strOffset,
                         "flag_view":strListType]
            
             print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print(json)
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                        
                        if self.strOffset == "0"
                        {
                            self.arrOfHistory = []
                        }
                        
                        self.strOffset = json["next_offset"].stringValue
                        self.arrOfHistory = json["data"].arrayValue
                        self.tblPastRide.reloadData()
                        
                    }
                    else
                    {
                        self.arrOfHistory = []
                        self.strOffset = json["next_offset"].stringValue
                        self.strMessage = json["msg"].stringValue
                        self.tblPastRide.reloadData()
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    //MARK: - Scrollview Delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if strOffset != "-1" && strOffset != "0"
        {
            if tblPastRide.contentOffset.y >= (tblPastRide.contentSize.height - tblPastRide.bounds.size.height)
            {
                getHistoryList()
                MBProgressHUD.hideAllHUDs(for: self.view!, animated: true)
            }
        }
    }


}
