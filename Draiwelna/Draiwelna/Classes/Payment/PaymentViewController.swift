//
//  PaymentViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 25/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView

class PaymentViewController: UIViewController
{
    //MARK:- Variable Decalration
    
    var strBookingDate = String()
    var strBookingTime = String()
    var strComment = String()
    var strRideType = String()
    var strSpecialComment = String()
    var dictLocationInfo = NSMutableDictionary()
    var dictRideData:JSON!
    
    
    
    //MARK:- Outlet Zone

    @IBOutlet var btnBackOutletEN: UIButton!
    @IBOutlet var btnBackOutletAR: UIButton!
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var viewOfEnglish: UIView!
    @IBOutlet var lblScreenTitle: UILabel!
    
    //MARK:- ViewLife cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        HideGestture()
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
        self.lblScreenTitle.text = mapping.string(forKey: "Payment")       
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.viewOfEnglish.isHidden = false
            self.viewOfArabic.isHidden = true
            
            self.btnBackOutletAR.isHidden = true
            self.btnBackOutletEN.isHidden = false
        }
        else
        {
            self.btnBackOutletAR.isHidden = false
            self.btnBackOutletEN.isHidden = true
            
            self.viewOfEnglish.isHidden = true
            self.viewOfArabic.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreditCardAction(_ sender: Any)
    {
        let obj:CreditCardViewController = CreditCardViewController(nibName: "CreditCardViewController", bundle: nil)
        self.navigationController?.pushViewController(obj, animated: true)
    }

    @IBAction func btnCashPaymentAction(_ sender: Any)
    {
        let BookDate:Date = stringTodate(Formatter: "dd-MM-yyyy", strDate: strBookingDate)
        print("BookDate: \(BookDate)")
        let strConfirmDate:String = DateToString(Formatter: "yyyy-MM-dd", date: BookDate)
        print("strConfirmDate: \(strConfirmDate)")
        
        let BookTime:Date = stringTodate(Formatter: "hh:mm a", strDate: strBookingTime)
        print("BookTime: \(BookTime)")
        let strConfirmTime:String = DateToString(Formatter: "HH:mm:ss", date: BookTime)
        print("strConfirmTime: \(strConfirmTime)")
        
        userViewConfirmRide(confirmDate:strConfirmDate,confirmTime:strConfirmTime)
    }
    
    
    //MARK:- Service
    
    func userViewConfirmRide(confirmDate:String,confirmTime:String)
    {
        let kRegiURL = "\(DraiwelnaMainURL)manage_ride_request"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param = ["ride_id" : "0",
                         "is_cancel" :"0",
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "flag_view":"1",
                         "comment":strComment,
                         "start_date":confirmDate,
                         "start_time":confirmTime,
                        // "ride_type":strRideType,
                       //  "special_comment":strSpecialComment,
                         "pay_type":"0",
                         "ride_data":dictRideData] as [String : Any]
            print("param:- \(param)")
            
            if let theJSONData = try? JSONSerialization.data(withJSONObject: self.dictLocationInfo,options: [])
            {
                if let theJSONText = String(data: theJSONData,encoding: .ascii)
                {
                    print("JSON string = \(theJSONText)")
                    param["location_info"] = theJSONText
                }
            }
            
            print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        print("json \(json)")
                        
                        let obj:HomeViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
                        let navigationController = NavigationController(rootViewController: obj)
                        navigationController.isNavigationBarHidden = true
                        let mainViewController = MainViewController()
                        mainViewController.rootViewController = navigationController
                        mainViewController.setup(type: 6)
                        
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = mainViewController
                        
                        KSToastView.ks_showToast(mapping.string(forKey: "Your booking being processed"), duration: ToastDuration)
                      //  UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}
