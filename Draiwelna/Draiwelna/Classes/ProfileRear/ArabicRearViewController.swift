//
//  ArabicRearViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 22/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView

class ArabicRearViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    //MARK: - Varible Decalration
    
    var arrOfRearProfile = NSMutableArray()
    
    //MARK: - Outlet Zone
    
    @IBOutlet var tblOfRearAr: UITableView!
    
    
    //MARK: - ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        setUpRearArray()
        
        self.tblOfRearAr.register(UINib(nibName: "RearARCell", bundle: nil), forCellReuseIdentifier: "RearARCell")
        self.tblOfRearAr.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        self.tblOfRearAr.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloaTbl), name: NSNotification.Name(rawValue: "tblRearARReload"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Memory management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
   
    //MARK:- Action Zone
    
    @IBAction func btnCloseSideMenuAction(_ sender: Any)
    {
        sideMenuController?.leftViewController?.hideRightViewAnimated(true)
    }
    
    //MARK: - TableView Life Cycle
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrOfRearProfile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {            
            let cell:ProfileCell = self.tblOfRearAr.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
            cell.imgOfUserProfile.layer.masksToBounds = false
            cell.imgOfUserProfile.layer.borderColor = UIColor.clear.cgColor
            cell.imgOfUserProfile.layer.cornerRadius = cell.imgOfUserProfile.frame.height/2
            cell.imgOfUserProfile.clipsToBounds = true
            cell.btnLogOutOutletAR.isHidden = false
            cell.btnLogOutOutletEN.isHidden = true
            cell.btnLogOutOutletAR.addTarget(self, action: #selector(ArabicRearViewController.btnLogOutAction), for: .touchUpInside)
            
            cell.lblUserName.text = UserDefaults.standard.value(forKey: "UserName") as? String
            
            cell.imgOfUserProfile.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            cell.imgOfUserProfile.layer.borderWidth = 2
            
            let strImage = UserDefaults.standard.value(forKey: "UserProfilePicture") as? String
            let strPlaceImage="user_placeholder_register_screen.png"
            let urlImage:NSURL = NSURL(string: strImage!)!
            cell.imgOfUserProfile.sd_setImage(with: urlImage as URL, placeholderImage:UIImage(named:strPlaceImage))
            
            return cell
            
            
        }
        else
        {
            
            let cell:RearARCell = self.tblOfRearAr.dequeueReusableCell(withIdentifier: "RearARCell") as! RearARCell
            let strTitleName = (arrOfRearProfile.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Name") as! String
            cell.lblName.text = strTitleName
            cell.imgOfCell.image = UIImage.init(named: (arrOfRearProfile.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Image") as! String)
            
            let selected = (arrOfRearProfile.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Selected") as! String
            if selected == "0"
            {
                cell.viewOfHighlightCell.isHidden = true
            }
            else
            {
                cell.viewOfHighlightCell.isHidden = false
            }
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let strTitleName = (arrOfRearProfile.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Name") as! String
        if strTitleName == ""
        {
            return 60
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let strTitleName = (arrOfRearProfile.object(at: indexPath.row) as! NSMutableDictionary).value(forKey: "Name") as! String
        if strTitleName == ""
        {
            return 60
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            return
        }
        for i in 0 ..< arrOfRearProfile.count
        {
            let dict = (arrOfRearProfile.object(at: i) as! NSMutableDictionary)
            dict.setValue("0", forKey: "Selected")
        }
        let dict = (arrOfRearProfile.object(at: indexPath.row) as! NSMutableDictionary)
        dict.setValue("1", forKey: "Selected")
        
        
        self.tblOfRearAr.reloadData()
        
        
         self.perform(#selector(self.reloadData), with: indexPath, afterDelay: 0.2)
        
    }
    
    //MARK:- Action Zone
    
    func btnLogOutAction(sender:UIButton)
    {
        let alert = UIAlertController(title: AppName, message: mapping.string(forKey: "Are you sure want to logout?"), preferredStyle: UIAlertControllerStyle.alert);
        let cancelAction = UIAlertAction(title: mapping.string(forKey: "No"), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
            
        }
        let okAction = UIAlertAction(title: mapping.string(forKey: "Yes"), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
           self.userLogout()
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Private Method
    
    func reloaTbl()
    {
        setUpRearArray()
        self.tblOfRearAr.reloadData()
    }
    
    func setUpRearArray()
    {
        arrOfRearProfile = NSMutableArray()
        var dict = NSMutableDictionary()
        
        dict.setValue("Picture", forKey: "Name")
        dict.setValue("Image", forKey: "Image")
        dict.setValue("0", forKey: "Selected")
        dict.setValue("0", forKey: "Selected_Image")
        arrOfRearProfile.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue(mapping.string(forKey: "Home"), forKey: "Name")
        dict.setValue("ic_home_sidemenu.png", forKey: "Image")
        dict.setValue("1", forKey: "Selected")
        dict.setValue("", forKey: "Selected_Image")
        arrOfRearProfile.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue(mapping.string(forKey: "Rides"), forKey: "Name")
        dict.setValue("ic_book_ride_home_screen.png", forKey: "Image")
        dict.setValue("0", forKey: "Selected")
        dict.setValue("", forKey: "Selected_Image")
        arrOfRearProfile.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue(mapping.string(forKey: "Account"), forKey: "Name")
        dict.setValue("ic_about_sidemenu.png", forKey: "Image")
        dict.setValue("0", forKey: "Selected")
        dict.setValue("", forKey: "Selected_Image")
        arrOfRearProfile.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue(mapping.string(forKey: "Language"), forKey: "Name")
        dict.setValue("ic_language_sidemenu.png", forKey: "Image")
        dict.setValue("0", forKey: "Selected")
        dict.setValue("", forKey: "Selected_Image")
        arrOfRearProfile.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue(mapping.string(forKey: "Contact us"), forKey: "Name")
        dict.setValue("ic_contact_sidemenu.png", forKey: "Image")
        dict.setValue("0", forKey: "Selected")
        dict.setValue("", forKey: "Selected_Image")
        arrOfRearProfile.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue(mapping.string(forKey: "About us"), forKey: "Name")
        dict.setValue("ic_about_sidemenu.png", forKey: "Image")
        dict.setValue("0", forKey: "Selected")
        dict.setValue("", forKey: "Selected_Image")
        arrOfRearProfile.add(dict)
        
        for i in 0..<arrOfRearProfile.count
        {
            let dict = arrOfRearProfile.object(at: i) as! NSMutableDictionary
            if i == CurrentScreen{
                dict.setValue("1", forKey: "Selected")
            }
            else{
                dict.setValue("0", forKey: "Selected")
            }
        }
    }
    
    func reloadData(index:IndexPath)
    {
        let strTitleName = (arrOfRearProfile.object(at: index.row) as! NSMutableDictionary).value(forKey: "Name") as! String
        if strTitleName == mapping.string(forKey: "Home")
        {
            CurrentScreen = 1
            let navigationController = sideMenuController!.rootViewController as! NavigationController
            let obj = HomeViewController()
            navigationController.setViewControllers([obj], animated: false)
            sideMenuController!.hideRightViewAnimated(true)
        }
        else if strTitleName == mapping.string(forKey: "Rides")
        {
            CurrentScreen = 2
            let navigationController = sideMenuController!.rootViewController as! NavigationController
            let obj = HistoryViewController()
            navigationController.setViewControllers([obj], animated: false)
            sideMenuController!.hideRightViewAnimated(true)
        }
        else if strTitleName == mapping.string(forKey: "Account")
        {
            CurrentScreen = 3
            let navigationController = sideMenuController!.rootViewController as! NavigationController
            let obj = AccountViewController()
            navigationController.setViewControllers([obj], animated: false)
            sideMenuController!.hideRightViewAnimated(true)
        }
        else if strTitleName == mapping.string(forKey: "Language")
        {
            let obj:LanguageViewController = LanguageViewController(nibName: "LanguageViewController", bundle: nil)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true, completion: nil)
            sideMenuController!.hideRightViewAnimated(true)
        }
        else if strTitleName == mapping.string(forKey: "Contact us")
        {
            CurrentScreen = 5
            let navigationController = sideMenuController!.rootViewController as! NavigationController
            let obj = ContactUsViewController()
            navigationController.setViewControllers([obj], animated: false)
            sideMenuController!.hideRightViewAnimated(true)
        }
        else if strTitleName == mapping.string(forKey: "About us")
        {
            CurrentScreen = 6
            let navigationController = sideMenuController!.rootViewController as! NavigationController
            let obj = AboutUsViewController()
            navigationController.setViewControllers([obj], animated: false)
            sideMenuController!.hideRightViewAnimated(true)
        }       
        
    }

    //MARK:- Logout
    
    func userLogout()
    {
        let kRegiURL = "\(DraiwelnaMainURL)logout"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "device_token" : UserDefaults.standard.value(forKey: "deviceToken") as! String,
                         "user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "user_type":UserDefaults.standard.value(forKey: "isDriverLogin") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String]
            
            print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        print(json)
                      //  KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                        UserDefaults.standard.removeObject(forKey: "UserId")
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as!ViewController
                        let navigationController = NavigationController(rootViewController: nextViewController)
                        
                        let mainViewController = MainViewController()
                        mainViewController.rootViewController = navigationController
                        mainViewController.setup(type: 6)
                        
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = mainViewController
                        
                        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    
}
