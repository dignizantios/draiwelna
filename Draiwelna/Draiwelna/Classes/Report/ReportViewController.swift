//
//  ReportViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 19/08/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import KSToastView

class ReportViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK:- Variable Declaration
    
    var arrReportType = NSMutableArray()
    var strRideId = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var lblReportTitle: UILabel!
    @IBOutlet var btnCloseOutletAR: UIButton!
    @IBOutlet var btnCloseOutletEN: UIButton!
    @IBOutlet var txtSubjectName: UITextField!
    @IBOutlet var imgDropDownAR: UIImageView!
    @IBOutlet var imgDropDownEN: UIImageView!
    @IBOutlet var txtViewMessage: UITextView!
    @IBOutlet var lblPlaceHolderAR: UILabel!
    @IBOutlet var lblPlaceHolderEN: UILabel!
    @IBOutlet var btnSubmitOutlet: UIButton!
    @IBOutlet var topOfSubjetPicker: NSLayoutConstraint!
    @IBOutlet var btnCancelOutlet: UIButton!
    @IBOutlet var btnDoneOutlet: UIButton!
    @IBOutlet var normalPickerView: UIPickerView!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.topOfSubjetPicker.constant = UIScreen.main.bounds.height
        self.lblReportTitle.text = mapping.string(forKey: "Report")
        self.txtSubjectName.placeholder = mapping.string(forKey: "Subject")
        self.btnSubmitOutlet.setTitle(mapping.string(forKey: "Submit"), for: .normal)
        
        self.txtViewMessage.layer.cornerRadius = 5
        self.txtViewMessage.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.txtViewMessage.layer.borderWidth = 1

        self.btnSubmitOutlet.layer.cornerRadius = 5
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnCloseOutletEN.isHidden = false
            self.btnCloseOutletAR.isHidden = true
            
            self.imgDropDownAR.isHidden = true
            self.imgDropDownEN.isHidden = false
            
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
            
            self.lblPlaceHolderAR.isHidden = true
            self.lblPlaceHolderEN.isHidden = false
            
            [txtSubjectName].forEach({ customizeTextFieldEN(textfield: $0) })
            arrReportType.add("Payment")
            arrReportType.add("Late")
            arrReportType.add("Complaint")
            arrReportType.add("Other")
            
            self.txtViewMessage.textContainerInset = UIEdgeInsets(top: 9 , left: 13, bottom: 0, right: 10)
            
            self.txtSubjectName.textAlignment = .left
            self.txtViewMessage.textAlignment = .left
            
        }
        else
        {
            self.txtSubjectName.textAlignment = .right
            self.txtViewMessage.textAlignment = .right
            
            self.lblPlaceHolderAR.isHidden = false
            self.lblPlaceHolderEN.isHidden = true
            
            self.imgDropDownAR.isHidden = false
            self.imgDropDownEN.isHidden = true
            
            self.btnCancelOutlet.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnDoneOutlet.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
            
            self.btnCloseOutletEN.isHidden = true
            self.btnCloseOutletAR.isHidden = false
            
            [txtSubjectName].forEach({ customizeTextFieldAR(textfield: $0) })
          
            self.txtViewMessage.textContainerInset = UIEdgeInsets(top: 9 , left: 10, bottom: 0, right: 13)
            
            arrReportType.add("دفع")
            arrReportType.add("متأخر")
            arrReportType.add("شكوى")
            arrReportType.add("آخر")
        }
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.backgroundColor = UIColor.clear
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.backgroundColor = UIColor.clear
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField == self.txtSubjectName
        {
            self.movePickerUp()
            return false
        }
        self.movePickerDown()
        return true
    }
    
    
    //MARK:- TextView Delegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        self.movePickerDown()
        return true
    }
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        if self.txtViewMessage.text == ""
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                self.lblPlaceHolderEN.isHidden = false
                self.lblPlaceHolderAR.isHidden = true
            }
            else
            {
                self.lblPlaceHolderEN.isHidden = true
                self.lblPlaceHolderAR.isHidden = false
            }
        }
        else
        {
            self.lblPlaceHolderEN.isHidden = true
            self.lblPlaceHolderAR.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    //MARK:- PickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arrReportType.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arrReportType.object(at: row) as? String
    }


    
    //MARK: - View UP and Down Method
    
    func movePickerUp()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.topOfSubjetPicker.constant = UIScreen.main.bounds.height - 180
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()       
        
    }
    
    func movePickerDown()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.topOfSubjetPicker.constant = UIScreen.main.bounds.height
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
    }

    
    //MARK:- Action Zone
    
    @IBAction func btnCloseAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any)
    {
        let trimmedUserFirstName = self.txtViewMessage.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.txtViewMessage.text = trimmedUserFirstName
        
        if self.txtSubjectName.text == ""
        {
            KSToastView.ks_showToast(mapping.string(forKey: "Please enter a subject."), duration: ToastDuration)
        }
        else if self.txtViewMessage.text == ""
        {
           KSToastView.ks_showToast(mapping.string(forKey: "Please enter comments"), duration: ToastDuration) 
        }
        else
        {
            self.movePickerDown()
            postRideReport()
        }
        
    }

    @IBAction func btnCancelAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.movePickerDown()
        }
        else
        {
            self.txtSubjectName.text = self.arrReportType.object(at: self.normalPickerView.selectedRow(inComponent: 0)) as? String
            self.movePickerDown()
        }
    }
    @IBAction func btnDoneAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.txtSubjectName.text = self.arrReportType.object(at: self.normalPickerView.selectedRow(inComponent: 0)) as? String
            self.movePickerDown()
        }
        else
        {
             self.movePickerDown()
        }
    }
    
    //MARK: - Service
    
    func postRideReport()
    {
        let kRegiURL = "\(DraiwelnaMainURL)add_ride_report"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param = ["user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "ride_id":strRideId,
                         "subject":self.txtSubjectName.text!,
                         "message":self.txtViewMessage.text!]
            
            print("Parameter:- \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user:basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print("json:-\(json)")
                    if json["flag"].stringValue == "1"
                    {
                        self.dismiss(animated: true, completion: nil)
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        self.dismiss(animated: true, completion: nil)
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
                
            }
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

}
