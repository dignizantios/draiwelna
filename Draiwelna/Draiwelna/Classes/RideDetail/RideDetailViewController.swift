//
//  RideDetailViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 24/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import GoogleMaps
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import DropDown
import KSToastView

class RideDetailViewController: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate
{
    //MARK:- Variable Declaration
    
    var LocationDropDown = DropDown()
    var isTrasform = Bool()
    var arrOfSuggestionLocation: [String] = []
    var activeTextFieldTag = Int()
    var origin = CLLocationCoordinate2D()
    var destination = CLLocationCoordinate2D()
    var currentLocation = CLLocationCoordinate2D()
    var strBookingDate = String()
    var strBookingTime = String()
    var strComment = String()
    var strRideType = String()
    var strSpecialComment = String()
    var dictLocationInfo = NSMutableDictionary()
    var locationManager = CLLocationManager()
    var didFindLocation = Bool()
    var strCurrentAddress =  String()
    var dictRideData:JSON!
    var isComeFromEditRide = Bool()
    var rideEditID = String()
    
    //MARK:- Outlet Zone
    
    //Common Outlet
    
    @IBOutlet var ViewOfGoogleMap: GMSMapView!
    @IBOutlet var btnConfirmBookingOutlet: UIButton!
    @IBOutlet var lblTitleArrivedIn: UILabel!
    @IBOutlet var lblArrivalTime: UILabel!
    @IBOutlet var lblStaticFareTitle: UILabel!
    //@IBOutlet var lblTotalKM: UILabel!
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var lblTotalPrice: UILabel!
    
    //English Outlet
    @IBOutlet var btnBackOutletEN: UIButton!
    @IBOutlet var txtPickUpEN: SkyFloatingLabelTextField!
    @IBOutlet var txtDropEN: SkyFloatingLabelTextField!
    @IBOutlet var imgOfBgTransformLocationEN: UIImageView!
    @IBOutlet var viewOfEnglish: UIView!
    
    //Arabic Outlet
    
    @IBOutlet var btnBackOutletAR: UIButton!
    @IBOutlet var viewOfArabicOutlet: UIView!
    @IBOutlet var txtPickUpAR: SkyFloatingLabelTextField!
    @IBOutlet var txtDropAR: SkyFloatingLabelTextField!
    @IBOutlet var imgOfBgTransformLocationAR: UIImageView!
    
    
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
       
        setupUserCurrentLocation()
        
        self.imgOfBgTransformLocationEN.layer.cornerRadius = 5
        self.imgOfBgTransformLocationAR.layer.cornerRadius = 5
        
        self.btnConfirmBookingOutlet.setTitle(mapping.string(forKey: "Confirm booking"), for: .normal)
        self.lblStaticFareTitle.text = mapping.string(forKey: "Fare estimate")
        self.lblTitleArrivedIn.text = mapping.string(forKey: "Ride Duration")
        
        self.lblScreenTitle.text = mapping.string(forKey: "Ride Details")
        self.btnConfirmBookingOutlet.layer.cornerRadius = 5       
        
        HideGestture()
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            self.locationManager.distanceFilter = kCLDistanceFilterNone
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            self.viewOfEnglish.isHidden = false
            self.viewOfArabicOutlet.isHidden = true
            
            self.btnBackOutletAR.isHidden = true
            self.btnBackOutletEN.isHidden = false
            
         //   [txtPickUpEN,txtDropEN].forEach({ customizeTextFieldEN(textfield: $0) })
            
            self.txtPickUpEN.addTarget(self, action: #selector(RideDetailViewController.didChangeText(textField:)), for: .editingChanged)
            self.txtDropEN.addTarget(self, action: #selector(RideDetailViewController.didChangeText(textField:)), for: .editingChanged)
        }
        else
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self          
            self.locationManager.distanceFilter = kCLDistanceFilterNone
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            self.viewOfEnglish.isHidden = true
            self.viewOfArabicOutlet.isHidden = false
            
            self.btnBackOutletAR.isHidden = false
            self.btnBackOutletEN.isHidden = true
            
           // [txtPickUpAR,txtDropAR].forEach({ customizeTextFieldEN(textfield: $0) })
            
            self.txtPickUpAR.addTarget(self, action: #selector(RideDetailViewController.didChangeText(textField:)), for: .editingChanged)
            self.txtDropAR.addTarget(self, action: #selector(RideDetailViewController.didChangeText(textField:)), for: .editingChanged)
            
            self.txtPickUpAR.isLTRLanguage = false
            self.txtDropAR.isLTRLanguage = false
        }
        
        if isComeFromEditRide == true
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                self.txtPickUpEN.text = dictLocationInfo["source_address"] as? String
                self.txtDropEN.text = dictLocationInfo["desti_address"] as? String
            }
            else
            {
                self.txtPickUpAR.text = dictLocationInfo["source_address"] as? String
                self.txtDropAR.text = dictLocationInfo["desti_address"] as? String
            }
            self.strCurrentAddress = (dictLocationInfo["book_address"] as? String)!
            self.origin.latitude = Double((dictLocationInfo["source_lat"] as? String)!)!
            self.origin.longitude = Double((dictLocationInfo["source_lng"] as? String)!)!
            self.destination.latitude = Double((dictLocationInfo["desti_lat"] as? String)!)!
            self.destination.longitude = Double((dictLocationInfo["desti_lng"] as? String)!)!
            self.lblArrivalTime.text = self.dictRideData["total_time"].stringValue
            self.lblTotalPrice.text = "\(dictRideData["currency"].stringValue ) \(dictRideData["fair_amount"].stringValue)"
           
          //  self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
          //  self.setUpPinOnMap(lat: self.destination.latitude, long: self.destination.longitude,type: 2)
            drawPath()
        }
        else
        {
            rideEditID = "0"
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.getEstimateAmount), name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
       
    }

    //MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Private Method
    
    func getEstimateAmount()
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if self.txtPickUpEN.text != "" &&  self.txtDropEN.text != ""
            {
                self.dictLocationInfo = NSMutableDictionary()
                self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                self.dictLocationInfo.setValue(self.txtPickUpEN.text!, forKey: "source_address")
                self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                self.dictLocationInfo.setValue(self.txtDropEN.text, forKey: "desti_address")
                self.userViewBookRide()
                self.drawPath()
            }
            else if self.txtPickUpEN.text != ""
            {
                self.ViewOfGoogleMap.clear()
                let marker = GMSMarker()
                marker.map = nil
                let polyLine = GMSPolyline()
                polyLine.map = nil
                self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
            }
            
        }
        else
        {
            if self.txtPickUpAR.text != "" &&  self.txtDropAR.text != ""
            {
                self.dictLocationInfo = NSMutableDictionary()
                self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                self.dictLocationInfo.setValue(self.txtPickUpAR.text!, forKey: "source_address")
                self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                self.dictLocationInfo.setValue(self.txtDropAR.text, forKey: "desti_address")
                self.userViewBookRide()
                self.drawPath()
            }
            else if self.txtPickUpAR.text != ""
            {
                self.ViewOfGoogleMap.clear()
                let marker = GMSMarker()
                marker.map = nil
                let polyLine = GMSPolyline()
                polyLine.map = nil
                self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
            }
            
        }

    }
    
    func openSetting()
    {
        let alertController = UIAlertController (title: AppName, message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func setupUserCurrentLocation()
    {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        didFindLocation = true
    }
 
    
    //MARK:- CLLocationManager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if didFindLocation == true
        {
            didFindLocation = false
            currentLocation = manager.location!.coordinate
           //
            print("locations = \(currentLocation.latitude) \(currentLocation.longitude)")
            strCurrentAddress = reverseGeoCoding(Latitude: currentLocation.latitude, Longitude: currentLocation.longitude)
            
            print("strAddress \(strCurrentAddress)")
           
            if isComeFromEditRide == false
            {
                origin = manager.location!.coordinate
                let camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 20)
                ViewOfGoogleMap.animate(to: camera)
                
                // Creates a marker in the center of the map.
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
                let Address = reverseGeoCoding(Latitude: currentLocation.latitude, Longitude: currentLocation.longitude)
               
                marker.title = Address
                marker.icon = UIImage.init(named: "pin_pickup_location.png")
                marker.map = ViewOfGoogleMap
            }            
            
        }
        else
        {
            didFindLocation = false
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            setupUserCurrentLocation()
            // manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            openSetting()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            openSetting()
            break
            //   default:
            // break
        }
    }
    
    //MARK:- Reverse gecode and get local address from Latitude and longitude
    
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                
                
                let lines = address.lines! as [String]
                
                currentAddress = lines.joined(separator: ",")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
                if self.isComeFromEditRide == false
                {
                    if self.txtPickUpAR.text! == "" || self.txtPickUpEN.text! == ""
                    {
                        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                        {
                            self.txtPickUpEN.text = currentAddress
                        }
                        else
                        {
                            self.txtPickUpAR.text = currentAddress
                        } 
                    }
                    
                }
                
                print("Address - ",currentAddress)
                
            }
        }
        return currentAddress
    }


    
    //MARK:- Action Zone
    
    @IBAction func btnRotateLocationAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if self.txtPickUpEN.text == "" || self.txtDropEN.text == ""
            {
                return
            }
            if isTrasform == false
            {
                isTrasform = true
                let strPickup = self.txtPickUpEN.text
                let strDrop = self.txtDropEN.text
                
                self.txtDropEN.text = strPickup
                self.txtPickUpEN.text = strDrop
                
                //change pin
                
                ViewOfGoogleMap.clear()
                let marker = GMSMarker()
                marker.map = nil
                
                var dummyOrigin = CLLocationCoordinate2D()
                dummyOrigin.latitude = origin.latitude
                dummyOrigin.longitude = origin.longitude
                
                var dummyDestination = CLLocationCoordinate2D()
                dummyDestination.latitude = destination.latitude
                dummyDestination.longitude = destination.longitude
                
                self.destination.latitude = dummyOrigin.latitude
                self.destination.longitude = dummyOrigin.longitude
                self.setUpPinOnMap(lat: self.destination.latitude, long: self.destination.longitude,type: 2)
                
                self.origin.latitude = dummyDestination.latitude
                self.origin.longitude = dummyDestination.longitude
                self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
                
                self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                self.dictLocationInfo.setValue(self.txtPickUpEN.text!, forKey: "source_address")
                self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                self.dictLocationInfo.setValue(self.txtDropEN.text, forKey: "desti_address")
                self.userViewBookRide()
                
                drawPath()
            }
            else
            {
                isTrasform = false
                let strPickup = self.txtDropEN.text
                let strDrop = self.txtPickUpEN.text
                
                self.txtPickUpEN.text = strPickup
                self.txtDropEN.text = strDrop
                
                //change pin
                
                ViewOfGoogleMap.clear()
                let marker = GMSMarker()
                marker.map = nil
                
                var dummyOrigin = CLLocationCoordinate2D()
                dummyOrigin.latitude = destination.latitude
                dummyOrigin.longitude = destination.longitude
                
                var dummyDestination = CLLocationCoordinate2D()
                dummyDestination.latitude = origin.latitude
                dummyDestination.longitude = origin.longitude
                
                self.origin.latitude = dummyOrigin.latitude
                self.origin.longitude = dummyOrigin.longitude
                self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
                
                self.destination.latitude = dummyDestination.latitude
                self.destination.longitude = dummyDestination.longitude
                self.setUpPinOnMap(lat: self.destination.latitude, long: self.destination.longitude,type: 2)
                
                self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                self.dictLocationInfo.setValue(self.txtPickUpEN.text!, forKey: "source_address")
                self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                self.dictLocationInfo.setValue(self.txtDropEN.text, forKey: "desti_address")
                self.userViewBookRide()
                
                drawPath()
            }
            
        }
        else
        {
            if self.txtPickUpAR.text == "" || self.txtDropAR.text == ""
            {
                return
            }
            if isTrasform == false
            {
                isTrasform = true
                let strPickup = self.txtPickUpAR.text
                let strDrop = self.txtDropAR.text
                
                self.txtDropAR.text = strPickup
                self.txtPickUpAR.text = strDrop
                
                //change pin
                
                ViewOfGoogleMap.clear()
                let marker = GMSMarker()
                marker.map = nil
                
                var dummyOrigin = CLLocationCoordinate2D()
                dummyOrigin.latitude = origin.latitude
                dummyOrigin.longitude = origin.longitude
                
                var dummyDestination = CLLocationCoordinate2D()
                dummyDestination.latitude = destination.latitude
                dummyDestination.longitude = destination.longitude
                
                self.destination.latitude = dummyOrigin.latitude
                self.destination.longitude = dummyOrigin.longitude
                self.setUpPinOnMap(lat: self.destination.latitude, long: self.destination.longitude,type: 2)
                
                self.origin.latitude = dummyDestination.latitude
                self.origin.longitude = dummyDestination.longitude
                self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
                
                self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                self.dictLocationInfo.setValue(self.txtPickUpAR.text!, forKey: "source_address")
                self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                self.dictLocationInfo.setValue(self.txtDropAR.text, forKey: "desti_address")
                self.userViewBookRide()
                
                drawPath()
            }
            else
            {
                isTrasform = false
                let strPickup = self.txtDropAR.text
                let strDrop = self.txtPickUpAR.text
                
                self.txtPickUpAR.text = strPickup
                self.txtDropAR.text = strDrop
                
                //change pin
                
                ViewOfGoogleMap.clear()
                let marker = GMSMarker()
                marker.map = nil
                
                var dummyOrigin = CLLocationCoordinate2D()
                dummyOrigin.latitude = destination.latitude
                dummyOrigin.longitude = destination.longitude
                
                var dummyDestination = CLLocationCoordinate2D()
                dummyDestination.latitude = origin.latitude
                dummyDestination.longitude = origin.longitude
                
                self.origin.latitude = dummyOrigin.latitude
                self.origin.longitude = dummyOrigin.longitude
                self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
                
                self.destination.latitude = dummyDestination.latitude
                self.destination.longitude = dummyDestination.longitude
                self.setUpPinOnMap(lat: self.destination.latitude, long: self.destination.longitude,type: 2)
                
                self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                self.dictLocationInfo.setValue(self.txtPickUpAR.text!, forKey: "source_address")
                self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                self.dictLocationInfo.setValue(self.txtDropAR.text, forKey: "desti_address")
                self.userViewBookRide()
                drawPath()
            }

        }
        
    }
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnConfirmBookingAction(_ sender: Any)
    {
        if isComeFromEditRide == true
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                let trimmedPickUp = self.txtPickUpEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtPickUpEN.text = trimmedPickUp
                
                let trimmeDrop = self.txtDropEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtDropEN.text = trimmeDrop
                
                if self.txtPickUpEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a pickup location"), duration: ToastDuration)
                }
                else if self.txtDropEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a drop location"), duration: ToastDuration)
                }
                else
                {
                    let BookDate:Date = stringTodate(Formatter: "dd-MM-yyyy", strDate: strBookingDate)
                    print("BookDate: \(BookDate)")
                    let strConfirmDate:String = DateToString(Formatter: "yyyy-MM-dd", date: BookDate)
                    print("strConfirmDate: \(strConfirmDate)")
                    
                    let BookTime:Date = stringTodate(Formatter: "hh:mm a", strDate: strBookingTime)
                    print("BookTime: \(BookTime)")
                    let strConfirmTime:String = DateToString(Formatter: "HH:mm:ss", date: BookTime)
                    print("strConfirmTime: \(strConfirmTime)")
                    
                    userViewConfirmRide(confirmDate:strConfirmDate,confirmTime:strConfirmTime)
                }
            }
            else
            {
                let trimmedPickUp = self.txtPickUpAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtPickUpAR.text = trimmedPickUp
                
                let trimmeDrop = self.txtDropAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtDropAR.text = trimmeDrop
                
                if self.txtPickUpAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a pickup location"), duration: ToastDuration)
                }
                else if self.txtDropAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a drop location"), duration: ToastDuration)
                }
                else
                {
                    let BookDate:Date = stringTodate(Formatter: "dd-MM-yyyy", strDate: strBookingDate)
                    print("BookDate: \(BookDate)")
                    let strConfirmDate:String = DateToString(Formatter: "yyyy-MM-dd", date: BookDate)
                    print("strConfirmDate: \(strConfirmDate)")
                    
                    let BookTime:Date = stringTodate(Formatter: "hh:mm a", strDate: strBookingTime)
                    print("BookTime: \(BookTime)")
                    let strConfirmTime:String = DateToString(Formatter: "HH:mm:ss", date: BookTime)
                    print("strConfirmTime: \(strConfirmTime)")
                    
                    userViewConfirmRide(confirmDate:strConfirmDate,confirmTime:strConfirmTime)
                }
            }
        }
        else
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                let trimmedPickUp = self.txtPickUpEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtPickUpEN.text = trimmedPickUp
                
                let trimmeDrop = self.txtDropEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtDropEN.text = trimmeDrop
                
                if self.txtPickUpEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a pickup location"), duration: ToastDuration)
                }
                else if self.txtDropEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a drop location"), duration: ToastDuration)
                }
                else
                {
                    let obj:PaymentViewController = PaymentViewController(nibName: "PaymentViewController", bundle: nil)
                    print("BookDate: \(strBookingDate)")
                   // obj.strSpecialComment = strSpecialComment
                   // obj.strRideType = strRideType
                    obj.strBookingTime = strBookingTime
                    obj.strBookingDate = strBookingDate
                    obj.dictLocationInfo = dictLocationInfo
                    obj.dictRideData = dictRideData
                    obj.strComment = strComment
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
            else
            {
                let trimmedPickUp = self.txtPickUpAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtPickUpAR.text = trimmedPickUp
                
                let trimmeDrop = self.txtDropAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.txtDropAR.text = trimmeDrop
                
                if self.txtPickUpAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a pickup location"), duration: ToastDuration)
                }
                else if self.txtDropAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select a drop location"), duration: ToastDuration)
                }
                else
                {
                    let obj:PaymentViewController = PaymentViewController(nibName: "PaymentViewController", bundle: nil)
                    print("BookDate: \(strBookingDate)")
                   // obj.strSpecialComment = strSpecialComment
                  //  obj.strRideType = strRideType
                    obj.strBookingTime = strBookingTime
                    obj.strBookingDate = strBookingDate
                    obj.dictLocationInfo = dictLocationInfo
                    obj.dictRideData = dictRideData
                    obj.strComment = strComment
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }
       
    }
    //MARK:- TextField Delegate
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.delegate = self
    }
    
    func didChangeText(textField:UITextField)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if textField == self.txtPickUpEN
            {
                activeTextFieldTag = 1
            }
            else
            {
                activeTextFieldTag = 2
            }
        }
        else
        {
            if textField == self.txtPickUpAR
            {
                activeTextFieldTag = 1
            }
            else
            {
                activeTextFieldTag = 2
            }
        }

        getAddressComplete(Location:textField.text!)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        activeTextFieldTag = Int()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if textField == self.txtPickUpEN
            {
                activeTextFieldTag = 1
            }
            else
            {
                activeTextFieldTag = 2
            }
        }
        else
        {
            if textField == self.txtPickUpAR
            {
                activeTextFieldTag = 1
            }
            else
            {
                activeTextFieldTag = 2
            }
        }
        
        let obj:SelectLocationViewController = SelectLocationViewController(nibName: "SelectLocationViewController", bundle: nil)
        obj.activeTextField = activeTextFieldTag
        obj.strLocation = textField.text!
        obj.RideDetail = self
        obj.isComeFromEdit = isComeFromEditRide
        self.navigationController?.pushViewController(obj, animated: true)
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        return  textField.resignFirstResponder()
    }
    
    //MARK:- Set Pin On Map
    
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 20)
        ViewOfGoogleMap.animate(to: camera)
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
      //  let Address = reverseGeoCoding(Latitude: lat, Longitude: long)
        //marker.title = Address
        if type == 1
        {
           marker.icon = UIImage.init(named: "pin_pickup_location.png")
        }
        else
        {
            marker.icon = UIImage.init(named: "pin_drop_location.png")
        }
        
        marker.map = ViewOfGoogleMap
    }
  
    
    //MARK:- DropDown Configure
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
    }
    
    //MARK:- Service
    
    func getAddressComplete(Location:String)
    {
        let urlPath = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
        let url = urlPath + "input=\(Location)&types=geocode&sensor=false&key=AIzaSyCczddW-L4lKmZaq-I-lW7O9goKJk5Ni0U"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
              
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print(json)
                    self.arrOfSuggestionLocation = [String]()
                    let arrLocation = json["predictions"].arrayValue
                    if arrLocation.count == 0
                    {
                        return
                    }
                    for i in 0 ..< arrLocation.count
                    {
                        let strLocation = (arrLocation[i].dictionaryValue)["description"]?.stringValue
                        self.arrOfSuggestionLocation.append(strLocation!)
                    }
                    if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                    {
                        if self.activeTextFieldTag == 1
                        {
                            self.configDD(dropdown: self.LocationDropDown, sender: self.txtPickUpEN)
                        }
                        else
                        {
                            self.configDD(dropdown: self.LocationDropDown, sender: self.txtDropEN)
                        }
                    }
                    else
                    {
                        if self.activeTextFieldTag == 1
                        {
                            self.configDD(dropdown: self.LocationDropDown, sender: self.txtPickUpAR)
                        }
                        else
                        {
                            self.configDD(dropdown: self.LocationDropDown, sender: self.txtDropAR)
                        }
                    }
                    
                    
                    self.LocationDropDown.dataSource = self.arrOfSuggestionLocation
                    self.LocationDropDown.show()
                    if self.activeTextFieldTag == 1
                    {
                        self.LocationDropDown.selectionAction = { (index, item) in
                            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                            {
                                if self.txtDropEN.text == ""
                                {
                                    self.ViewOfGoogleMap.clear()
                                    let marker = GMSMarker()
                                    marker.map = nil
                                    let polyLine = GMSPolyline()
                                    polyLine.map = nil
                                }
                               
                                self.txtPickUpEN.text = item
                                self.getLocation(Location: item)
                                self.txtDropEN.becomeFirstResponder()
                                
                            }
                            else
                            {
                                if self.txtDropAR.text == ""
                                {
                                    self.ViewOfGoogleMap.clear()
                                    let marker = GMSMarker()
                                    marker.map = nil
                                    let polyLine = GMSPolyline()
                                    polyLine.map = nil
                                }
                               
                                self.txtPickUpAR.text = item
                                self.getLocation(Location: item)
                                self.txtDropAR.becomeFirstResponder()
                            }
                            self.LocationDropDown.hide()
                         }
                    }
                    else
                    {
                        self.LocationDropDown.selectionAction = { (index, item) in
                            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                            {
                                self.txtDropEN.text = item
                                self.getLocation(Location: item)
                                self.view.endEditing(true)
                            }
                            else
                            {
                                self.txtDropAR.text = item
                                self.getLocation(Location: item)
                                self.view.endEditing(true)
                            }
                            self.LocationDropDown.hide()
                        }
                    }
                }
                else
                {
                    self.LocationDropDown.hide()
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            self.LocationDropDown.hide()
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getLocation(Location:String)
    {
        let trimmedString = Location.trimmingCharacters(in: .whitespacesAndNewlines)
        let FinalAddress = trimmedString.replacingOccurrences(of: " ", with: "")
        let urlPath = "http://maps.google.com/maps/api/geocode/json?"
        let url = urlPath + "address=\(FinalAddress)&sensor=false"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
               
                if let json = respones.result.value
                {
                    print(json)
                    let arrLocation = json["results"].arrayValue
                    if  arrLocation.count == 0
                    {
                        return
                    }
                    let outerDict = arrLocation[0].dictionaryValue
                    let innerGeometry = outerDict["geometry"]?.dictionaryValue
                    let dictLocation = innerGeometry?["location"]?.dictionaryValue
                    
                    let latitude = dictLocation?["lat"]?.doubleValue
                    let longitude = dictLocation?["lng"]?.doubleValue
                    
                    if self.activeTextFieldTag == 1
                    {
                        self.origin.latitude = latitude!
                        self.origin.longitude = longitude!
                        self.setUpPinOnMap(lat: latitude!, long: longitude!,type: self.activeTextFieldTag)
                    }
                    else
                    {
                        self.destination.latitude = latitude!
                        self.destination.longitude = longitude!
                        self.setUpPinOnMap(lat: latitude!, long: longitude!,type: self.activeTextFieldTag)
                    }
                    
                    if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                    {
                        if self.txtPickUpEN.text != "" &&  self.txtDropEN.text != ""
                        {
                            self.dictLocationInfo = NSMutableDictionary()
                            self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                            self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                            self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                            self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                            self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                            self.dictLocationInfo.setValue(self.txtPickUpEN.text!, forKey: "source_address")
                            self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                            self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                            self.dictLocationInfo.setValue(self.txtDropEN.text, forKey: "desti_address")
                            self.userViewBookRide()
                            self.drawPath()
                        }
                    }
                    else
                    {
                        if self.txtPickUpAR.text != "" &&  self.txtDropAR.text != ""
                        {
                            self.dictLocationInfo = NSMutableDictionary()
                            self.dictLocationInfo.setValue(String(self.currentLocation.latitude), forKey: "book_lat")
                            self.dictLocationInfo.setValue(String(self.currentLocation.longitude), forKey: "book_lng")
                            self.dictLocationInfo.setValue(self.strCurrentAddress, forKey: "book_address")
                            self.dictLocationInfo.setValue(String(self.origin.latitude), forKey: "source_lat")
                            self.dictLocationInfo.setValue(String(self.origin.longitude), forKey: "source_lng")
                            self.dictLocationInfo.setValue(self.txtPickUpAR.text!, forKey: "source_address")
                            self.dictLocationInfo.setValue(String(self.destination.latitude), forKey: "desti_lat")
                            self.dictLocationInfo.setValue(String(self.destination.longitude), forKey: "desti_lng")
                            self.dictLocationInfo.setValue(self.txtDropAR.text, forKey: "desti_address")
                            self.userViewBookRide()
                            self.drawPath()
                        }
                    }
                 }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: 1)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: 1)
        }
    }
    
    func drawPath()
    {
        self.ViewOfGoogleMap.clear()
        let marker = GMSMarker()
        marker.map = nil
        let polyLine = GMSPolyline()
        polyLine.map = nil
        
        var origin = String()
        var destination = String()
        
        origin = "\(self.origin.latitude),\(self.origin.longitude)"
        destination = "\(self.destination.latitude),\(self.destination.longitude)"
        
        self.setUpPinOnMap(lat: self.origin.latitude, long: self.origin.longitude,type: 1)
        self.setUpPinOnMap(lat: self.destination.latitude, long: self.destination.longitude,type: 2)
       
        
       /* if isTrasform == false
        {
            origin = "\(self.origin.latitude),\(self.origin.longitude)"
            destination = "\(self.destination.latitude),\(self.destination.longitude)"
        }
        else
        {
            origin = "\(self.origin.latitude),\(self.origin.longitude)"
            destination = "\(self.destination.latitude),\(self.destination.longitude)"
        }*/
       
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(GoogleMapsDrawPathKey)"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url).responseJSON { response in
                
                let json = JSON(data: response.data!)
                let routes = json["routes"].arrayValue
                if routes.count == 0
                {
                    return
                }
                for route in routes
                {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.map = self.ViewOfGoogleMap
                    polyline.strokeColor = MySingleton.sharedManager.themeColor
                    polyline.strokeWidth = 3
                    let bounds = GMSCoordinateBounds(path: path!)
                    self.ViewOfGoogleMap!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100))
                }
            }

        }
        else
        {
             KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
        
    }
    
    //MARK:- Service
    
    func userViewBookRide()
    {
        let kRegiURL = "\(DraiwelnaMainURL)manage_ride_request"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param = ["ride_id" : rideEditID,
                         "is_cancel" :"0",
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "flag_view":"0"]
            
            if let theJSONData = try? JSONSerialization.data(withJSONObject: self.dictLocationInfo,options: [])
            {
                if let theJSONText = String(data: theJSONData,encoding: .ascii)
                {
                    print("JSON string = \(theJSONText)")
                    param["location_info"] = theJSONText
                }
            }
            
            print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        print("json \(json)")
                       
                        self.dictRideData = json["data"]
                        self.lblArrivalTime.text = self.dictRideData["total_time"].stringValue
                        self.lblTotalPrice.text = "\(self.dictRideData["currency"].stringValue ) \(String(format: "%.2f", self.dictRideData["fair_amount"].floatValue))"                      
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    
    func userViewConfirmRide(confirmDate:String,confirmTime:String)
    {
        let kRegiURL = "\(DraiwelnaMainURL)manage_ride_request"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param = ["ride_id" : rideEditID,
                         "is_cancel" :"0",
                         "lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "access_token" : UserDefaults.standard.value(forKey: "AccessToken") as! String,
                         "user_id" : UserDefaults.standard.value(forKey: "UserId") as! String,
                         "flag_view":"1",
                         "comment":strComment,
                         "start_date":confirmDate,
                         "start_time":confirmTime,
                         //"ride_type":strRideType,
                       //  "special_comment":strSpecialComment,
                         "pay_type":"0",
                         "ride_data":dictRideData] as [String : Any]
            
            if let theJSONData = try? JSONSerialization.data(withJSONObject: self.dictLocationInfo,options: [])
            {
                if let theJSONText = String(data: theJSONData,encoding: .ascii)
                {
                    print("JSON string = \(theJSONText)")
                    param["location_info"] = theJSONText
                }
            }
            
            print("param \(param)")
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            Alamofire.request(kRegiURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: basic_username, password: basic_password).responseSwiftyJSON { respones in
                //                dismiss
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    if json["flag"].stringValue == "1"
                    {
                        print("json \(json)")
                        let obj:HistoryViewController = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
                        let navigationController = NavigationController(rootViewController: obj)
                        navigationController.isNavigationBarHidden = true
                        let mainViewController = MainViewController()
                        mainViewController.rootViewController = navigationController
                        mainViewController.setup(type: 6)
                        
                        let window = UIApplication.shared.delegate!.window!!
                        window.rootViewController = mainViewController

                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                    else
                    {
                        KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                    }
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }


}
