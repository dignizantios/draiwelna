//
//  SelectLocationViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 03/08/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import DropDown
import KSToastView

class SelectLocationViewController: UIViewController,UITextFieldDelegate,GMSMapViewDelegate
{
    //MARK:- Variable Declaration
    
    var locationManager: Location!
    var didFindLocation = Bool()
    var activeTextField = Int()
    var strLocation = String()
    var arrOfSuggestionLocation: [String] = []
    var LocationDropDown = DropDown()
    var RideDetail : RideDetailViewController?
    var selectedLocation = CLLocationCoordinate2D()
    var isComeFromEdit = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var btnBackAR: UIButton!
    @IBOutlet var btnBackEN: UIButton!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet var viewOfGoogleMap: GMSMapView!
    @IBOutlet var imgOfPin: UIImageView!
    
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.txtLocation.text = strLocation
        
        
        if activeTextField == 1
        {
            self.imgOfPin.image = UIImage.init(named: "pin_pickup_location.png")
            self.lblScreenTitle.text = mapping.string(forKey: "Pickup")
        }
        else
        {
            self.imgOfPin.image = UIImage.init(named: "pin_drop_location.png")
            self.lblScreenTitle.text = mapping.string(forKey: "Drop")
        }
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.btnBackEN.setImage(UIImage.init(named: "ic_back_arrow_white.png"), for: .normal)
            self.btnBackAR.setImage(UIImage.init(named: "ic_done_white_header.png"), for: .normal)
        }
        else
        {
            self.btnBackEN.setImage(UIImage.init(named: "ic_done_white_header.png"), for: .normal)
            self.btnBackAR.setImage(UIImage.init(named: "ic_back_arrow_white_ar.png"), for: .normal)
        }
       
        if self.txtLocation.text == ""
        {
            let camera = GMSCameraPosition.camera(withLatitude: (self.RideDetail?.origin.latitude)!,
                                                  longitude:(self.RideDetail?.origin.longitude)!, zoom: 20)
            self.viewOfGoogleMap.animate(to: camera)
        //    self.txtLocation.text = reverseGeoCoding(Latitude: (self.RideDetail?.origin.latitude)!, Longitude:(self.RideDetail?.origin.longitude)!)
            print("Defalult latitude \((self.RideDetail?.origin.latitude)!)")
            print("Defalult longitude \((self.RideDetail?.origin.longitude)!)")
        }
        else
        {
            if activeTextField == 1
            {
                let camera = GMSCameraPosition.camera(withLatitude: (self.RideDetail?.origin.latitude)!,
                                                      longitude:(self.RideDetail?.origin.longitude)!, zoom: 20)
                self.viewOfGoogleMap.animate(to: camera)
            }
            else
            {
                let camera = GMSCameraPosition.camera(withLatitude: (self.RideDetail?.destination.latitude)!,
                                                      longitude:(self.RideDetail?.destination.longitude)!, zoom: 20)
                self.viewOfGoogleMap.animate(to: camera)
            }
            //getLocation(Location: strLocation)
        }
        
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        txtLocation.leftView = paddingView
        txtLocation.leftViewMode = .always
        txtLocation.layer.borderColor = UIColor.clear.cgColor
        txtLocation.layer.borderWidth = 1
        
        self.txtLocation.addTarget(self, action: #selector(RideDetailViewController.didChangeText(textField:)), for: .editingChanged)
        
        

        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Private Method
    
    func openSetting()
    {
        let alertController = UIAlertController (title: AppName, message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
  /*  func setupUserCurrentLocation()
    {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        didFindLocation = true
    }*/
    
    //MARK:- Google Maps Delegate
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        
        print("Defalult latitude \(position.target.latitude)")
        print("Defalult longitude \(position.target.longitude)")
        
        selectedLocation.latitude  = position.target.latitude
        selectedLocation.longitude  = position.target.longitude
        let str = reverseGeoCoding(Latitude: position.target.latitude, Longitude: position.target.longitude)
        self.view.endEditing(true)
        
    }
    
    //MARK:- TextField Delgate
    
    func didChangeText(textField:UITextField)
    {
        getAddressComplete(Location:textField.text!)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        getLocationWithAutoComplete(Location: textField.text!)
        return textField.resignFirstResponder()
    }
    //MARK:- CLLocationManager Delegate
    
    
    //MARK:- Reverse gecode and get local address from Latitude and longitude
    
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                
                
                let lines = address.lines! as [String]
                
                currentAddress = lines.joined(separator: ",")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
                self.txtLocation.text = currentAddress
                self.getLocation(Location: currentAddress)
                print("Address - ",currentAddress)
                
            }
        }
        return currentAddress
    }
    
    //MARK:- DropDown Configure
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.textColor = UIColor.black
        dropdown.selectionBackgroundColor = UIColor.clear
    }
    
    //MARK:- Service
    
    func getAddressComplete(Location:String)
    {
        let trimmedString = Location.trimmingCharacters(in: .whitespacesAndNewlines)
        let FinalAddress = trimmedString.replacingOccurrences(of: " ", with: "")
        let urlPath = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
        var url = String()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
             url = urlPath + "input=\(FinalAddress)&types=geocode&sensor=false&key=AIzaSyCczddW-L4lKmZaq-I-lW7O9goKJk5Ni0U&language=en&components=country:kw"
        }
        else
        {
             url = urlPath + "input=\(FinalAddress)&types=geocode&sensor=false&key=AIzaSyCczddW-L4lKmZaq-I-lW7O9goKJk5Ni0U&language=ar&components=country:kw"
        }
       
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                if let json = respones.result.value
                {
                    print(json)
                    self.arrOfSuggestionLocation = [String]()
                    let arrLocation = json["predictions"].arrayValue
                    if arrLocation.count == 0
                    {
                        return
                    }
                    for i in 0 ..< arrLocation.count
                    {
                        let strLocation = (arrLocation[i].dictionaryValue)["description"]?.stringValue
                        self.arrOfSuggestionLocation.append(strLocation!)
                    }
                    
                    self.configDD(dropdown: self.LocationDropDown, sender: self.txtLocation)
                    
                    
                    self.LocationDropDown.dataSource = self.arrOfSuggestionLocation
                    self.LocationDropDown.show()
                    self.LocationDropDown.selectionAction = { (index, item) in
                            
                            self.txtLocation.text = item
                            self.getLocationWithAutoComplete(Location: item)
                            self.LocationDropDown.hide()
                    }

                }
                else
                {
                    self.LocationDropDown.hide()
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            self.LocationDropDown.hide()
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func getLocation(Location:String)
    {
        let trimmedString = Location.trimmingCharacters(in: .whitespacesAndNewlines)
        let FinalAddress = trimmedString.replacingOccurrences(of: " ", with: "")
        let urlPath = "http://maps.google.com/maps/api/geocode/json?"
        let url = urlPath + "address=\(FinalAddress)&sensor=false"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                
                if let json = respones.result.value
                {
                    print(json)
                    let arrLocation = json["results"].arrayValue
                    if  arrLocation.count == 0
                    {
                        return
                    }
                    let outerDict = arrLocation[0].dictionaryValue
                    let innerGeometry = outerDict["geometry"]?.dictionaryValue
                    let dictLocation = innerGeometry?["location"]?.dictionaryValue
                    
                    let latitude = dictLocation?["lat"]?.doubleValue
                    let longitude = dictLocation?["lng"]?.doubleValue
                    
                   // self.selectedLocation.latitude = latitude!
                  //  self.selectedLocation.longitude = longitude!
                    
                  //  print("selectedLocation \(self.selectedLocation)")
                    
                   // let camera = GMSCameraPosition.camera(withLatitude: latitude!, longitude: longitude!, zoom: 20)
                   // self.viewOfGoogleMap.animate(to: camera)
                    self.LocationDropDown.hide()
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

    func getLocationWithAutoComplete(Location:String)
    {
        let trimmedString = Location.trimmingCharacters(in: .whitespacesAndNewlines)
        let FinalAddress = trimmedString.replacingOccurrences(of: " ", with: "")
        let urlPath = "http://maps.google.com/maps/api/geocode/json?"
        let url = urlPath + "address=\(FinalAddress)&sensor=false"
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: nil).responseSwiftyJSON { respones in
                
                if let json = respones.result.value
                {
                    print(json)
                    let arrLocation = json["results"].arrayValue
                    if  arrLocation.count == 0
                    {
                        return
                    }
                    let outerDict = arrLocation[0].dictionaryValue
                    let innerGeometry = outerDict["geometry"]?.dictionaryValue
                    let dictLocation = innerGeometry?["location"]?.dictionaryValue
                    
                    let latitude = dictLocation?["lat"]?.doubleValue
                    let longitude = dictLocation?["lng"]?.doubleValue
                    
                    self.selectedLocation.latitude = latitude!
                    self.selectedLocation.longitude = longitude!
                    
                    let camera = GMSCameraPosition.camera(withLatitude: latitude!, longitude: longitude!, zoom: 20)
                    self.viewOfGoogleMap.animate(to: camera)
                    self.LocationDropDown.hide()
                }
                else
                {
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            }
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }

    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
             self.navigationController?.popViewController(animated: true)
        }
        else
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    self.RideDetail?.origin.latitude = selectedLocation.latitude
                    self.RideDetail?.origin.longitude = selectedLocation.longitude
                    self.RideDetail?.txtPickUpEN.text = self.txtLocation.text
                }
                else
                {
                    self.RideDetail?.origin.latitude = selectedLocation.latitude
                    self.RideDetail?.origin.longitude = selectedLocation.longitude
                    self.RideDetail?.txtPickUpAR.text = self.txtLocation.text
                }
                
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    self.RideDetail?.destination.latitude = selectedLocation.latitude
                    self.RideDetail?.destination.longitude = selectedLocation.longitude
                    self.RideDetail?.txtDropEN.text = self.txtLocation.text
                }
                else
                {
                    self.RideDetail?.destination.latitude = selectedLocation.latitude
                    self.RideDetail?.destination.longitude = selectedLocation.longitude
                    self.RideDetail?.txtDropAR.text = self.txtLocation.text
                }
            }
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                if  self.RideDetail?.txtPickUpEN.text != "" &&  self.RideDetail?.txtDropEN.text != ""
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
                else if self.RideDetail?.txtPickUpEN.text != ""
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
            }
            else
            {
                if  self.RideDetail?.txtPickUpAR.text != "" &&  self.RideDetail?.txtDropAR.text != ""
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
                else if self.RideDetail?.txtPickUpAR.text != ""
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
            }
            self.navigationController?.popViewController(animated: true)
        }

       
    }
    
    @IBAction func btnDoneAction(_ sender: Any)
    {
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if activeTextField == 1
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    self.RideDetail?.origin.latitude = selectedLocation.latitude
                    self.RideDetail?.origin.longitude = selectedLocation.longitude
                    self.RideDetail?.txtPickUpEN.text = self.txtLocation.text
                }
                else
                {
                    self.RideDetail?.origin.latitude = selectedLocation.latitude
                    self.RideDetail?.origin.longitude = selectedLocation.longitude
                    self.RideDetail?.txtPickUpAR.text = self.txtLocation.text
                }                
            }
            else
            {
                if UserDefaults.standard.value(forKey: "lang") as? Int == 0
                {
                    self.RideDetail?.destination.latitude = selectedLocation.latitude
                    self.RideDetail?.destination.longitude = selectedLocation.longitude
                    self.RideDetail?.txtDropEN.text = self.txtLocation.text
                }
                else
                {
                    self.RideDetail?.destination.latitude = selectedLocation.latitude
                    self.RideDetail?.destination.longitude = selectedLocation.longitude
                    self.RideDetail?.txtDropAR.text = self.txtLocation.text
                }
            }
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                if  self.RideDetail?.txtPickUpEN.text != "" &&  self.RideDetail?.txtDropEN.text != ""
                {
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
                else if self.RideDetail?.txtPickUpEN.text != ""
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
            }
            else
            {
                if  self.RideDetail?.txtPickUpAR.text != "" &&  self.RideDetail?.txtDropAR.text != ""
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
                else if self.RideDetail?.txtPickUpAR.text != ""
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getEstimateAmount"), object: nil)
                }
            }
            
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
           self.navigationController?.popViewController(animated: true)
        }
        
    }

}
