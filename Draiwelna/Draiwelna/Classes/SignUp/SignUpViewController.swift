//
//  SignUpViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 20/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
//import DropDown
import KSToastView
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import CoreLocation

class SignUpViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate,CLLocationManagerDelegate,UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK:- Variable Zone
    
   // var genderDropDown = DropDown()
    var picker:UIImagePickerController?=UIImagePickerController()
    var strImage = String()
    var locationManager = CLLocationManager()
    var didFindLocation = Bool()
    var strCountryCode = String()
    var arrGender = NSMutableArray()
    var activeTextfield = Int()
    
    //MARK:- Outlet Zone
    
    //English Outlet
    
    @IBOutlet var btnRegisterOutletEN: UIButton!
    @IBOutlet var btnBackOutletEN: UIButton!
    @IBOutlet var btnReadTermOutletEN: UIButton!
    @IBOutlet var btnCheckedTermOutletEN: UIButton!
    @IBOutlet var txtUserConfirmPasswordEN: UITextField!
    @IBOutlet var txtUserPasswordEN: UITextField!
    @IBOutlet var txtUserMobileNumberEN: UITextField!
    @IBOutlet var txtUserGenderEN: UITextField!
    @IBOutlet var txtUserBODEN: UITextField!
    @IBOutlet var txtUserEmailEN: UITextField!
    @IBOutlet var txtUsernameEN: UITextField!
    @IBOutlet var txtLastNameEN: UITextField!
    @IBOutlet var txtFirstNameEN: UITextField!
    @IBOutlet var imgOfUserProfileEN: UIImageView!
    @IBOutlet var btnCancelOutletEN: UIButton!
    @IBOutlet var btnDoneOutletEN: UIButton!
    
    //Arabic Outlet
    
    @IBOutlet var btnRegisterOutletAR: UIButton!
    @IBOutlet var btnBackOutletAR: UIButton!
    @IBOutlet var btnReadTermOutletAR: UIButton!
    @IBOutlet var btnCheckedTermOutletAR: UIButton!
    @IBOutlet var txtUserConfirmPasswordAR: UITextField!
    @IBOutlet var txtUserPasswordAR: UITextField!
    @IBOutlet var txtUserMobileNumberAR: UITextField!
    @IBOutlet var txtUserGenderAR: UITextField!
    @IBOutlet var txtUserBODAR: UITextField!
    @IBOutlet var txtUserEmailAR: UITextField!
    @IBOutlet var txtUsernameAR: UITextField!
    @IBOutlet var txtLastNameAR: UITextField!
    @IBOutlet var txtFirstNameAR: UITextField!
    @IBOutlet var imgOfUserProfileAR: UIImageView!
    
    //CommonOutlet
    
    @IBOutlet var viewOfArabic: UIView!
    @IBOutlet var viewOfEnglish: UIView!
    @IBOutlet var topOfDatePicker: NSLayoutConstraint!
    @IBOutlet var viewOfDatePicker: UIDatePicker!
    @IBOutlet var viewOfNormalPicker: UIPickerView!
    
    //MARK:- ViewLifeCycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        HideGestture()
        
        KSToastView.ks_setAppearanceBackgroundColor(UIColor.white)
        KSToastView.ks_setAppearanceTextColor(UIColor.black)
       // KSToastView.ks_setAppearanceOffsetBottom(UIScreen.main.bounds.height - 150)
        
        self.topOfDatePicker.constant = UIScreen.main.bounds.height
        strImage = "0"
        
        self.btnReadTermOutletEN.setAttributedTitle(attributedString(str1:mapping.string(forKey: "Terms & Conditions"),str2:mapping.string(forKey: "Accept ")), for: .normal)
        self.btnBackOutletEN.setAttributedTitle(attributedString(str1:mapping.string(forKey: "Sign In"),str2:mapping.string(forKey: "Already registered? ")), for: .normal)
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            self.viewOfEnglish.isHidden = false
            self.viewOfArabic.isHidden = true
            
            self.btnRegisterOutletEN.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnRegisterOutletEN.layer.cornerRadius = self.btnRegisterOutletEN.bounds.size.height/2
            self.btnRegisterOutletEN.clipsToBounds = true
            [txtFirstNameEN,txtLastNameEN,txtUsernameEN,txtUserEmailEN,txtUserBODEN,txtUserGenderEN,txtUserMobileNumberEN,txtUserPasswordEN,txtUserConfirmPasswordEN].forEach({ customizeTextFieldEN(textfield: $0) })
            arrGender = NSMutableArray()
            arrGender.add("Male")
            arrGender.add("Female")
           // configDD(dropdown: genderDropDown, sender: txtUserGenderEN)
            //genderDropDown.dataSource = ["Male","Female"]
            
            self.imgOfUserProfileEN.layer.cornerRadius = self.imgOfUserProfileEN.bounds.size.height/2
            self.imgOfUserProfileEN.layer.borderWidth = 2
            self.imgOfUserProfileEN.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            imgOfUserProfileEN.isUserInteractionEnabled = true
            imgOfUserProfileEN.addGestureRecognizer(tapGestureRecognizer)
            
            
            self.btnDoneOutletEN.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnCancelOutletEN.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
        }
        else
        {
            self.viewOfEnglish.isHidden = true
            self.viewOfArabic.isHidden = false
            
            self.btnRegisterOutletAR.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnRegisterOutletAR.layer.cornerRadius = self.btnRegisterOutletAR.bounds.size.height/2
            self.btnRegisterOutletAR.clipsToBounds = true
            
            [txtFirstNameAR,txtLastNameAR,txtUsernameAR,txtUserEmailAR,txtUserBODAR,txtUserGenderAR,txtUserMobileNumberAR,txtUserPasswordAR,txtUserConfirmPasswordAR].forEach({ customizeTextFieldAR(textfield: $0) })
            arrGender = NSMutableArray()
            arrGender.add("ذكر")
            arrGender.add("أنثى")
           // configDD(dropdown: genderDropDown, sender: txtUserGenderAR)
           // genderDropDown.dataSource = ["الذكر","إناثا"]
            
            self.imgOfUserProfileAR.layer.cornerRadius = self.imgOfUserProfileAR.bounds.size.width/2
            self.imgOfUserProfileAR.layer.borderWidth = 2
            self.imgOfUserProfileAR.layer.borderColor = MySingleton.sharedManager.themeColor.cgColor
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            imgOfUserProfileAR.isUserInteractionEnabled = true
            imgOfUserProfileAR.addGestureRecognizer(tapGestureRecognizer)
            
           // self.btnReadTermOutletAR.setAttributedTitle(attributedString(str1:mapping.string(forKey: "Terms & Conditions"),str2:mapping.string(forKey: "Accept ")), for: .normal)
         //   self.btnBackOutletAR.setAttributedTitle(attributedString(str1:mapping.string(forKey: "Sign In"),str2:mapping.string(forKey: "Already registered? ")), for: .normal)
            
            self.btnCancelOutletEN.setTitle(mapping.string(forKey: "Done"), for: .normal)
            self.btnDoneOutletEN.setTitle(mapping.string(forKey: "Cancel"), for: .normal)
        }
        
        locationManager.delegate = self
      
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - StatusBar
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return UIStatusBarStyle.lightContent
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Textfield Placeholder & Delegate Method
    
    func customizeTextFieldEN(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.backgroundColor = UIColor.clear
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func customizeTextFieldAR(textfield: UITextField)
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 10))
        textfield.leftView = paddingView
        textfield.leftViewMode = .always
        let rightPaddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        textfield.rightView = rightPaddingView
        textfield.rightViewMode = .always
        textfield.backgroundColor = UIColor.clear
        textfield.layer.cornerRadius = textfield.bounds.size.height/2
        textfield.layer.borderColor = UIColor.white.cgColor
        textfield.layer.borderWidth = 1
        textfield.setValue(UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5), forKeyPath: "_placeholderLabel.textColor")
        textfield.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
       // genderDropDown.hide()
        activeTextfield = Int()
        if textField == self.txtUserBODEN || textField == self.txtUserBODAR
        {
            activeTextfield = 1
            self.view.endEditing(true)
            self.viewOfNormalPicker.isHidden = true
            self.viewOfDatePicker.isHidden = false
            self.movePickerUp()
            self.viewOfDatePicker.maximumDate = Date()
            self.viewOfDatePicker.datePickerMode = .date
            return false
        }
        else if textField == self.txtUserGenderEN || textField == self.txtUserGenderAR
        {
            activeTextfield = 2
            self.view.endEditing(true)
            self.viewOfNormalPicker.isHidden = false
            self.viewOfDatePicker.isHidden = true
            self.movePickerUp()
           // genderDropDown.show()
          //  genderDropDown.selectionAction = { (index, item) in
              //  self.txtUserGenderEN.text = item
              //  self.selectedCategory = self.categoryItems[index].id
          //  }
            return false
        }

        self.movePickerDown()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.movePickerDown()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if textField == self.txtFirstNameEN
            {
                self.txtLastNameEN.becomeFirstResponder()
            }
            else if textField == self.txtLastNameEN
            {
                self.txtUsernameEN.becomeFirstResponder()
            }
            else if textField == self.txtUsernameEN
            {
                self.txtUserEmailEN.becomeFirstResponder()
            }
            else if textField == self.txtUserEmailEN
            {
                self.view.endEditing(true)
                self.movePickerUp()
                self.txtUserBODEN.resignFirstResponder()
            }
            else if textField == self.txtUserGenderEN
            {
                self.view.endEditing(true)
                self.movePickerDown()
                self.txtUserGenderEN.resignFirstResponder()
            }
            else if textField == self.txtUserMobileNumberEN
            {
                self.txtUserPasswordEN.becomeFirstResponder()
            }
            else if textField == self.txtUserPasswordEN
            {
                self.txtUserConfirmPasswordEN.becomeFirstResponder()
            }
            else
            {
                textField.resignFirstResponder()
            }

        }
        else
        {
            if textField == self.txtFirstNameAR
            {
                self.txtLastNameAR.becomeFirstResponder()
            }
            else if textField == self.txtLastNameAR
            {
                self.txtUsernameAR.becomeFirstResponder()
            }
            else if textField == self.txtUsernameAR
            {
                self.txtUserEmailAR.becomeFirstResponder()
            }
            else if textField == self.txtUserEmailAR
            {
                self.view.endEditing(true)
                self.movePickerUp()
                self.txtUserBODAR.resignFirstResponder()
            }
            else if textField == self.txtUserGenderAR
            {
                self.view.endEditing(true)
                self.movePickerDown()
                self.txtUserGenderAR.resignFirstResponder()
            }
            else if textField == self.txtUserMobileNumberAR
            {
                self.txtUserPasswordAR.becomeFirstResponder()
            }
            else if textField == self.txtUserPasswordAR
            {
                self.txtUserConfirmPasswordAR.becomeFirstResponder()
            }
            else
            {
                textField.resignFirstResponder()
            }

        }
        self.movePickerDown()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtUserMobileNumberEN ||  textField == txtUserMobileNumberAR
        {
            let limitLength = 8
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= limitLength // Bool
        }
        return true
    }

    
    //MARK: - View UP and Down Method
    
    func movePickerUp()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.topOfDatePicker.constant = UIScreen.main.bounds.height - 180
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
        
        
    }
    
    func movePickerDown()
    {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3)
        {
            self.topOfDatePicker.constant = UIScreen.main.bounds.height
            self.view.layoutIfNeeded()
        }
        self.view.layoutIfNeeded()
        UIView.commitAnimations()
    }
    

    //MARK:- DropDown Configure
    
   /* func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.clipsToBounds = true
        dropdown.layer.cornerRadius = 5
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
       // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.black
        dropdown.textColor = UIColor.white
        dropdown.selectionBackgroundColor = UIColor.clear
    }*/

    //MARK:- PickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arrGender.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arrGender.object(at: row) as? String
    }
    
   
    //MARK: - ImagePicker Delegate Methods
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        strImage = "0"
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        imgOfUserProfileEN.contentMode = .scaleAspectFit
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            imgOfUserProfileEN.image = chosenImage
        }
        else
        {
            imgOfUserProfileAR.image = chosenImage
        }
        
        strImage = "1"
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Select Picture Methods
    
    func openGallary()
    {
        picker?.delegate = self
        picker!.allowsEditing = true
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker!, animated: true, completion: nil)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                picker?.delegate = self
                picker!.allowsEditing = true
                picker!.sourceType = UIImagePickerControllerSourceType.camera
                picker!.cameraCaptureMode = .photo
                present(picker!, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: AppName, message: mapping.string(forKey: "This device has no Camera"), preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
            }
    }

     //MARK:- Private Method
    
    private func attributedString(str1:String,str2:String) -> NSAttributedString?
    {
        let attributes1 = [
          
            NSForegroundColorAttributeName : UIColor.white,
            NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue
            ] as [String : Any]
        
        let attributes2 = [
            NSForegroundColorAttributeName : UIColor.white
           ] as [String : Any]
        
        let attributedString1 = NSAttributedString(string: str1, attributes: attributes1)
        let attributedString2 = NSAttributedString(string: str2, attributes: attributes2)
        
        let FormatedString = NSMutableAttributedString()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            FormatedString.append(attributedString2)
            FormatedString.append(attributedString1)
        }
        else
        {
            FormatedString.append(attributedString1)
            FormatedString.append(attributedString2)
        }
        
        return FormatedString
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
        let optionMenu = UIAlertController(title: nil, message: mapping.string(forKey: "Choose option"), preferredStyle: .actionSheet)
        
        // 2
        let galleryAction = UIAlertAction(title:mapping.string(forKey: "Select from library") , style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.openGallary()
        })
        let camaraAction = UIAlertAction(title: mapping.string(forKey: "Take a Photo"), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.openCamera()
        })
        
        //
        let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel"), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        // 4
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(camaraAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    //MARK:- CLLocationManager Delegate
    
    func openSetting()
    {
        let alertController = UIAlertController (title: AppName, message: "Go to Settings?", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func setupUserCurrentLocation()
    {
        didFindLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if didFindLocation == true
        {
            didFindLocation = false
            
            guard let currentLocation = locations.first else { return }
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
                guard let currentLocPlacemark = placemarks?.first else { return }
                print(currentLocPlacemark.country ?? "No country found")
                print(currentLocPlacemark.isoCountryCode ?? "No country code found")
                self.strCountryCode = self.getCountryPhonceCode(currentLocPlacemark.isoCountryCode!)
            }
            
            
        }
        else
        {
            didFindLocation = false
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        didFindLocation = false
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            setupUserCurrentLocation()
            // manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            openSetting()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            openSetting()
            break
            //   default:
            // break
        }
    }
    

    

    //MARK:- Action Zone
    
    @IBAction func btnCheckedTermAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.movePickerDown()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            if self.btnCheckedTermOutletEN.isSelected == true
            {
                self.btnCheckedTermOutletEN.isSelected = false
            }
            else
            {
                self.btnCheckedTermOutletEN.isSelected = true
            }
        }
        else
        {
            if self.btnCheckedTermOutletAR.isSelected == true
            {
                self.btnCheckedTermOutletAR.isSelected = false
            }
            else
            {
                self.btnCheckedTermOutletAR.isSelected = true
            }
        }
        
    }
    
    @IBAction func btnReadTermAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.movePickerDown()
    }

    @IBAction func btnRegisterAction(_ sender: Any)
    {
        //self.view.endEditing(true)
        self.movePickerDown()
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            let trimmedUserName = self.txtUsernameEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUsernameEN.text = trimmedUserName
            
            let trimmedUserFirstName = self.txtFirstNameEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtFirstNameEN.text = trimmedUserFirstName
            
            let trimmedUserLastName = self.txtLastNameEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtLastNameEN.text = trimmedUserLastName
            
            let trimmedUserEmail = self.txtUserEmailEN.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserEmailEN.text = trimmedUserEmail
            
            if self.txtFirstNameEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your first name"), duration: ToastDuration)
            }
            else if self.txtLastNameEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your last name"), duration: ToastDuration)
            }
           /* else if strImage == "0"
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select a profile picture"), duration: ToastDuration)
            }*/
            else if self.txtUsernameEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserEmailEN.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter email address"), duration: ToastDuration)
            }
            else
            {
                let emailid: String = self.txtUserEmailEN.text!
                let myStringMatchesRegEx: Bool = self.isValidEmail(emailAddressString: emailid)
                if myStringMatchesRegEx == false
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter valid email address"), duration: ToastDuration)
                }
              /*  else if self.txtUserBODEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter birthdate"), duration: ToastDuration)
                }
                else if self.txtUserGenderEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select gender"), duration: ToastDuration)
                }*/
                else if self.txtUserMobileNumberEN.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter your mobile number"), duration: ToastDuration)
                }
                else if (self.txtUserMobileNumberEN.text?.characters.count)! < 8
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter a valid 8-digit mobile number"), duration: ToastDuration)
                }
                else if (self.txtUserPasswordEN.text == "")
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter your password"), duration: ToastDuration)
                }
                else if !(self.txtUserPasswordEN.text == "")
                {
                    if (self.txtUserPasswordEN.text?.characters.count)! < 6
                    {
                        KSToastView.ks_showToast(mapping.string(forKey: "Password must be at least 6 characters long."), duration: ToastDuration)
                    }
                    else if self.txtUserConfirmPasswordEN.text == ""
                    {
                        KSToastView.ks_showToast(mapping.string(forKey: "Please confirm your password"), duration: ToastDuration)
                    }
                    else
                    {
                        if(!(self.txtUserPasswordEN.text == self.txtUserConfirmPasswordEN.text))
                        {
                            KSToastView.ks_showToast(mapping.string(forKey: "Password and confirmation password must match."), duration: ToastDuration)
                        }
                        else if self.btnCheckedTermOutletEN.isSelected == false
                        {
                            KSToastView.ks_showToast(mapping.string(forKey: "You must agree our terms & conditions"), duration: ToastDuration)
                        }
                        else
                        {
                            self.view.endEditing(true)
                            userRegistrationForEnglish()
                        }
                    }
                }
                
            }

        }
        else
        {
            let trimmedUserName = self.txtUsernameAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUsernameAR.text = trimmedUserName
            
            let trimmedUserFirstName = self.txtFirstNameAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtFirstNameAR.text = trimmedUserFirstName
            
            let trimmedUserLastName = self.txtLastNameAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtLastNameAR.text = trimmedUserLastName
            
            let trimmedUserEmail = self.txtUserEmailAR.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.txtUserEmailAR.text = trimmedUserEmail
            
            if self.txtFirstNameAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your first name"), duration: ToastDuration)
            }
            else if self.txtLastNameAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter your last name"), duration: ToastDuration)
            }
           /* else if strImage == "0"
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please select a profile picture"), duration: ToastDuration)
            }*/
            else if self.txtUsernameAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter a username."), duration: ToastDuration)
            }
            else if self.txtUserEmailAR.text == ""
            {
                KSToastView.ks_showToast(mapping.string(forKey: "Please enter email address"), duration: ToastDuration)
            }
            else
            {
                let emailid: String = self.txtUserEmailAR.text!
                let myStringMatchesRegEx: Bool = self.isValidEmail(emailAddressString: emailid)
                if myStringMatchesRegEx == false
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter valid email address"), duration: ToastDuration)
                }
               /* else if self.txtUserBODAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter birthdate"), duration: ToastDuration)
                }
                else if self.txtUserGenderAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please select gender"), duration: ToastDuration)
                }*/
                else if self.txtUserMobileNumberAR.text == ""
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter your mobile number"), duration: ToastDuration)
                }
                else if (self.txtUserMobileNumberAR.text?.characters.count)! < 8
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please enter a valid 8-digit mobile number"), duration: ToastDuration)
                }
                else if (self.txtUserPasswordAR.text == "")
                {
                    KSToastView.ks_showToast(mapping.string(forKey: "Please confirm your password"), duration: ToastDuration)
                }
                else if !(self.txtUserPasswordAR.text == "")
                {
                    if (self.txtUserPasswordAR.text?.characters.count)! < 6
                    {
                        KSToastView.ks_showToast(mapping.string(forKey: "Password must be at least 6 characters long."), duration: ToastDuration)
                    }
                    else if self.txtUserConfirmPasswordAR.text == ""
                    {
                        KSToastView.ks_showToast(mapping.string(forKey: "Please confirm your password"), duration: ToastDuration)
                    }
                    else
                    {
                        if(!(self.txtUserPasswordAR.text == self.txtUserConfirmPasswordEN.text))
                        {
                            KSToastView.ks_showToast(mapping.string(forKey: "Password and confirmation password must match."), duration: ToastDuration)
                        }
                        else if self.btnCheckedTermOutletAR.isSelected == false
                        {
                            KSToastView.ks_showToast(mapping.string(forKey: "You must agree our terms & conditions"), duration: ToastDuration)
                        }
                        else
                        {
                            self.view.endEditing(true)
                            userRegistrationForArabic()
                        }
                    }
                }
            }
            
        }
        
    }
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.movePickerDown()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPickerCancelAction(_ sender: Any)
    {
        if activeTextfield == 1
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                self.movePickerDown()
            }
            else
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd - MM - yyyy"
                self.txtUserBODAR.text = dateFormatter.string(from: self.viewOfDatePicker.date)
                self.movePickerDown()
            }
        }
        else
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                self.movePickerDown()
            }
            else
            {
                self.txtUserGenderAR.text = self.arrGender.object(at: self.viewOfNormalPicker.selectedRow(inComponent: 0)) as? String
                self.movePickerDown()
                
            }
        }
        
        
    }
    @IBAction func btnDonePickerAction(_ sender: Any)
    {
        if activeTextfield == 1
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd - MM - yyyy"
                self.txtUserBODEN.text = dateFormatter.string(from: self.viewOfDatePicker.date)
                self.movePickerDown()
            }
            else
            {
                self.movePickerDown()
            }
        }
        else
        {
            if UserDefaults.standard.value(forKey: "lang") as? Int == 0
            {
                self.txtUserGenderEN.text = self.arrGender.object(at: self.viewOfNormalPicker.selectedRow(inComponent: 0)) as? String
                self.movePickerDown()
            }
            else
            {
                self.movePickerDown()
            }
        }
        
    }
    
    //MARK:- Service
    
    func userRegistrationForEnglish()
    {
        let kRegiURL = "\(DraiwelnaMainURL)register"
        var strDOB = String()
        if self.txtUserBODEN.text == ""
        {
            strDOB = ""
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd - MM - yyyy"
            let date = (dateFormatter.date(from: self.txtUserBODEN.text!)!)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            strDOB = dateFormatter.string(from: date)
        }
        var strGender = String()
        if self.txtUserGenderEN.text == ""
        {
            strGender = ""
        }
        else
        {
            strGender = ((self.txtUserGenderEN.text)?.lowercased())!
        }
        
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param:[String:String] = ["lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                         "timezone": TimeZone.current.identifier,
                         "first_name" : self.txtFirstNameEN.text!,
                         "last_name" : self.txtLastNameEN.text!,
                         "username" : self.txtUsernameEN.text!,
                         "email" : self.txtUserEmailEN.text!,
                         "password" : self.txtUserPasswordEN.text!,
                         "mobile_number" : self.txtUserMobileNumberEN.text!,
                         "country_code":strCountryCode,
                         "gender":strGender,
                         "birth_date" : strDOB,
                         "device_token":UserDefaults.standard.value(forKey: "deviceToken") as! String,
                         "register_id":"",
                         "device_type":DeviceType]
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            let PasswordString =  String(format: "\(basic_username):\(basic_password)")
            let PasswordData = PasswordString.data(using: .utf8)
            let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
            
            let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
            print("headers:==\(headers)")
            
            let unit64:UInt64 = 10_000_000
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if let img = self.imgOfUserProfileEN.image {
                    let imgData = UIImageJPEGRepresentation(img, 0.7)
                    multipartFormData.append(imgData!, withName: "profile_image", fileName:"image" , mimeType: "image/png")
                }
                
                for (key, value) in param
                {
                    print("\(key) \(value)")
                     multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }, usingThreshold: unit64, to: kRegiURL, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
                print("encoding result:\(encodingResult)")
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //send progress using delegate
                    })
                    upload.responseSwiftyJSON(completionHandler: { (responce) in
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        print("response:==>\(responce)")
                        if let json = responce.result.value
                        {
                            print("json\(json)")
                            if json["flag"].stringValue == "1"
                            {
                                CurrentScreen = 1
                                let dictData = json["data"]
                                let defaults = UserDefaults.standard
                                defaults.set(json["access_token"].stringValue, forKey: "AccessToken")
                                defaults.set(dictData["id"].stringValue, forKey: "UserId")
                                defaults.set(dictData["profile_image"].stringValue, forKey: "UserProfilePicture")
                                defaults.set(dictData["username"].stringValue, forKey: "UserName")
                                defaults.synchronize()
                                
                                UserDefaults.standard.set("0", forKey: "isRideStart")
                                UserDefaults.standard.set("0", forKey: "isDriverLogin")
                                let HomeVC = HomeViewController()
                                let navigationController = NavigationController(rootViewController: HomeVC)
                                let mainViewController = MainViewController()
                                mainViewController.rootViewController = navigationController
                                mainViewController.setup(type: UInt(6))
                                
                                navigationController.isNavigationBarHidden = true
                                let window = UIApplication.shared.delegate!.window!!
                                window.rootViewController = mainViewController
                            }
                            else
                            {
                                KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                            }
                        }
                        else
                        {
                            KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                        }
                        //completion(responce.result)
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            })
           
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
    
    func userRegistrationForArabic()
    {
        let kRegiURL = "\(DraiwelnaMainURL)register"
        var strDOB = String()
        if self.txtUserBODAR.text == ""
        {
            strDOB = ""
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd - MM - yyyy"
            let date = (dateFormatter.date(from: self.txtUserBODAR.text!)!)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            strDOB = dateFormatter.string(from: date)
        }
        var strGender = String()
        if self.txtUserGenderAR.text == ""
        {
            strGender = ""
        }
        else
        {
            strGender = ((self.txtUserGenderAR.text)?.lowercased())!
        }

        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param:[String:String] = ["lang" : String(UserDefaults.standard.value(forKey: "lang") as! Int),
                                         "timezone": TimeZone.current.identifier,
                                         "first_name" : self.txtFirstNameAR.text!,
                                         "last_name" : self.txtLastNameAR.text!,
                                         "username" : self.txtUsernameAR.text!,
                                         "email" : self.txtUserEmailAR.text!,
                                         "password" : self.txtUserPasswordAR.text!,
                                         "mobile_number" : self.txtUserMobileNumberAR.text!,
                                         "country_code":strCountryCode,
                                         "gender":strGender,
                                         "birth_date" : strDOB,
                                         "device_token":UserDefaults.standard.value(forKey: "deviceToken") as! String,
                                         "register_id":"",
                                         "device_type":DeviceType]
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            show
            let PasswordString =  String(format: "\(basic_username):\(basic_password)") // "usr_easyjob:3a8wGTszSJWU2J99"
            let PasswordData = PasswordString.data(using: .utf8)
            let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
            
            let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
            print("headers:==\(headers)")
            
            let unit64:UInt64 = 10_000_000
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if let img = self.imgOfUserProfileAR.image {
                    let imgData = UIImageJPEGRepresentation(img, 0.7)
                    multipartFormData.append(imgData!, withName: "profile_image", fileName:"image" , mimeType: "image/png")
                }
                
                for (key, value) in param
                {
                    print("\(key) \(value)")
                    multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }, usingThreshold: unit64, to: kRegiURL, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
                print("encoding result:\(encodingResult)")
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //send progress using delegate
                    })
                    upload.responseSwiftyJSON(completionHandler: { (responce) in
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        print("response:==>\(responce)")
                        if let json = responce.result.value
                        {
                            print("json\(json)")
                            if json["flag"].stringValue == "1"
                            {
                                print(json)
                                CurrentScreen = 1
                                let dictData = json["data"]
                                let defaults = UserDefaults.standard
                                defaults.set(json["access_token"].stringValue, forKey: "AccessToken")
                                defaults.set(dictData["id"].stringValue, forKey: "UserId")
                                defaults.set(dictData["profile_image"].stringValue, forKey: "UserProfilePicture")
                                defaults.set(dictData["username"].stringValue, forKey: "UserName")
                                defaults.synchronize()
                                
                                UserDefaults.standard.set("0", forKey: "isRideStart")
                                UserDefaults.standard.set("0", forKey: "isDriverLogin")
                                let HomeVC = HomeViewController()
                                let navigationController = NavigationController(rootViewController: HomeVC)
                                let mainViewController = MainViewController()
                                mainViewController.rootViewController = navigationController
                                mainViewController.setup(type: UInt(6))
                                
                                navigationController.isNavigationBarHidden = true
                                let window = UIApplication.shared.delegate!.window!!
                                window.rootViewController = mainViewController
                            }
                            else
                            {
                                KSToastView.ks_showToast(json["msg"].stringValue, duration: ToastDuration)
                            }
                        }
                        else
                        {
                            KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                        }
                        //completion(responce.result)
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    KSToastView.ks_showToast(WrongMsg, duration: ToastDuration)
                }
            })
            
            
        }
        else
        {
            KSToastView.ks_showToast(InternetMessage, duration: ToastDuration)
        }
    }
   
}
