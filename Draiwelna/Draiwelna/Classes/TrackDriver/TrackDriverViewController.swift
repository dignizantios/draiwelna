//
//  TrackDriverViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 25/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit
import GoogleMaps

class TrackDriverViewController: UIViewController,LocationDelegate
{
    //MARK:- Variable Declaration
    
    var isCheckLanguage = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet var btnSideMenuOutlet: UIButton!
    @IBOutlet var btnCurrentLocationOutlet: UIButton!
    @IBOutlet var lblScreenTitle: UILabel!
    
    @IBOutlet var viewOfGoogleMaps: GMSMapView!
    @IBOutlet var imgOfDriverProfile: UIImageView!
    @IBOutlet var lblDriverName: UILabel!

    @IBOutlet var btnMsgDeiverOutlet: UIButton!
    @IBOutlet var btnCallDriverOutlet: UIButton!
    @IBOutlet var btnDriverInfoOutlet: UIButton!
    @IBOutlet var btnCancelRideOutlet: ShadowButton!
    
    //MARK:- ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblScreenTitle.text = mapping.string(forKey: "Track Driver")
        self.btnCancelRideOutlet.setTitle(mapping.string(forKey: "CANCEL RIDE"), for: .normal)
        self.btnDriverInfoOutlet.setTitle(mapping.string(forKey: "DRIVER INFO"), for: .normal)
        
        var locationManager: Location!
        locationManager = Location()
        locationManager.needToDisplayAlert = false
        locationManager.delegate = self
        
        if UserDefaults.standard.value(forKey: "lang") as? Int == 0
        {
            isCheckLanguage = false
            self.btnCurrentLocationOutlet.setImage(UIImage.init(named: "ic_current_location_white_header.png"), for: .normal)
            self.btnSideMenuOutlet.setImage(UIImage.init(named: "ic_menubar_white_header.png"), for: .normal)
            
            self.btnCallDriverOutlet.setImage(UIImage.init(named: "ic_call_driver_track_details.png"), for: .normal)
            self.btnMsgDeiverOutlet.setImage(UIImage.init(named: "ic_message_driver_track_details.png"), for: .normal)
           
        }
        else
        {
            isCheckLanguage = true
            self.btnCurrentLocationOutlet.setImage(UIImage.init(named: "ic_menubar_white_header.png"), for: .normal)
            self.btnSideMenuOutlet.setImage(UIImage.init(named: "ic_current_location_white_header.png"), for: .normal)
            self.btnCallDriverOutlet.setImage(UIImage.init(named: "ic_message_driver_track_details.png"), for: .normal)
            self.btnMsgDeiverOutlet.setImage(UIImage.init(named: "ic_call_driver_track_details.png"), for: .normal)
        }
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Action Zone
   
    @IBAction func btnSideMenuAction(_ sender: Any)
    {
        if isCheckLanguage == true
        {
            //setupUserCurrentLocation()
        }
        else
        {
            sideMenuController?.leftViewController?.showLeftViewAnimated(true)
        }
    }

    @IBAction func btnUserCurrentLocationAction(_ sender: Any)
    {
        if isCheckLanguage == true
        {
            sideMenuController?.rightViewController?.showRightViewAnimated(true)
        }
        else
        {
            //setupUserCurrentLocation()
        }
    }
    
    @IBAction func btnCallDriverAction(_ sender: Any)
    {
        if isCheckLanguage == true
        {
            //arabic
        }
        else
        {
            //english
        }
    }
    @IBAction func btnMsgDriverAction(_ sender: Any)
    {
        if isCheckLanguage == true
        {
            //arabic
        }
        else
        {
            //english
        }
    }
    @IBAction func btnDriverInfoAction(_ sender: Any)
    {
        let obj:DriverInfoViewController = DriverInfoViewController(nibName: "DriverInfoViewController", bundle: nil)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnCancelRideAction(_ sender: Any) {
    }
    
    //MARK:- Update Location
    
    func didUpdateLocation(lat: Double?, lon: Double?)
    {
        if UserDefaults.standard.value(forKey: "isDriverLogin") as? String == "0"
        {
            //user
            if UserDefaults.standard.value(forKey: "isRideStart") as? String == "1"
            {
                
            }
        }
        else
        {
            //driver
            if UserDefaults.standard.value(forKey: "isRideStart") as? String == "1"
            {
                //UpdateCurrentLocation(latitude: lat!, longitude: lon!)
            }
        }
    }
}
