//
//  ViewController.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 20/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    
    //MARK:- Outlet Zone
    
    @IBOutlet var btnEnglishOutlet: UIButton!
    @IBOutlet var btnArabicOutlet: UIButton!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        printFonts()
        HideGestture()

        self.navigationController?.navigationBar.isHidden = true
        if UserDefaults.standard.value(forKey: "UserId") as? String != nil
        {
            if UserDefaults.standard.value(forKey: "isDriverLogin") as? String == "0"
            {
                CurrentScreen = 1
                UserDefaults.standard.set("0", forKey: "isRideStart")
                let HomeVC = HomeViewController()
                let navigationController = NavigationController(rootViewController: HomeVC)
                let mainViewController = MainViewController()
                mainViewController.rootViewController = navigationController
                mainViewController.setup(type: UInt(6))
                
                navigationController.isNavigationBarHidden = true
                let window = UIApplication.shared.delegate!.window!!
                window.rootViewController = mainViewController
            }
            else
            {
                let obj:DriverHomeViewController = DriverHomeViewController(nibName: "DriverHomeViewController", bundle: nil)
                self.navigationController?.pushViewController(obj, animated: false)
            }
           
        }
        else
        {
            self.btnEnglishOutlet.backgroundColor = MySingleton.sharedManager.themeColor
            self.btnEnglishOutlet.layer.cornerRadius = self.btnEnglishOutlet.bounds.size.height/2
            self.btnEnglishOutlet.clipsToBounds = true
            
            self.btnArabicOutlet.layer.cornerRadius = self.btnArabicOutlet.frame.size.height/2
            self.btnArabicOutlet.clipsToBounds = true
            self.btnArabicOutlet.layer.borderColor = UIColor.white.cgColor
            self.btnArabicOutlet.layer.borderWidth = 1.0
        }
        
        // Do any additional setup after loading the view.
    }

    //MARK:- Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - StatusBar
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return UIStatusBarStyle.lightContent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Action Zone

    @IBAction func btnEnglishAction(_ sender: Any)
    {
        setLangauge(language: English)
        UserDefaults.standard.set(0, forKey: "lang")
        
        let obj:LoginViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnArabicAction(_ sender: Any)
    {
        setLangauge(language: Arabic)
        UserDefaults.standard.set(1, forKey: "lang")

        let obj:LoginViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
