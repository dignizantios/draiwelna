//
//  View+Animation.swift
//  Networker
//
//  Created by haresh on 28/06/17.
//  Copyright © 2017 haresh. All rights reserved.
//

import Foundation


public extension UIView {
    
    func fadeIn(withDuration duration: TimeInterval = 0.5)
    {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    func fadeOut(withDuration duration: TimeInterval = 0.5)
    {
        UIView.animate(withDuration: duration, animations:
            {
                self.alpha = 0.0
        })
    }
    
}
