//
//  LocationManager.swift
//  SalonX
//
//  Created by Haresh Vavadiya on 5/15/17.
//  Copyright © 2017 Jaydeep Vora. All rights reserved.
//

import UIKit
import CoreLocation
protocol LocationDelegate {
    func didUpdateLocation(lat:Double?,lon:Double?)
    //func didFailedLocation(error:String)
}

class Location: NSObject,CLLocationManagerDelegate {

    static let shared = Location()
    var needToDisplayAlert = false
    var delegate:LocationDelegate!
    var lattitude:Double?
    var longitude:Double?
    
    private var locationManager:CLLocationManager!
    
    override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }

        lattitude = 0.00
        longitude = 0.00

    }
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            print("Location didChangeAuthorization to authorizedAlways")
            startUpdatingLocation()
        }
        else if status == .authorizedWhenInUse {
            print("Location didChangeAuthorization to authorizedWhenInUse")
            startUpdatingLocation()
        }
        else if status == .denied {
            if needToDisplayAlert {
               // displayAlert()
            }
            print("Location didChangeAuthorization to denied")
            stopUpdatingLocation()
            //delegate.didFailedLocation(error: "Location service is denied")
        }
        else if status == .notDetermined {
            print("Location didChangeAuthorization to notDetermined")
            startUpdatingLocation()
          //  delegate.didFailedLocation(error: "Location service is not determined")
        }
        else if status == .restricted {
            if needToDisplayAlert {
             //   displayAlert()
            }
            print("Location didChangeAuthorization to authorizedWhenInUse")
            stopUpdatingLocation()
           // delegate.didFailedLocation(error: "Location service is restricted")
        }
        else {
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let latestLocation = locations.first
        {
         //   userLocation = latestLocation
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude {
                self.lattitude = latestLocation.coordinate.latitude
                self.longitude = latestLocation.coordinate.longitude
                self.delegate.didUpdateLocation(lat:self.lattitude,lon:self.longitude)
            }
            stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      //  SalonLog("LocationFailedError:-\(error.localizedDescription)")
       // delegate.didFailedLocation(error: "LocationDidnotAllowError")
        
    }
    
    deinit {
        locationManager = nil
        delegate = nil
    }
    
   /* func displayAlert()
    {
        let alertController = UIAlertController(title: kTitle, message: "For more amazing experience, Please allow location", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }*/
    
}
