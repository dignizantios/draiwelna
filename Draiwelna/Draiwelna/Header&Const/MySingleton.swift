//
//  MySingleton.swift
//  SwiftMVC
//
//  Created by Jitendra Bhadja on 05/04/17.
//  Copyright © 2017 Nami. All rights reserved.
//

import Foundation
import UIKit

class MySingleton
{
    // Declare class instance property
    static let sharedManager = MySingleton()
    
    //MARK: - Application color theme declaration
    var themeColor = UIColor()
//    var themeNavigatiobBarColor = UIColor()
//    var themeTextGrayColor = UIColor()
//    var themeLightGrayColor = UIColor()
//    var themeDarkTextColor = UIColor()
//    var themeBlueColor = UIColor()
//    var themeGreenColor = UIColor()
//    var themeGrayColor = UIColor()
//
//    var themeSideMenuBackgroudColor = UIColor()
//    var themeSideMenuTextColor = UIColor()
//
//    var themeFBTextColor = UIColor()
//    var themeLinkedInTextColor = UIColor()
//    var themeSettingTextColor = UIColor()
    
    
    //MARK: - UIScreen Bounds
    var screenRect = CGRect()

    
    //MARK: - Initializer of Class
    // @desc Declare an initializer
    // Because this class is singleton only one instance of this class can be created
    /// Initialization of variables with default value
    init()
    {
        //Shared object created of Datamanager
        
        themeColor = UIColor.init(red: 188/255, green: 31/255, blue: 47/255, alpha: 1.0)

//        themeNavigatiobBarColor=UIColorToRGB(hexValue : 0xE94B4C)
//        themeTextGrayColor=UIColorToRGB(hexValue : 0xAAAAAA)
//        themeLightGrayColor=UIColorToRGB(hexValue : 0xF2F2F2)
//        themeDarkTextColor=UIColorToRGB(hexValue : 0x141414)
//        themeBlueColor=UIColorToRGB(hexValue : 0x469BD1)
//        themeGreenColor=UIColorToRGB(hexValue : 0x28BC39)
//        themeGrayColor=UIColorToRGB(hexValue : 0xBABABA)
//
//        themeSideMenuBackgroudColor=UIColorToRGB(hexValue : 0x2E3341)
//        themeSideMenuTextColor=UIColorToRGB(hexValue : 0x828EAE)
//        
//        themeFBTextColor=UIColorToRGB(hexValue : 0x3A589B)
//        themeLinkedInTextColor=UIColorToRGB(hexValue : 0x469BD1)
//        themeSettingTextColor=UIColorToRGB(hexValue : 0xB5B5B5)
        
        screenRect = UIScreen.main.bounds
        
    }
    
    
//    //MARK: - Get Custom Fonts With size
//    /// Get custom regular type font with size
//    ///
//    /// - Parameter fontSize: font size in Int dataType
//    /// - Returns: UIFont with size
//    func getFontForTypeMediumWithSize(fontSize : Int) -> UIFont
//    {
//        return UIFont(name: "gotham-medium", size: CGFloat(fontSize))!
//    }
//    
//    /// Get custom Bold type font with size
//    ///
//    /// - Parameter fontSize: font size in Int dataType
//    /// - Returns: UIFont with size
//    func getFontForTypeRoundedBoldWithSize(fontSize : Int) -> UIFont
//    {
//        return UIFont(name: "GothamRounded-Bold", size: CGFloat(fontSize))!
//    }
//    
//    /// Get custom Medium type font with size
//    ///
//    /// - Parameter fontSize: font size in Int dataType
//    /// - Returns: UIFont with size
//    func getFontForTypeRoundedMediumWithSize(fontSize : Int) -> UIFont
//    {
//        return UIFont(name: "GothamRounded-Medium", size: CGFloat(fontSize))!
//    }
//    
//    /// Get custom Light type font with size
//    ///
//    /// - Parameter fontSize: font size in Int dataType
//    /// - Returns: UIFont with size
//    func getFontForTypeRoundedLightWithSize(fontSize : Int) -> UIFont
//    {
//        return UIFont(name: "GothamRounded-Light", size: CGFloat(fontSize))!
//    }
//    
//    /// Get custom Book type font with size
//    ///
//    /// - Parameter fontSize: font size in Int dataType
//    /// - Returns: UIFont with size
//    func getFontForTypeRoundedBookWithSize(fontSize : Int) -> UIFont
//    {
//        return UIFont(name: "GothamRounded-Book", size: CGFloat(fontSize))!
//    }
}

