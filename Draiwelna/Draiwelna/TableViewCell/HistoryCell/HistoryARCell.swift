//
//  HistoryARCell.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 27/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class HistoryARCell: UITableViewCell
{
    //MARK:- Outlet Zone
    
    @IBOutlet var btnEditRideOutlet: UIButton!
    @IBOutlet var btnDeleteRideOutlet: UIButton!
    @IBOutlet var lblRideDateTime: UILabel!
    @IBOutlet var lblDistanceAR: UILabel!
    @IBOutlet var lblPickUpLocationAR: UILabel!
    @IBOutlet var lblDestinationLocationAR: UILabel!
    
    @IBOutlet var lblTotalPriceAR: UILabel!
    @IBOutlet var lblStatus: UILabel!
    
    //MARK:- TableView LifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
