//
//  HistoryCell.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 26/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell
{
    //MARK:- Outlet Zone
    
    @IBOutlet var btnEditRideOutlet: UIButton!
    @IBOutlet var btnDeleteRideOutlet: UIButton!
    @IBOutlet var lblRideDateTime: UILabel!
    @IBOutlet var lblDistanceEN: UILabel!
    @IBOutlet var lblPickUpLocationEN: UILabel!
    @IBOutlet var lblDestinationLocationEN: UILabel!
    
    @IBOutlet var lblTotalPriceEN: UILabel!
    @IBOutlet var lblStatus: UILabel!
    
    //MARK:- TableView Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
