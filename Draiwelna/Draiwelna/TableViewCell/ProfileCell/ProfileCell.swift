//
//  ProfileCell.swift
//  Queless
//
//  Created by Jaydeep Virani on 30/03/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell
{
    //MARK:- Outlet Zone
    
    @IBOutlet var imgOfUserProfile: UIImageView!
    @IBOutlet var btnLogOutOutletEN: UIButton!
     @IBOutlet var btnLogOutOutletAR: UIButton!
    @IBOutlet var lblUserName: UILabel!
    
    
    
    //MARK : - TableView Life Cycle
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
