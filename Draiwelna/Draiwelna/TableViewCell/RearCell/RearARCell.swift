//
//  RearARCell.swift
//  Draiwelna
//
//  Created by Jaydeep Virani on 22/07/17.
//  Copyright © 2017 Jaydeep Virani. All rights reserved.
//

import UIKit

class RearARCell: UITableViewCell {

    //MARK:- Outlet Zone
    @IBOutlet var viewOfHighlightCell: UIView!
    @IBOutlet var imgOfCell: UIImageView!
    @IBOutlet var lblName: UILabel!
    
    //MARK:- TableView LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
